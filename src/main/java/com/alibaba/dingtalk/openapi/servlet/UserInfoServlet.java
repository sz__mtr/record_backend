package com.alibaba.dingtalk.openapi.servlet;

import com.alibaba.dingtalk.openapi.demo.OApiException;
import com.alibaba.dingtalk.openapi.demo.auth.AuthHelper;
import com.alibaba.dingtalk.openapi.demo.user.UserHelper;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.dingtalk.open.client.api.model.corp.CorpUserDetail;
import com.sg.common.empty.service.CfgService;
import com.sg.common.utils.Constant;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

/**
 * 根据免登授权码查询免登用户信息
 */
public class UserInfoServlet extends HttpServlet {

	@Autowired
	CfgService service;

    /**
     * 根据免登授权码获取免登用户userId
     *
     * @param request
     * @param response
     * @throws IOException
     */
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        // 获取免登授权码
//        String code = request.getParameter("code");
//        String corpId = request.getParameter("corpid");
//        System.out.println("authCode:" + code + " corpid:" + corpId);
//        try {
//            response.setContentType("text/html; charset=utf-8");
//
//            String accessToken = AuthHelper.getAccessToken();
//            System.out.println("access token:" + accessToken);
//            CorpUserDetail user = UserHelper.getUser(accessToken, UserHelper.getUserInfo(accessToken, code).getUserid());
//
//            String userJson = JSON.toJSONString(user);
//            response.getWriter().append(userJson);
//            System.out.println("userjson:" + userJson);
//        } catch (Exception e) {
//            e.printStackTrace();
//            response.getWriter().append(e.getMessage());
//        }
//    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String code = request.getParameter("code");
		String corpId = request.getParameter("corpid");
      	System.out.println("authCode:" + code + " corpid:" + corpId);
		System.out.println(code);

		JSONObject res=new JSONObject();
		try {
			String accessToken = AuthHelper.getAccessToken();
			 CorpUserDetail user = UserHelper.getUser(accessToken, UserHelper.getUserInfo(accessToken, code).getUserid());
//			JSONObject userInfo=UserHelper.getUserInfo(accessToken, code);
//			response.getWriter().append(userInfo.toString());
//			 System.out.println("getUserid"+user.getUserid());
//			 System.out.println("getName"+user.getName());
//			 System.out.println("getMobile"+user.getMobile());
			 
//			System.out.println(UserHelper.getUserInfo(accessToken, code).toString());
			request.getSession().setAttribute("user_name", user.getName());
			request.getSession().setAttribute(Constant.SESSION_MEMBER_CODE, user.getUserid());
			request.getSession().setAttribute("user_tel", user.getMobile());
			request.getSession().setAttribute("canin", true);
			res.put("user_name", user.getName());
			res.put("user_code", user.getUserid());
			res.put("user_tel", user.getMobile());
			res.put("canin", true);
			response.setContentType("application/json");
			response.getWriter().append(res.toJSONString());	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.getWriter().append(e.getMessage());
//			response.setContentType("application/json");
//			res.put("user_name", "王泽");
//			res.put("user_code", "203187");
//			res.put("user_tel", "15862596666");
//			res.put("canin", true);
//			request.getSession().setAttribute("user_name", "王泽");
//			request.getSession().setAttribute(Constant.SESSION_MEMBER_CODE, "203187");
//			request.getSession().setAttribute("user_tel", "15862596666");
//			request.getSession().setAttribute("canin", true);
//			response.getWriter().append(res.toJSONString());
		}
	}
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        doGet(request, response);
    }

}
