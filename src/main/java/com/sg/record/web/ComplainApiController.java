package com.sg.record.web;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.sql.ast.SQLObject;
import com.alibaba.fastjson.JSONArray;
import com.sg.common.empty.core.ConditionRes;
import com.sg.common.empty.core.Conditions2;
import com.sg.common.empty.core.FilterVal;
import com.sg.common.empty.core.Params2;
import com.sg.common.empty.service.CfgService;
import com.sg.common.utils.CollUtil;
import com.sg.common.utils.Constant;
import com.sg.common.utils.DateTimeUtils;

@Repository
@Controller
@RequestMapping(value = "/api")
public class ComplainApiController {
	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	CfgService service;


	@RequestMapping(value = "/record/complain/complainList")
	@ResponseBody
	public Object findPlanList(HttpServletRequest request,
			@RequestBody Map<String, Object> requestBody) throws Exception {
		long s = System.currentTimeMillis();
//		System.out.println("requestBody:    "+requestBody);
		
		String route = (String) request.getAttribute("conf.route");
		// TODO 如果有js，使用js确定使用哪个sql
		Conditions2 condis = Conditions2.init(request, requestBody);
//		HttpSession session = request.getSession();
//		List<String> myRoles = (List<String>) session
//				.getAttribute(Constant.SESSION_MYROLES_CODE);
//		Boolean canSeeAll = false;
//		if (CollUtil.isNotEmpty(myRoles)) {
//			for (String myRole : myRoles) {
//				String TRI001 = "TRI001";		//TRI001,企划部综合管理员与人教部教培科人员,可以看到所有部门中心培训计划/实际
//				if (StringUtils.equals(myRole, TRI001)) {
//					canSeeAll = true;
//					break;
//				}
//			}
//		}
//		if (canSeeAll) {
//			session.setAttribute(Constant.SESSION_MYDEPORG, null);
//		}

		ConditionRes cr = condis.buildCon();
		String sorts = condis.buildSorts();                                                                                                             
		String limit = condis.buildLimit();

		// 替换
		String orancsql = Constant.get(route + ".csql");
		String oransql = Constant.get(route + ".sql");
		orancsql = orancsql.replace("@conditions", cr.getSql()).replace(
				"@orderby", sorts);
		oransql = oransql.replace("@conditions", cr.getSql())
				.replace("@orderby", sorts).replace("@limit", limit);

		List<Object> countList = service.queryBySql(orancsql, cr.getParams());
		String res = Constant.get(route + ".res");
		List<Map<String, Object>> dataList = service.queryBySqlToMap(oransql,
				cr.getParams(), res.split(","));
		
//		System.out.println(dataList);
		
//		List<Map<String, Object>> planList = new ArrayList<Map<String, Object>>();
		
//		System.out.println(planList);
		

		int count = ((BigInteger) countList.get(0)).intValue();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("items", dataList);
		map.put("total", count);
		map.put("code", 2000);
		long e = System.currentTimeMillis();
		System.out.println(e - s);
		return map;
	}
	
	
	@RequestMapping(value = "/record/complain/complainUpdate")
	@ResponseBody
	public Object up(HttpServletRequest request,@RequestBody Map<String, Object> requestBody) {
		String route = (String) request.getAttribute("conf.route");
		System.out.println("requestBody             :            "+requestBody);
		String oransql = Constant.get(route + ".sql");
		List<String> sqlParams = Params2.init(request,requestBody,service).build();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			service.excuteBySql(oransql, sqlParams.toArray());
			map.put("code", 2000);
		} catch (Exception e) {
			logger.error(e.getMessage());
			map.put("code", 5030);
			map.put("msg", "更新发生内部错误，请联系管理员");
		}
		return map;
	}

}
