package com.sg.record.web;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.druid.sql.ast.SQLObject;
import com.alibaba.fastjson.JSONArray;
import com.sg.common.login.entity.Member;
import com.sg.common.login.service.OrgMemberService;
import com.sg.common.empty.core.ConditionRes;
import com.sg.common.empty.core.Conditions2;
import com.sg.common.empty.core.FilterVal;
import com.sg.common.empty.core.Params2;
import com.sg.common.empty.service.CfgService;
import com.sg.common.utils.CollUtil;
import com.sg.common.utils.Constant;
import com.sg.common.utils.DateTimeUtils;
import com.sg.record.service.RecordApiService;;;

@Repository
@Controller
@RequestMapping(value = "/api")
public class RecordApiController {
	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	CfgService service;
	
	@Autowired
	OrgMemberService omService;

	@Autowired
	RecordApiService recordService;

	@RequestMapping(value = "/record/compLog/list")
	@ResponseBody
	public Object findPlanList(HttpServletRequest request,
			@RequestBody Map<String, Object> requestBody) throws Exception {
		long s = System.currentTimeMillis();
		String route = (String) request.getAttribute("conf.route");
		// TODO 如果有js，使用js确定使用哪个sql
		Conditions2 condis = Conditions2.init(request, requestBody);


		ConditionRes cr = condis.buildCon();
		String sorts = condis.buildSorts();                                                                                                             
		String limit = condis.buildLimit();

		// 替换
		String orancsql = "";
		String oransql = "";
		
		String res = Constant.get(route + ".res");
//		List<Object> countList = new ArrayList<Object>();
//		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		
//		String org = requestBody.get("org").toString(); requestBody.containsKey("orgId") == false || 
		
		System.out.printf("requestBody.get(org).toString()          "+requestBody.containsKey("orgId") +"             ");
		
		if(StringUtils.isNotBlank(requestBody.get("orgId").toString())){
			List<String> conditions = Constant.getStringList(route + ".conditions1");
			cr = condis.buildCon(conditions);
			orancsql = Constant.get(route + ".csql1");
			oransql = Constant.get(route + ".sql1");
			orancsql = orancsql.replace("@conditions1", cr.getSql()).replace(
					"@orderby", sorts);
			oransql = oransql.replace("@conditions1", cr.getSql())
					.replace("@orderby", sorts).replace("@limit", limit);

		}else{
			orancsql = Constant.get(route + ".csql");
			oransql = Constant.get(route + ".sql");
			orancsql = orancsql.replace("@conditions", cr.getSql()).replace(
					"@orderby", sorts);
			oransql = oransql.replace("@conditions", cr.getSql())
					.replace("@orderby", sorts).replace("@limit", limit);
//			countList = service.queryBySql(orancsql, cr.getParams());
//			
//			dataList = service.queryBySqlToMap(oransql,
//					cr.getParams(), res.split(","));
		}
		
		List<Object> countList = service.queryBySql(orancsql, cr.getParams());
		
		List<Map<String, Object>> dataList = service.queryBySqlToMap(oransql,
				cr.getParams(), res.split(","));
		
		int count = ((BigInteger) countList.get(0)).intValue();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("items", dataList);
		map.put("total", count);
//		map.put("myStationOrgId", cr.getParams()[0]);
		map.put("code", 2000);
		long e = System.currentTimeMillis();
		return map;
	}
	
	
	@RequestMapping(value = "/record/compLog/add")
	@ResponseBody
	public Object add(HttpServletRequest request,@RequestBody Map<String, Object> requestBody) throws Exception {
		String route = (String) request.getAttribute("conf.route");
		
//		在有未交接班的情况下，不允许新建
		Conditions2 condis = Conditions2.init(request, requestBody);
		List<String> conditions = Constant.getStringList(route + ".addCheckConditions");
		ConditionRes cr = condis.buildCon(conditions);
		String oranaddCheckSql = Constant.get(route + ".addCheckSql");
		oranaddCheckSql = oranaddCheckSql.replace("@addCheckConditions", cr.getSql());
		String addComplogCheck = requestBody.get("addComplogCheck").toString();
		
		List<Object> countAddCheck = service.queryBySql(oranaddCheckSql, cr.getParams());
		int count = ((BigInteger) countAddCheck.get(0)).intValue();
		
		System.out.print("   System  :          "+count);

		Map<String, Object> map = new HashMap<String, Object>();
		
		if(count == 0 || addComplogCheck.equals("0")){
		
		
		
		
		
		//同一日期、同一班组、同一班次，仅能有一个有效状态（新建/已接班确认）
		
		conditions = Constant.getStringList(route + ".cconditions");
		cr = condis.buildCon(conditions);
		String sorts = condis.buildSorts();
		String limit = condis.buildLimit();
		
		String orancsql = Constant.get(route + ".csql");
		orancsql = orancsql.replace("@cconditions", cr.getSql()).replace("@orderby", sorts);
		List<Object> countList = service.queryBySql(orancsql, cr.getParams());
//		int count = ((BigInteger) countList.get(0)).intValue();
		List<String> sqlParams = Params2.init(request,requestBody,service).build();
		Map<String, Object> item = new HashMap<String, Object>();
		if(((BigInteger) countList.get(0)).intValue() == 0){
			
			
			//综合日志的添加
			String oransql = Constant.get(route + ".sql");
			sqlParams = Params2.init(request,requestBody,service).build();
			service.excuteBySql(oransql, sqlParams.toArray());
			
			//综合日志添加完成后，查找上一个班日志，查找本组织下，除当前日志外创建日期最近的日志
//			Conditions2 condis = Conditions2.init(request, requestBody);
			cr = condis.buildCon();
			sorts = condis.buildSorts();
			limit = condis.buildLimit();
			
			String res3 = "";
			String id = "";
			String currentTime = "";

			
			String orancomplogsql = Constant.get(route + ".complogsql");
			orancomplogsql = orancomplogsql.replace("@conditions", cr.getSql()).replace("@orderby", sorts).replace("@limit", limit);
			
			List<Object> complog = service.queryBySql(orancomplogsql, cr.getParams());
			
			String complogId = complog.get(0).toString();
			requestBody.put("complogId", complogId);
			
			//判断该人员属于车站人员（orge=1400），还是行政组织人员(orge=1000)
			String orge = requestBody.get("orge").toString();
			if(orge.equals("1400")){
				//车站人员
				//按照  complogId  查询impth(当班期间重要事项)中的 所有需要传下去的数据
				conditions = Constant.getStringList(route + ".Conditions3");
				condis = Conditions2.init(request, requestBody);
				cr = condis.buildCon(conditions);
				res3 = Constant.get(route + ".impthRes");
				String oranimpthListSql = Constant.get(route + ".impthListSql");
				oranimpthListSql = oranimpthListSql.replace("@Conditions3", cr.getSql().replace("@orderby", sorts).replace("@limit", limit));
				 
				List<Map<String, Object>> impthList = service.queryBySqlToMap(oranimpthListSql,cr.getParams(), res3.split(","));
//				System.out.print("impthList              "+impthList);
				
				String currentMemberId = (String) FilterVal.init("currentMemberId", service).get2();
				//将查到的数据insert到impth表中
				//1,需处理;2,待交接                ①待处理事项，自动复制至下一个日志                         ②需交接事项，交接次数+1，默认交接满8个班次后不再交接
				for(Map<String, Object> impth : impthList){
					id = (String) FilterVal.init("sys32Id", service).get2();
					currentTime = (String) FilterVal.init("currentTime", service).get2();
					int pasts = 0;
					String impthAddSql = "insert into record_impth (id,compLog,enable,pasts,content,remark,parent,firstDate,updateMember,updateDate,createMember,createDate,usrct) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
					if(impth.get("firstDate") == null && impth.get("parent") == null){
						if(impth.get("enable").equals("2")){
							pasts = Integer.parseInt(impth.get("pasts").toString()) + 1;
						}
						else{
							pasts = Integer.parseInt(impth.get("pasts").toString());
						}
						service.excuteBySql(impthAddSql, new Object[]{id, sqlParams.get(0), impth.get("enable"), pasts,impth.get("content"),impth.get("remark"),impth.get("id"),impth.get("createDate"),impth.get("updateMember"),impth.get("updateDate"),currentMemberId,currentTime,impth.get("usrct")});
					}else{
						if(impth.get("enable").equals("2")){
							pasts += Integer.parseInt(impth.get("pasts").toString()) ;
						}
						else{
							pasts = Integer.parseInt(impth.get("pasts").toString());
						}
						service.excuteBySql(impthAddSql, new Object[]{id, sqlParams.get(0), impth.get("enable"), pasts,impth.get("content"),impth.get("remark"),impth.get("parent"),impth.get("firstDate"),impth.get("updateMember"),impth.get("updateDate"),currentMemberId,currentTime,impth.get("usrct")});
					}
					
				}
				
				
				//按照  complogId  查询complain(乘客投诉)中的 所有需要传下去的数据
//				conditions = Constant.getStringList(route + ".complainConditions");
//				condis = Conditions2.init(request, requestBody);
//				cr = condis.buildCon(conditions);
				res3 = Constant.get(route + ".res2");
				String orancomplainListSql = Constant.get(route + ".complainListSql");
				orancomplainListSql = orancomplainListSql.replace("@Conditions3", cr.getSql().replace("@orderby", sorts).replace("@limit", limit));
				
//				String complainRes = Constant.get(route + ".complainRes");
				List<Map<String, Object>> complainList = service.queryBySqlToMap(orancomplainListSql,cr.getParams(), res3.split(","));
				System.out.print("complainList              "+complainList);
				
				
				//将查到的数据insert到complain表中
				//1,需处理;2,待交接                ①待处理事项，自动复制至下一个日志                         ②需交接事项，交接次数+1，默认交接满8个班次后不再交接
				for(Map<String, Object> complain : complainList){
					id = (String) FilterVal.init("sys32Id", service).get2();
					currentTime = (String) FilterVal.init("currentTime", service).get2();
					int pasts = 0;
					String complainAddSql = "insert into record_complain (id,compLog,enable,date,time,pasts,content,remark,parent,firstDate,updateMember,updateDate,createMember,createDate,usrct) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					if(complain.get("firstDate") == null && complain.get("parent") == null){
						if(complain.get("enable").equals("2")){
							pasts = Integer.parseInt(complain.get("pasts").toString()) + 1;
						}
						else{
							pasts = Integer.parseInt(complain.get("pasts").toString());
						}
						service.excuteBySql(complainAddSql, new Object[]{id, sqlParams.get(0), complain.get("enable"),complain.get("date"),complain.get("time"), pasts,complain.get("content"),complain.get("remark"),complain.get("id"),complain.get("createDate"),complain.get("updateMember"),complain.get("updateDate"),currentMemberId,currentTime,complain.get("usrct")});
					}else{
						if(complain.get("enable").equals("2")){
							pasts = Integer.parseInt(complain.get("pasts").toString()) + 1;
						}
						else{
							pasts = Integer.parseInt(complain.get("pasts").toString());
						}
						service.excuteBySql(complainAddSql, new Object[]{id, sqlParams.get(0), complain.get("enable"), complain.get("date"),complain.get("time"), pasts,complain.get("content"),complain.get("remark"),complain.get("parent"),complain.get("firstDate"),complain.get("updateMember"),complain.get("updateDate"),currentMemberId,currentTime,complain.get("usrct")});
					}
					
				}
				
				
				

				String oraninjuredListSql = Constant.get(route + ".injuredListSql");
				oraninjuredListSql = oraninjuredListSql.replace("@Conditions3", cr.getSql().replace("@orderby", sorts).replace("@limit", limit));
				
				List<Map<String, Object>> injuredList = service.queryBySqlToMap(oraninjuredListSql,cr.getParams(), res3.split(","));
//				System.out.print("injuredList              "+injuredList);
				
//				String id = "";
//				String currentTime = "";
//				String currentMemberId = (String) FilterVal.init("currentMemberId", service).get2();;
				//将查到的数据insert到impth表中
				//1,需处理;2,待交接                ①待处理事项，自动复制至下一个日志                         ②需交接事项，交接次数+1，默认交接满8个班次后不再交接
				for(Map<String, Object> injured : injuredList){
					id = (String) FilterVal.init("sys32Id", service).get2();
					currentTime = (String) FilterVal.init("currentTime", service).get2();
					int pasts = 0;
					String injuredAddSql = "insert into record_injured (id,compLog,enable,date,time,pasts,content,remark,parent,firstDate,updateMember,updateDate,createMember,createDate,usrct) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					if(injured.get("firstDate") == null && injured.get("parent") == null){
						if(injured.get("enable").equals("2")){
							pasts = Integer.parseInt(injured.get("pasts").toString()) + 1;
						}
						else{
							pasts = Integer.parseInt(injured.get("pasts").toString());
						}
						service.excuteBySql(injuredAddSql, new Object[]{id, sqlParams.get(0), injured.get("enable"), injured.get("date"),injured.get("time"), pasts,injured.get("content"),injured.get("remark"),injured.get("id"),injured.get("createDate"),injured.get("updateMember"),injured.get("updateDate"),currentMemberId,currentTime,injured.get("usrct")});
					}else{
						if(injured.get("enable").equals("2")){
							pasts = Integer.parseInt(injured.get("pasts").toString()) + 1;
						}
						else{
							pasts = Integer.parseInt(injured.get("pasts").toString());
						}
						service.excuteBySql(injuredAddSql, new Object[]{id, sqlParams.get(0), injured.get("enable"), injured.get("date"),injured.get("time"), pasts,injured.get("content"),injured.get("remark"),injured.get("parent"),injured.get("firstDate"),injured.get("updateMember"),injured.get("updateDate"),currentMemberId,currentTime,injured.get("usrct")});
					}
					
				}
			}else{
				//行政组织人员
				//按照  complogId  查询handover(当班内容)中的 所有需要传下去的数据
				conditions = Constant.getStringList(route + ".Conditions3");
				condis = Conditions2.init(request, requestBody);
				cr = condis.buildCon(conditions);
				res3 = Constant.get(route + ".handoverRes");
				String oranimpthListSql = Constant.get(route + ".handoverListSql");
				oranimpthListSql = oranimpthListSql.replace("@Conditions3", cr.getSql().replace("@orderby", sorts).replace("@limit", limit));
				 
				List<Map<String, Object>> handoverList = service.queryBySqlToMap(oranimpthListSql,cr.getParams(), res3.split(","));
//				System.out.print("impthList              "+impthList);
				
				
				String currentMemberId = (String) FilterVal.init("currentMemberId", service).get2();
				//将查到的数据insert到impth表中
				//1,需处理;2,待交接                ①待处理事项，自动复制至下一个日志                         ②需交接事项，交接次数+1，默认交接满8个班次后不再交接
				for(Map<String, Object> handover : handoverList){
					id = (String) FilterVal.init("sys32Id", service).get2();
					currentTime = (String) FilterVal.init("currentTime", service).get2();
					String handoverAddSql = "insert into record_handover (id,compLog,enable,content,remark,parent,yorn,updateMember,updateDate,createMember,createDate,usrct) values(?,?,?,?,?,?,?,?,?,?,?,?)";
					if(handover.get("parent") == null){
						service.excuteBySql(handoverAddSql, new Object[]{id, sqlParams.get(0), handover.get("enable"), handover.get("content"),handover.get("remark"),handover.get("id"),handover.get("yorn"),handover.get("updateMember"),handover.get("updateDate"),currentMemberId,currentTime,handover.get("usrct")});
					}else{
						service.excuteBySql(handoverAddSql, new Object[]{id, sqlParams.get(0), handover.get("enable"), handover.get("content"),handover.get("remark"),handover.get("parent"),handover.get("yorn"),handover.get("updateMember"),handover.get("updateDate"),currentMemberId,currentTime,handover.get("usrct")});
					}
				}
			}
			item.put("id", sqlParams.get(0));
			map.put("item", item);
			map.put("code", 2000);
			
		}else{
			logger.error("");
			map.put("code", 5000);
			map.put("msg", "本站当前已有该班次，清核对后重新提交");
			
		}
		
	}else{
		logger.error("");
		map.put("code", 5000);
		map.put("msg", "本站当前有"+count+"条综合日志未完成交接班，清确认交接班后再新建综合日志");
	}
		
		return map;
	}
	
	
	@RequestMapping(value = "/record/compLog/addComplogCheck")
	@ResponseBody
	public Object addComplogCheck(HttpServletRequest request,@RequestBody Map<String, Object> requestBody) throws Exception {
		String route = (String) request.getAttribute("conf.route");
		ConditionRes cr = Conditions2.init(request,requestBody).buildCon();
		String orancsql = Constant.get(route + ".csql");
		orancsql = orancsql.replace("@conditions", cr.getSql());
		String addComplogCheck = requestBody.get("addComplogCheck").toString();
		
		List<Object> countList = service.queryBySql(orancsql, cr.getParams());
		int count = ((BigInteger) countList.get(0)).intValue();

		Map<String, Object> map = new HashMap<String, Object>();
		
		
		
		if(count == 0 ||addComplogCheck.equals("0")){
			map.put("code", 2000);
		}else{
			logger.error("");
			map.put("code", 5000);
			map.put("msg", "本站当前有"+count+"条综合日志未完成交接班，清确认交接班后再新建综合日志");
		}
		return map;
	}
	
	@RequestMapping(value = "/record/compLog/confirmComplogCheck")
	@ResponseBody
	public Object confirmComplogCheck(HttpServletRequest request,@RequestBody Map<String, Object> requestBody) throws Exception {
		String route = (String) request.getAttribute("conf.route");
		ConditionRes cr = Conditions2.init(request,requestBody).buildCon();
		String oranfundsql = Constant.get(route + ".fundsql");
		String oranumbrsql = Constant.get(route + ".umbrsql");
		oranfundsql = oranfundsql.replace("@conditions", cr.getSql());
		oranumbrsql = oranumbrsql.replace("@conditions", cr.getSql());
		
		List<Object> fundCountList = service.queryBySql(oranfundsql, cr.getParams());
		int fundCount = ((BigInteger) fundCountList.get(0)).intValue();
		
		List<Object> umbrCountList = service.queryBySql(oranumbrsql, cr.getParams());
		int umbrCount = ((BigInteger) umbrCountList.get(0)).intValue();

		Map<String, Object> map = new HashMap<String, Object>();
		
		
		if(fundCount != 0){
			if(requestBody.get("shift").toString().equals("Y1")){
				if(umbrCount != 0){
					map.put("code", 2000);
				}else{
					map.put("code", 5000);
					map.put("msg", "本班次中的  便民雨伞  未添加，请确认添加后再交接班");
				}
				
			}else{
				map.put("code", 2000);
			}
		}else{
			map.put("code", 5000);
			map.put("msg", "本班次中的  应急备用金  未添加，请确认添加后再交接班");
		}
		return map;
	}
	
	
	@RequestMapping(value = "/record/compLog/confirm")
	@ResponseBody
	public Object confirm(HttpServletRequest request,@RequestBody Map<String, Object> requestBody) {
		String route = (String) request.getAttribute("conf.route");
		
		Map<String, Object> map = new HashMap<String, Object>();
		if(requestBody.get("enable").toString().equals("1")){
			if (StringUtils.equalsIgnoreCase(requestBody.get("code").toString(), requestBody.get("memCode").toString())) {//限定,日志创建人与交班确认人,必须为同一人！

				Member m = omService.findAllMemberByCode(requestBody.get("memCode").toString());
				if(m!=null){
//					res = recordService.confirm(id, null);
					String oransql = Constant.get(route + ".sql");
					List<String> sqlParams = Params2.init(request,requestBody,service).build();
					service.excuteBySql(oransql, sqlParams.toArray());
					Map<String, Object> item = new HashMap<String, Object>();
					item.put("id", sqlParams.get(0));
					map.put("item", item);
					map.put("code", 2000);
				}else{
					map.put("code", 5000);
					map.put("msg", "该账号不存在");
				}
			}else{
//				logger.error("");
				map.put("code", 5000);
				map.put("msg", "日志创建人与接班确定人必须为同一人，日志创建人工号："+requestBody.get("code").toString()+",当前接班确定人工号："+requestBody.get("memCode").toString());
			}
			
		}else{
//			logger.error("");
			map.put("code", 5000);
			map.put("msg", "不能完成交接班确认，请确认日志为新建状态");
			
		}
		return map;
	}
	
	
	
	@RequestMapping(value = "/record/compLog/sConfirm")
	@ResponseBody
	public Object sConfirm(HttpServletRequest request,@RequestBody Map<String, Object> requestBody) {
		String route = (String) request.getAttribute("conf.route");
		
		Map<String, Object> map = new HashMap<String, Object>();
		if(requestBody.get("enable").toString().equals("1")){
			if (StringUtils.equalsIgnoreCase(requestBody.get("code").toString(), requestBody.get("memCode").toString())) {//限定,日志创建人与账户登录人,必须为同一人！
				if (!StringUtils.equalsIgnoreCase(requestBody.get("confirmCode").toString(), requestBody.get("memCode").toString())) { // 接班时，必须接班人与当前登录账户不同(与日志创建账户不同)
					Member m = omService.confirmMember(requestBody.get("confirmCode").toString(),requestBody.get("confirmPass").toString());
					if(m!=null){
						
						//得到接班人的id
						Conditions2 condis = Conditions2.init(request, requestBody);
						ConditionRes cr = condis.buildCon();
						String sorts = condis.buildSorts();
						String limit = condis.buildLimit();
						String idsql = Constant.get(route + ".idsql");
						idsql = idsql.replace("@conditions", cr.getSql()).replace("@orderby", sorts).replace("@limit", limit);
						
						List<Object> MemberId = service.queryBySql(idsql, cr.getParams());
						
						String confirmId = MemberId.get(0).toString();
						requestBody.put("confirmId", confirmId);
						
						
						//验证便民雨伞中是否有月盘点或其他盘点没有清点
						List<String> conditions = Constant.getStringList(route + ".allconditions");
						cr = condis.buildCon(conditions);
						String umbrcsql = Constant.get(route + ".umbrcsql");
						umbrcsql = umbrcsql.replace("@allconditions", cr.getSql()).replace("@orderby", sorts).replace("@limit", limit);
						List<Object> umbrCount = service.queryBySql(umbrcsql, cr.getParams());
						int uCount = ((BigInteger) umbrCount.get(0)).intValue();   //uCount == 0,表示本综合日志中，便民雨伞没有月盘点与其他盘点
						
						
						//验证应急备用金中是否有月盘点或其他盘点没有清点
						String fundcsql = Constant.get(route + ".fundcsql");
						fundcsql = fundcsql.replace("@allconditions", cr.getSql()).replace("@orderby", sorts).replace("@limit", limit);
						List<Object> fundCount = service.queryBySql(fundcsql, cr.getParams());
						int fCount = ((BigInteger) fundCount.get(0)).intValue();   //uCount == 0,表示本综合日志中，应急备用金没有月盘点与其他盘点
						
//						String res = "";
						
						// 应急备用金 日盘点  准备
						String umbrsql = Constant.get(route + ".umbrsql");
						List<String> umbrps = Constant.getStringList(route + ".umbrparams");
						List<String> umbrparams = Params2.init(request,requestBody,service).build(umbrps);
						
						// 便民雨伞 日盘点  准备
						String fundsql = Constant.get(route + ".fundsql");
						List<String> fundps = Constant.getStringList(route + ".fundparams");
						List<String> fundparams = Params2.init(request,requestBody,service).build(fundps);
						
						
						//综合日志交接班准备
						String oransql = Constant.get(route + ".sql");
						List<String> sqlParams = Params2.init(request,requestBody,service).build();
						
						if(uCount == 0 && fCount == 0){
							// 便民雨伞，应急备用金，可直接update，修改接班人
							
							try {
								//日盘点便民雨伞清点
								service.excuteBySql(umbrsql, umbrparams.toArray());
								//日盘点应急备用金清点
								service.excuteBySql(fundsql, fundparams.toArray());
								//综合日志完成交接班
								service.excuteBySql(oransql, sqlParams.toArray());
								map.put("code", 2000);
							} catch (Exception e) {
								logger.error(e.getMessage());
								map.put("code", 5030);
								map.put("msg", "更新发生内部错误，请联系管理员");
							}
						}else{
//							res = Constant.get(route + ".res");
							String umbrlistsql = Constant.get(route + ".umbrlistsql");
							umbrlistsql = umbrlistsql.replace("@allconditions", cr.getSql().replace("@orderby", sorts).replace("@limit", limit));
							List<Object> umbrList = service.queryBySql(umbrlistsql, cr.getParams());
//							List<Map<String, Object>> umbrList = service.queryBySqlToMap(umbrlistsql,cr.getParams(), res.split(","));
//							System.out.print("impthList              "+impthList);
							
							
							String fundlistsql = Constant.get(route + ".fundlistsql");
							fundlistsql = fundlistsql.replace("@allconditions", cr.getSql().replace("@orderby", sorts).replace("@limit", limit));
							List<Object> fundlist = service.queryBySql(fundlistsql, cr.getParams());
							
//							String currentMemberId = (String) FilterVal.init("currentMemberId", service).get2();
							//将查到的数据insert到impth表中
							//1,需处理;2,待交接                ①待处理事项，自动复制至下一个日志                         ②需交接事项，交接次数+1，默认交接满8个班次后不再交接
							
							if(uCount != 0 && fCount != 0){
								for(Object umbr : umbrList){
//								System.out.print("    umbrList           :"+CollUtil.isNotEmpty(umbrList)+"      :"+!(umbr == null));
									if(umbr == null){
										//umbr为月盘点中的清点人，若清点人为空，则证明月盘点与其他盘点，未清点，不能交接班
										map.put("code", 5000);
										map.put("msg", "便民雨伞中的月盘点还未清点，请确认清点后再提交");
										return map;
									}
								}
								for(Object fund : fundlist){
									if(fund == null){
										//fund为月盘点中的清点人，若清点人为空，则证明月盘点与其他盘点，未清点，不能交接班
										map.put("code", 5000);
										map.put("msg", "应急备用金中的月盘点还未清点，请确认清点后再提交");
										return map;
									}
								}
								//若还未return 则开始交接班
								try {
									//日盘点便民雨伞清点
									service.excuteBySql(umbrsql, umbrparams.toArray());
									//日盘点应急备用金清点
									service.excuteBySql(fundsql, fundparams.toArray());
									//综合日志完成交接班
									service.excuteBySql(oransql, sqlParams.toArray());
									map.put("code", 2000);
								} catch (Exception e) {
									logger.error(e.getMessage());
									map.put("code", 5030);
									map.put("msg", "更新发生内部错误，请联系管理员");
								}
							}else if(uCount == 0 && fCount != 0){
								for(Object fund : fundlist){
									if(fund == null){
										//fund为月盘点中的清点人，若清点人为空，则证明月盘点与其他盘点，未清点，不能交接班
										map.put("code", 5000);
										map.put("msg", "应急备用金中的月盘点还未清点，请确认清点后再提交");
										return map;
									}
								}
								//若还未return 则开始交接班
								try {
									//日盘点便民雨伞清点
									service.excuteBySql(umbrsql, umbrparams.toArray());
									//日盘点应急备用金清点
									service.excuteBySql(fundsql, fundparams.toArray());
									//综合日志完成交接班
									service.excuteBySql(oransql, sqlParams.toArray());
									map.put("code", 2000);
								} catch (Exception e) {
									logger.error(e.getMessage());
									map.put("code", 5030);
									map.put("msg", "更新发生内部错误，请联系管理员");
								}
							}else if(uCount != 0 && fCount == 0){
								for(Object umbr : umbrList){
									if(umbr == null){
										//umbr为月盘点中的清点人，若清点人为空，则证明月盘点与其他盘点，未清点，不能交接班
										map.put("code", 5000);
										map.put("msg", "便民雨伞中的月盘点还未清点，请确认清点后再提交");
										return map;
									}
								}
								//若还未return 则开始交接班
								try {
									//日盘点便民雨伞清点
									service.excuteBySql(umbrsql, umbrparams.toArray());
									//日盘点应急备用金清点
									service.excuteBySql(fundsql, fundparams.toArray());
									//综合日志完成交接班
									service.excuteBySql(oransql, sqlParams.toArray());
									map.put("code", 2000);
								} catch (Exception e) {
									logger.error(e.getMessage());
									map.put("code", 5030);
									map.put("msg", "更新发生内部错误，请联系管理员");
								}
							}
						}
					}else{
						map.put("code", 5000);
						map.put("msg", "该账号不存在");
					}
				}else{
					map.put("code", 5000);
					map.put("msg", "日志创建人与接班人不能为同一人，日志创建人工号："+requestBody.get("code").toString()+", 当前接班确定人工号："+requestBody.get("confirmCode").toString());
				}

			}else{
//				logger.error("");
				map.put("code", 5000);
				map.put("msg", "日志创建人与当前登录人必须为同一人，日志创建人工号："+requestBody.get("code").toString()+", 当前系统登录人工号："+requestBody.get("memCode").toString());
			}
			
		}else{
//			logger.error("");
			map.put("code", 5000);
			map.put("msg", "不能完成交接班确认，请确认日志为新建状态");
			
		}
		return map;
	}
	
	

}
