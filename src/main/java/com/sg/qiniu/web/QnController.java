package com.sg.qiniu.web;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qiniu.util.Auth;
import com.sg.common.utils.Constant;

@Repository
@Controller
public class QnController {

	private Logger logger = LoggerFactory.getLogger(getClass());
	// final String SEPORATOR = System.getProperty("line.separator");

//	@RequestMapping(value = "/api/qn/repltoken")
//	@ResponseBody
//	public Object repltoken(HttpServletRequest request) throws Exception {
//	    String accessKey = Constant.get("qn.AKey");
//	    String secretKey = Constant.get("qn.SKey");
//	    String bucket = Constant.get("qn.bucket");
//	    String key = request.getParameter("key");
//	    
//	    Auth auth = Auth.create(accessKey, secretKey);
//		if(StringUtils.isNoneEmpty(key)){
//			String upToken = auth.uploadToken(bucket, key);
//		}
//	    String repltoken = auth.uploadToken(bucket);
//	    System.out.println(repltoken);
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("repltoken", repltoken);
//		map.put("code", 1);
//		return map;
//	}

	@RequestMapping(value = "/api/qn/uptoken")
	@ResponseBody
	public Object uptoken(HttpServletRequest request) throws Exception {
	    String accessKey = Constant.get("qn.AKey");
	    String secretKey = Constant.get("qn.SKey");
	    String bucket = Constant.get("qn.bucket");
	    Auth auth = Auth.create(accessKey, secretKey);
	    String key = request.getParameter("key");
	    String upToken;
		if(StringUtils.isNoneEmpty(key)){
			upToken = auth.uploadToken(bucket, key);
		}else{
			upToken = auth.uploadToken(bucket);
		}
	    System.out.println(upToken);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("uptoken", upToken);
		map.put("code", 2000);
		return map;
	}
}