package com.sg.common.mail;

import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;



public class Mail {
	private Logger log = Logger.getLogger(Mail.class);

	private String host;

	private String mail_head_name;

	private String mail_head_value;

	private String mail_to;
	private String mail_cc;
	private String mail_bcc;

	private String mail_from;

	private String mail_subject;

	private String mail_body;

	private String personalName;
	
	public static Mail getScInstance(){
		Mail m = new Mail();
//		m.setHost("SZGDJT-CAS-01.szgdjt.local");
		m.setHost("mail.sz-mtr.com");
		m.setMail_head_value("自动发送邮件");
		m.setMail_head_name("自动发送邮件");
		m.setMail_from("qianxiaowei@sz-mtr.com");
		m.setPersonalName("系统邮件,请勿回复");
//		m.setMail_subject("行政效能监督时间提醒");
		return m;
	}

	public void sendMail(boolean ifServer){
		if(ifServer){
			try {
				Properties props = new Properties();// 获取系统环境
				Authenticator auth = new Email_Autherticator();// 进行邮件服务用户认证
				props.put("mail.smtp.host", host);
				props.put("mail.smtp.auth", "true");
	//			System.out.println(props);
				Session session = Session.getDefaultInstance(props, auth);
				// 设置session,和邮件服务器进行通讯
				MimeMessage message = new MimeMessage(session);
				message.setContent(mail_body, "text/html; charset=utf-8");// 设置邮件格式
				message.setSubject(mail_subject);// 设置邮件主题
	//			message.setText(mail_body);// 设置邮件内容
				message.setHeader(mail_head_name, mail_head_value);// 设置邮件标题
				message.setSentDate(new Date());// 设置邮件发送时期
				Address address = new InternetAddress(mail_from, personalName);
				message.setFrom(address);// 设置邮件发送者的地址
	//			Address toaddress = new InternetAddress(mailto);// 设置邮件接收者的地址
				message.addRecipients(Message.RecipientType.TO, mail_to);
				if(StringUtils.isNotEmpty(mail_cc)){
					message.addRecipients(Message.RecipientType.CC, mail_cc);
				}
				if(StringUtils.isNotEmpty(mail_bcc)){
					message.addRecipients(Message.RecipientType.BCC, mail_bcc);
				}
				Transport.send(message);
				log.info("success to mail to:"+mail_to+" "+mail_cc+" "+mail_bcc);
			} catch (Exception e) {
				log.error("fail to mail to:"+mail_to+" "+mail_cc+" "+mail_bcc);
				log.error(e.getStackTrace().toString());
				log.error(e.toString());
			}
		}else{
			log.info("本地 prepared send TO:"+mail_to+" "+mail_cc+" "+mail_bcc);
		}
		// return flag;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getMail_head_name() {
		return mail_head_name;
	}

	public void setMail_head_name(String mailHeadName) {
		mail_head_name = mailHeadName;
	}

	public String getMail_head_value() {
		return mail_head_value;
	}

	public void setMail_head_value(String mailHeadValue) {
		mail_head_value = mailHeadValue;
	}

	public String getMail_to() {
		return mail_to;
	}

	public void setMail_to(String mailTo) {
		mail_to = mailTo;
	}

	public String getMail_from() {
		return mail_from;
	}

	public void setMail_from(String mailFrom) {
		mail_from = mailFrom;
	}

	public String getMail_subject() {
		return mail_subject;
	}

	public void setMail_subject(String mailSubject) {
		mail_subject = mailSubject;
	}

	public String getMail_body() {
		return mail_body;
	}

	public void setMail_body(String mailBody) {
		mail_body = mailBody;
	}

	public String getPersonalName() {
		return personalName;
	}

	public void setPersonalName(String personalName) {
		this.personalName = personalName;
	}

	public void setMail_cc(String mail_cc) {
		this.mail_cc = mail_cc;
	}

	public String getMail_cc() {
		return mail_cc;
	}

	public void setMail_bcc(String mail_bcc) {
		this.mail_bcc = mail_bcc;
	}

	public String getMail_bcc() {
		return mail_bcc;
	}
}