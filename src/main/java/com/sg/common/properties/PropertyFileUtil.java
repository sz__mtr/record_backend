package com.sg.common.properties;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.DefaultPropertiesPersister;
import org.springframework.util.PropertiesPersister;

import com.sg.common.utils.Constant;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

/**
 * 系统属性工具类
 *
 * @author HenryYan
 */
public class PropertyFileUtil {


    private static final String DEFAULT_ENCODING = "UTF-8";
    private static Logger logger = LoggerFactory.getLogger(PropertyFileUtil.class);
    private static Properties properties;
    private static PropertiesPersister propertiesPersister = new DefaultPropertiesPersister();
    private static ResourceLoader resourceLoader = new DefaultResourceLoader();
    private static Properties activePropertyFiles = null;
    private static String PROFILE_ID = StringUtils.EMPTY;
    public static boolean INITIALIZED = false; // 是否已初始化

    /**
     * 初始化读取配置文件，读取的文件列表位于classpath下面的application-files.properties<br/>
     * <p/>
     * 多个配置文件会用最后面的覆盖相同属性值
     *
     * @throws IOException 读取属性文件时
     */
    public static void init() throws IOException {
        String fileNames = "application-files.properties";
        PROFILE_ID = StringUtils.EMPTY;
//        properties=new Properties();
        innerInit(fileNames);
        activePropertyFiles(fileNames);
        INITIALIZED = true;
    }

    /**
     * 初始化读取配置文件，读取的文件列表位于classpath下面的application-[type]-files.properties<br/>
     * <p/>
     * 多个配置文件会用最后面的覆盖相同属性值
     *
     * @param profile 配置文件类型，application-[profile]-files.properties
     * @throws IOException 读取属性文件时
     */
    public static void init(String profile) throws IOException {
        if (StringUtils.isBlank(profile)) {
            init();
        } else {
            PROFILE_ID = profile;
            String fileNames = "application-" + profile + "-files.properties";
            innerInit(fileNames);
        }
        INITIALIZED = true;
    }

    /**
     * 内部处理
     *
     * @param fileName
     * @throws IOException
     */
    private static void innerInit(String fileName) throws IOException {
        String[] propFiles = activePropertyFiles(fileName);
        logger.debug("读取属性文件：{}", ArrayUtils.toString(propFiles));
        properties = loadProperties(propFiles);
        Set<Object> keySet = properties.keySet();
        for (Object key : keySet) {
            logger.debug("property: {}, value: {}", key, properties.getProperty(key.toString()));
        }
    }

    /**
     * 获取读取的资源文件列表
     *
     * @param fileName
     * @return
     * @throws IOException
     */
    private static String[] activePropertyFiles(String fileName) throws IOException {
        logger.info("读取" + fileName);
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream resourceAsStream = loader.getResourceAsStream(fileName);
        // 默认的Properties实现使用HashMap算法，为了保持原有顺序使用有序Map
        activePropertyFiles = new LinkedProperties();
        activePropertyFiles.load(resourceAsStream);

        Set<Object> fileKeySet = activePropertyFiles.keySet();
        String[] propFiles = new String[fileKeySet.size()];
        List<Object> fileList = new ArrayList<Object>();

        fileList.addAll(activePropertyFiles.keySet());
        for (int i = 0; i < propFiles.length; i++) {
            String fileKey = fileList.get(i).toString();
            propFiles[i] = activePropertyFiles.getProperty(fileKey);
        }
        return propFiles;
    }

    /**
     * 载入多个properties文件, 相同的属性在最后载入的文件中的值将会覆盖之前的载入.
     * 文件路径使用Spring Resource格式, 文件编码使用UTF-8.
     *
     * @see org.springframework.beans.factory.config.PropertyPlaceholderConfigurer
     */
    public static Properties loadProperties(String... resourcesPaths) throws IOException {
        Properties props = new Properties();

        for (String location : resourcesPaths) {

            logger.debug("Loading properties file from:" + location);

            InputStream is = null;
            try {
                Resource resource = resourceLoader.getResource(location);
                is = resource.getInputStream();
                propertiesPersister.load(props, new InputStreamReader(is, DEFAULT_ENCODING));
            } catch (IOException ex) {
                logger.info("Could not load properties from classpath:" + location + ": " + ex.getMessage());
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }
        return props;
    }

    /**
     * 获取所有的key
     *
     * @return
     */
    public static Set<String> getKeys() {
        return properties.stringPropertyNames();
    }

    /**
     * 获取键值对Map
     *
     * @return
     */
    public static Map<String, String> getKeyValueMap() {
        Set<String> keys = getKeys();
        Map<String, String> values = new HashMap<String, String>();
        for (String key : keys) {
            values.put(key, get(key));
        }
        return values;
    }

    /**
     * 获取属性值
     *
     * @param key 键
     * @return 值
     */
    public static String get(String key) {
        String propertyValue = properties.getProperty(key);
        logger.debug("获取属性：{}，值：{}", key, propertyValue);
        return propertyValue;
    }

    /**
     * 获取属性值
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 值
     */
    public static String get(String key, String defaultValue) {
        String propertyValue = properties.getProperty(key);
        String value = StringUtils.defaultString(propertyValue, defaultValue);
        logger.debug("获取属性：{}，值：{}", key, value);
        return value;
    }

    /**
     * 向内存添加属性
     *
     * @param key   键
     * @param value 值
     */
    public static void add(String key, String value) {
        properties.put(key, value);
        logger.debug("通过方法添加属性到内存：{}，值：{}", key, value);
    }

    /**
     * 读取路径下面所有配置文件信息，并放到内存中
     * @param layoutJsonPath
     */
	public static void flushCONF(String layoutJsonPath) {
		List<File> fileList = new ArrayList<File>();
		try {
			layoutJsonPath = URLDecoder.decode(layoutJsonPath, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			logger.error("路径转换出错："+e1.getMessage());
		}
		traverseFolder(layoutJsonPath, fileList);

		Constant.LOAD =null;
		for (File file : fileList) {
			Config parseURL = ConfigFactory.parseFile(file);
			if (Constant.LOAD == null) {
				Constant.LOAD = ConfigFactory.load(parseURL);
			} else {
				Constant.LOAD = Constant.LOAD.withFallback(parseURL);
			}
		}
	}

	public static void traverseFolder(String path, List<File> fileList) {
		File file = new File(path);
		if (file.exists()) {
			File[] files = file.listFiles();
			if (files.length == 0) {
				return;
			} else {
				for (File file2 : files) {
					if (file2.isDirectory()) {
						traverseFolder(file2.getAbsolutePath(), fileList);
					} else {
						fileList.add(file2);
					}
				}
			}
		} else {
			logger.error("文件不存在!" + path);
		}
	}
	
    public static Properties getActivePropertyFiles() {
        return activePropertyFiles;
    }

    public static String getProfile() {
        return PROFILE_ID;
    }
}