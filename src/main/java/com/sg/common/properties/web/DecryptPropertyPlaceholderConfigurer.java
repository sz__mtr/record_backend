package com.sg.common.properties.web;

import java.io.IOException;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import com.sg.common.utils.DesUtils;

public class DecryptPropertyPlaceholderConfigurer extends
		PropertyPlaceholderConfigurer {

	@Override
	protected String convertProperty(String propertyName, String propertyValue) {
		if (isEncryptPropertyVal(propertyName)) {
			try {
				return DesUtils.decrypt(propertyValue, "sgyyxxhk");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return "error";
			}
		}else {
			return propertyValue;
		}
	}

	private boolean isEncryptPropertyVal(String propertyName) {
		if (propertyName.equals("password")) {
			return true;
		} else {
			return false;
		}
	}
}