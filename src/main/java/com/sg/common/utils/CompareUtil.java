package com.sg.common.utils;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class CompareUtil {
	public static Object containObject(List<Object> mlist2,
			String propertyName, String compareValue)
			throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {
		for (Object m : mlist2) {
			if (ReUtils.getStr(m, propertyName).equals(compareValue)) {
				return m;
			}
		}
		return null;
	}

	public static Object containObjectForTwo(List<Object> mlist2,
			String[] propertyName, String[] compareValue)
			throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {
		for (Object m : mlist2) {
			if (ReUtils.getStr(m, propertyName[0]).equals(compareValue[0])
					&& ReUtils.getStr(m, propertyName[1]).equals(
							compareValue[1])) {
				return m;
			}
		}
		return null;
	}
}
