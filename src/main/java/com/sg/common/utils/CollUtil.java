package com.sg.common.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CollUtil {
	public static <T> boolean isNotEmpty(Collection<T> list) {
		return list != null && (!list.isEmpty());
	}
	public static <T> boolean isEmpty(Collection<T> list) {
		return list == null || list.isEmpty();
	}
	
	public static List<String> getStringMapByStartKey(Map<String, String> Map,
			String startKey) {
		List<String> values=new ArrayList<String>();
		Set<String> keySet = Map.keySet();
		for (String key : keySet) {
			if (key.startsWith(startKey)) {// 所有startKey开头的vlaue
				values.add(Map.get(key));
			}
		}
		return values;
	}
	
	public static List<Object> getObjectMapByStartKey(Map<String, Object> Map,
			String startKey) {
		List<Object> values=new ArrayList<Object>();
		Set<String> keySet = Map.keySet();
		for (String key : keySet) {
			if (key.startsWith(startKey)) {// 所有startKey开头的vlaue
				values.add(Map.get(key));
			}
		}
		return values;
	}
}
