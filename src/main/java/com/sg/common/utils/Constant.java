package com.sg.common.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.sg.common.empty.entity.PerfProperty;
import com.sg.common.empty.entity.Property;
import com.sg.common.empty.entity.TableBean;
import com.sg.common.login.entity.Member;
import com.sg.common.properties.PropertyFileUtil;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigList;

public class Constant {

	public static final String SESSION_MEMBER = "SESSION_MEMBER";
	public static final String SESSION_MEMBER_CODE = "SESSION_MEMBER_CODE";

	public static Map<String, String> CONSMAP = null;
	public static final String BEAN_TYPE = "$type$";

	public static final String SPLIT_MARK = ","; // 统一分隔标记

	public static final long DAY_SPAN = 24 * 60 * 60 * 1000;

	public static String SESSION_MARK = "m";

	public static final String ENABLE_DEL = "-2";
	public static final String ENABLE_NOR = "1";
	// public static String USRCT = "gt";//new Project's flag, <= 4 char
	// public static int MUTI_NUM = 5;//批量提交的长度。



	
	public static List<PerfProperty> PPS = new ArrayList<PerfProperty>();
	public static List<TableBean> BEANS = new ArrayList<TableBean>();

	public static final String FOOTER_HJ = "合计:";
	public static final String URL_SUFFIX = ".sg";
	// public static final String URL_START = "";
	public static final String PAGE_PARAM = "?_isp=1";
	public static final String PAGE_PARAM2 = "?_isp=2";
	public static final String EDIT = "edit";
	public static final String ADD = "add";
	public static final String DETAIL = "detail";
	public static final String EXPORT = "export";
	public static final String FIND = "find";
	public static final String PERF = "perf";
	public static final String ID = "id";
	public static final String DESCR = "descr";
	public static final String SGABS = "sgAbs";
	public static final String SESSION_MYORGS = "sessionMyOrgs";
	public static final String SESSION_MYORGS_INDIRECT = "sessionMyOrgsIndirect";
	public static final String SESSION_MANAGE_ORGS_BSY = "manageOrgsBsy";
	public static final String SESSION_MYOFFIORG = "sessionMyOffiOrg";
	public static final String SESSION_MYDEPORG = "sessionMyDepOrg";
	public static final String SESSION_MYROLES = "sessionMyRoles";
	public static final String SESSION_MYROLES_CODE = "sessionMyRolesCode";
	public static final String SESSION_MYRESTAURANT = "sessionMyRestaurantId";
	public static final String SESSION_MYWORKSHOPORG = "sessionMyWorkShopOrg";
	public static final String SESSION_MYSTATIONORG = "sessionMyStationOrg";
	// public static final String UPDATE_SUCCESS = "成功更新！";
	// public static final String ADD_SUCCESS = "成功新增！";
	// public static final String DEL_SUCCESS = "成功删除！";
	// public static final String DEL_ERR = "删除失败！";
	// public static final String FILTERVALUE_CLASSNAME = "";
	// public static String AC_MAX = "20";
	// public static String JDBC_URL = "";
	// public static String JDBC_USER ="";
	// public static String JDBC_PSD = "";
	// public static String JDBC_DRIVECLASS = "";
	public static Map<String, String> MAP = new HashMap<String, String>();
	public static Map<String, Property> BEANPROPMAP = new HashMap<String, Property>();

	public static Map<String, Property> EMPTYPROPS = new HashMap<String, Property>();

	public static Map<String, String> HOLIDAY = new HashMap<String, String>();
	
	public static Map<String, Object> ORGTREE = new HashMap<String, Object>();
	
	public static Config LOAD = null;
//	培训记录数据
	public static List<Map<String, Object>> MEMBERDATA = null;
	public static List<Map<String, Object>> ACTUALDATA = null;
	public static List<Map<String, Object>> PERSONALDATA = null;
	public static List<Map<String, Object>> CLSTDATA = null;

	// 布局配置的json文件
	public static Map<String, String> LAYOUTJSONMAP = new HashMap<String, String>();
	public static String PROPERTIES_PATH;

	@SuppressWarnings("unchecked")
	public static <T> T getAnyRef(String param) {
		if (LOAD.hasPath(param)) {
			return (T)LOAD.getAnyRef(param);
//			try {
//				if (t.newInstance() instanceof String) {
//					return (T) LOAD.getString(param);
//				} else if (t.newInstance() instanceof Config) {
//					return (T) LOAD.getConfig(param);
//				} else if (t.newInstance() instanceof Integer) {
//					return (T) (Integer) LOAD.getInt(param);
//				}
//			} catch (InstantiationException | IllegalAccessException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
		return null;
	}
	public static <T> T getT(String param,Class T) {
		if (LOAD.hasPath(param)) {
			return (T)LOAD.getAnyRef(param);
//			try {
//				if (t.newInstance() instanceof String) {
//					return (T) LOAD.getString(param);
//				} else if (t.newInstance() instanceof Config) {
//					return (T) LOAD.getConfig(param);
//				} else if (t.newInstance() instanceof Integer) {
//					return (T) (Integer) LOAD.getInt(param);
//				}
//			} catch (InstantiationException | IllegalAccessException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static <T> T getT(Config config, String param, Class<T> t) {
		if (config.hasPath(param)) {
			return (T)config.getAnyRef(param);
//			try {
//				if (t.newInstance() instanceof String) {
//					return (T) config.getString(param);
//				} else if (t.newInstance() instanceof Config) {
//					return (T) config.getConfig(param);
//				} else if (t.newInstance() instanceof Integer) {
//					return (T) (Integer) config.getInt(param);
//				}
//			} catch (InstantiationException | IllegalAccessException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
		return null;
	}

	public static String get(String param) {
		String loadValue = null;
		if (LOAD.hasPath(param)) {
			loadValue = LOAD.getString(param);
		}
		return loadValue;
	}

	public static String get(Config config, String param) {
		String loadValue = null;
		if (config.hasPath(param)) {
			loadValue = config.getString(param);
		}
		return loadValue;
	}

	public static Config getConfig(String param) {
		Config loadValue = null;
		if (LOAD.hasPath(param)) {
			loadValue = LOAD.getConfig(param);
		}
		return loadValue;
	}

	public static ConfigList getList(String param) {
		ConfigList loadValue = null;
		if (LOAD.hasPath(param)) {
			loadValue = LOAD.getList(param);
		}
		return loadValue;
	}

	public static ConfigList getList(Config config, String param) {
		ConfigList loadValue = null;
		if (config.hasPath(param)) {
			loadValue = config.getList(param);
		}
		return loadValue;
	}

	public static List<Config> getConfigList(String param) {
		List<Config> loadValue  = new ArrayList<Config>();
		if (LOAD.hasPath(param)) {
			loadValue = (List<Config>) LOAD.getConfigList(param);
		}
		return loadValue;
	}

	public static List<Object> getObjectList(String param) {
		List<Object> loadValue  = new ArrayList<Object>();
		if (LOAD.hasPath(param)) {
			loadValue = (List<Object>) LOAD.getObjectList(param);
		}
		return loadValue;
	}

	public static Integer getInt(String param) {
		Integer loadValue = null;
		if (LOAD.hasPath(param)) {
			loadValue = LOAD.getInt(param);
		}
		return loadValue;
	}

	public static <T> List<T>  getAnyRefList(String param) {
		List<T> loadValue = new ArrayList();
		if (LOAD.hasPath(param)) {
			loadValue = (List<T>) LOAD.getAnyRefList(param);
		}
		return loadValue;
	}

	public static List<String> getStringList(String param) {
		List<String> loadValue = new ArrayList<String>();
		if (LOAD.hasPath(param)) {
			loadValue = LOAD.getStringList(param);
		}
		return loadValue;
	}

	public static List<String> getStringList(Config config, String param) {
		List<String> loadValue = new ArrayList<String>();
		if (config.hasPath(param)) {
			loadValue = config.getStringList(param);
		}
		return loadValue;
	}

	public static Object getObject(String param) {
		Object loadValue = null;
		if (LOAD.hasPath(param)) {
			loadValue = LOAD.getObject(param);
		}
		return loadValue;
	}

	public static boolean hasPath(String param) {
		return LOAD.hasPath(param);
	}

	public static Map<String, String> getStart(String suffix) {
		Map<String, String> res = new HashMap<String, String>();
		Set<String> keySet = PropertyFileUtil.getKeys();
		for (String key : keySet) {
			if (key.startsWith(suffix)) {// 所有suffix开头的property
				res.put(key, PropertyFileUtil.get(key));
			}
		}
		return res;
	}

	// 获取当前项目的绝对路径
	public static String getPorjectPath() {
		String nowpath; // 当前tomcat的bin目录的路径 如
						// D:/java/software/apache-tomcat-6.0.14/bin
		String tempdir;
		nowpath = System.getProperty("user.dir");
		tempdir = nowpath.replace("bin", "webapps"); // 把bin 文件夹变到 webapps文件里面
		tempdir += "/" + getT("PROJECT_NAME", String.class); // 拼成D:/java/software/apache-tomcat-6.0.14/webapps/sz_pro
		return tempdir;
	}

	public static String getSessionMemberCode(HttpServletRequest request) {
		if (request != null) {
			return (String) request.getSession().getAttribute(
					Constant.SESSION_MEMBER_CODE);
		} else {
			return "";
		}
		// return "203187";//pw
		// return "201582";//ab
	}

	public static Member getSessionMember(HttpServletRequest request) {
		if (request != null) {
			return (Member) request.getSession().getAttribute(
					Constant.SESSION_MEMBER);
		} else {
			return null;
		}
	}

	public static Member getSessionMember() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
		return getSessionMember(request);
	}
	
	public static String getRbString(Map<String, Object> rb,String key) {
		Object value=rb.get(key);
		if(value!=null)
		{
			return String.valueOf(value);
		}
		return "";
	}
	
	@SuppressWarnings("unchecked")
	public static List<String> getRbList(Map<String, Object> rb,String key) {
		Object value=rb.get(key);
		if(value!=null)
		{
			return (List<String>)value;
		}
		return new ArrayList<String>();
	}
}
