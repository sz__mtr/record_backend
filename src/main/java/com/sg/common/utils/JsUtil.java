package com.sg.common.utils;

public class JsUtil {

	static final String SCRIPT_START = "<script>";
	static final String SCRIPT_END = "</script>";
	public static StringBuilder addScript(StringBuilder fucs) {
		StringBuilder res = new StringBuilder();
		res.append(SCRIPT_START).append(fucs).append(SCRIPT_END);
		return res;
	}
}
