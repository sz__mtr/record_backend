package com.sg.common.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class StrUtil {
	
	public static boolean requestParamsNotEmpty(String s){
		return StringUtils.isNotEmpty(s)&&(!"null".equals(s));
	}

	public static List<String> split(String oran){
		List<String> res = new ArrayList<String>();
		String[] ds = oran.split(Constant.SPLIT_MARK);
		for (String d : ds) {
			if (StringUtils.isNotEmpty(d))
				res.add(d);
		}
		return res;
	}
	/**
	 * str1是否包含在str2中。 eg:{str1[mn_1];str2[mn_0,mn_1];return true}
	 * {str1[mn_1];str2[mn_0,mn_1_1];return false}
	 * 
	 * @param str1
	 * @param str2
	 * @return true（包含）/false（不包含）
	 */
	public static boolean contains(String str1, String str2) {
		boolean result = false;
		if (str1 != null && str2 != null) {
			String commaUserFlag = commaString(str2);
			int index = commaUserFlag.indexOf(commaString(str1));
			if (index > -1) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * eg:{str1[mn_1mn_2];str2[mn_0,mn_1];return mn_1}
	 * {str1[mn_3mn_2];str2[mn_0,mn_1];return ""}
	 * 
	 * @param str1
	 *            长字符串
	 * @param str2
	 *            字符串列表
	 * @return
	 */
	public static String containsStr(String str1, String str2) {
		String[] split = str2.split(",");
		for (String s : split) {
			if (str1.contains(s)) {
				return s;
			}
		}
		return "";
	}

	/**
	 * string两端加【，】,<br>
	 * eg:{str1[mn_1];return [,mn_1,]}
	 * 
	 * @param string
	 * @return
	 */
	private static String commaString(String string) {
		StringBuilder sb = new StringBuilder(",");
		sb.append(string);
		sb.append(",");
		return sb.toString();
	}

	/**
	 * 把一个xx,yy,zz的字符串变成list
	 * 
	 * @return
	 */
	public static List<String> strToList(String oran) {
		List<String> res = new ArrayList<String>();
		if (StringUtils.isNotEmpty(oran)) {
			String[] split = oran.split(",");
			for (String s : split) {
				if (StringUtils.isNotEmpty(s)) {
					res.add(s);
				}
			}
		}
		return res;
	}

	/**
	 * 返回yy在xx,yy,zz的字符串中的位置,此yy的位置为1 不存在则返回-1
	 * 
	 * @return
	 */
	public static int getIndexInStr(String s, String oran) {
		int res = -1;
		res = strToList(oran).indexOf(s);
		return res;
	}

	/**
	 * 把, ,xx, ,yy,zz, ,变成xx,yy,zz,
	 * 
	 * @param oran
	 * @return
	 */
	public static String trimStr(String oran) {
		StringBuilder res = new StringBuilder();
		if (StringUtils.isNotEmpty(oran)) {
			String[] split = oran.split(",");
			for (String s : split) {
				if (StringUtils.isNotEmpty(s)) {
					res.append(s.trim()).append(",");
				}
			}
		}
		return res.toString();
	}

	/**
	 * 把s加入pid中, s="5";pid="2,3,";return "2,3,5,"; s="3";pid="2,3,";return
	 * "2,3,";
	 * 
	 * @param s
	 * @param oran
	 */
	public static String addStr(String s, String oran) {
		if (oran == null) {
			oran = "";
		}
		StringBuilder res = new StringBuilder(oran);
		if (commaString(oran).indexOf(commaString(s)) == -1) {
			res.append(s).append(",");
		}
		return res.toString();
	}

	/**
	 * 在pid中减去s s="5";pid="2,3,";return "2,3,"; s="2";pid="2,3,";return "3,";
	 * 
	 * @param s
	 * @param oran
	 */
	public static String minusStr(String s, String oran) {
		if (StringUtils.isNotEmpty(oran)) {
			return trimStr(commaString(oran).replaceAll(commaString(s), ","));
		}
		return oran;
	}

	/**
	 * 得到oran中有几个有效值 oran="1,";return 1; oran=", ,1,2,,3,,,";return 3;
	 * 
	 * @param pid
	 * @return
	 */
	public static int strNum(String oran) {
		int res = 0;
		if (StringUtils.isNotEmpty(oran)) {
			String[] split = oran.split(",");
			for (String s : split) {
				if (StringUtils.isNotEmpty(s)) {
					res++;
				}
			}
		}
		return 0;
	}

	/**
	 * 
	 * @param oran
	 *            1,3-5,
	 * @return 1,3,4,5
	 */
	public static List<String> spliter(String oran) {
		List<String> res = new ArrayList<String>();
		if (StringUtils.isNotEmpty(oran)) {
			String[] split = oran.split(",");
			for (String s : split) {
				if (s.contains("-")) {
					String[] split2 = s.split("-");
					int from = Integer.valueOf(split2[0]);
					int to = Integer.valueOf(split2[1]);
					for (int i = from; i <= to; i++) {
						res.add(String.valueOf(i));
					}
				} else {
					res.add(s);
				}
			}

		}
		return res;
	}

	public static String dot(String res, String... ss) {
		for(String i:ss){
			res += "."+i;
		}
		return res;
	}
}
