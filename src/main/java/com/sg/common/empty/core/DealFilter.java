package com.sg.common.empty.core;

import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.sg.common.login.entity.Member;
import com.sg.common.utils.Constant;

/**
 * 默认permission的处理方法，其他个性化处理通过继承此方法进行处理
 * 
 * @author wangze
 * 
 */
public class DealFilter {

	HttpServletRequest request;
	HttpServletResponse response;

	String descr;
	
	//分为js类型或js文件路径类型
	String type;
	
	String func;
	// 处理类的名称
	String dealClass;

	public boolean getDataIn() throws FileNotFoundException {
		boolean datain = true;
		Object bean = request.getAttribute("bean");
		Member member = (Member) request.getSession().getAttribute(
				Constant.SESSION_MEMBER);
		if (StringUtils.equalsIgnoreCase(type, "js")) {
			datain = false;
			if (StringUtils.isNotEmpty(func)) {
				ScriptEngineManager factory = new ScriptEngineManager();// step
				ScriptEngine engine = factory.getEngineByName("JavaScript");// Step
				engine.put("bean", bean);
				engine.put("request", request);
				try {
					engine.eval(func);
					datain = (Boolean) engine.get("cando");
				} catch (ScriptException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else if (StringUtils.equalsIgnoreCase(type, "jsFile")) {
			datain = false;
			FileReader fr = new FileReader(func);
			ScriptEngineManager factory = new ScriptEngineManager();// step
			ScriptEngine engine = factory.getEngineByName("JavaScript");// Step
			engine.put("bean", bean);
			engine.put("request", request);
			try {
				engine.eval(fr);
				datain = (Boolean) engine.get("cando");
			} catch (ScriptException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return datain;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFunc() {
		return func;
	}

	public void setFunc(String func) {
		this.func = func;
	}

	public String getDealClass() {
		return dealClass;
	}

	public void setDealClass(String dealClass) {
		this.dealClass = dealClass;
	}

}
