package com.sg.common.empty.core;

public class NameValuePair {
	String id;
	String text;
	public String getId() {
		return id;
	}
	public void setId(String name) {
		this.id = name;
	}
	public String getText() {
		return text;
	}
	public void setText(String value) {
		this.text = value;
	}
	public void setValue(int value) {
		this.text = String.valueOf(value);
	}
	public NameValuePair(String name, String value) {
		super();
		this.id = name;
		this.text = value;
	}
	public NameValuePair() {
		super();
	}
}
