package com.sg.common.empty.core;

import com.sg.common.empty.service.CfgService;

/**
 * 客户化的过滤都要继承该类
 * @author qianxiaowei
 *
 */
public abstract class FilterCus {

	private CfgService service;
	
	abstract Object get();

	public CfgService getService() {
		return service;
	}

	public void setService(CfgService service) {
		this.service = service;
	}
}
