package com.sg.common.empty.core;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.sg.common.empty.entity.PerfProperty;
import com.sg.common.empty.json.AttaList;
import com.sg.common.empty.json.Permission;
import com.sg.common.empty.json.SecList;
import com.sg.common.empty.json.SectionDraw;
import com.sg.common.empty.json.SectionListDraw;
import com.sg.common.empty.service.CfgService;

public class DetailBuilder {
	String url;
	Object bean;
	Object tableBean;
	private CfgService service;
	Map<String, Object> map = new HashMap<String, Object>();
	List<Object> listBean;
//	Perf perf;
	String beanId;
	String route;
	String ids;
	List<Permission> specialPermission;
	List<Permission> perfPermission;

	List<SectionDraw> sds;
	SecList[] sls;
	AttaList attaList;
	List<SectionListDraw> slds;
	String taskId;

	public void init(HttpServletRequest request) throws IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		url = request.getRequestURI();
//		bean = request.getAttribute("bean");
//		tableBean = request.getAttribute("tableBean");
		route = (String) request.getAttribute("conf.route");
//		perf = (Perf) request.getAttribute("perf");
//		beanId = ReUtils.getStr(bean, "id");
//		ids = request.getParameter("ids");
//		specialPermission = (List<Permission>) request
//				.getAttribute("specialPermission");
//		perfPermission = (List<Permission>) request
//				.getAttribute("perfPermission");
	}
//
//	public StringBuilder ndetail() throws ClassNotFoundException,
//			SecurityException, NoSuchFieldException, IllegalArgumentException,
//			IllegalAccessException, InvocationTargetException,
//			NoSuchMethodException {
//		StringBuilder res = new StringBuilder();
//		JspUtil3.fixTop(url, beanId, ids, res, "", perf.getId(),
//				specialPermission, perfPermission);
//
//		for (SectionDraw sd : sds) {
//			List<PerfProperty> pps = sd.getPps();
//			for (PerfProperty pp : pps) {
//				pp.setEsh("1");
//			}
//		}
//		JspUtil3.drawLayoutSecTable(bean, res, sds);
//
//		JspUtil3.drawLayoutAttaList(bean, res, attaList);
//
//		// 流程历史查看
//		SecList[] nsls = EpUtil.flowAttachment(tableBean);
//		JspUtil3.drawLayoutSecList(bean, res, sls, nsls);
//
//		return res;
//	}

	public StringBuilder emptyNdetail() throws ClassNotFoundException,
			SecurityException, NoSuchFieldException, IllegalArgumentException,
			IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {
		StringBuilder res = new StringBuilder();
		JspUtil3.emptyFixTop(route, beanId, ids, res, "");

		for (SectionDraw sd : sds) {
			List<PerfProperty> pps = sd.getPps();
			for (PerfProperty pp : pps) {
				pp.setEsh("1");
			}
		}

		JspUtil3.emptyDrawLayoutSecTable(map, res, sds);

//		JspUtil3.drawLayoutAttaList(bean, res, attaList);
//
//		// 流程历史查看
//		SecList[] nsls = CfgUtil.flowAttachment(tableBean);
//		JspUtil3.drawLayoutSecList(bean, res, sls, nsls);

		return res;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Object getBean() {
		return bean;
	}

	public void setBean(Object bean) {
		this.bean = bean;
	}

	public Object getTableBean() {
		return tableBean;
	}

	public void setTableBean(Object tableBean) {
		this.tableBean = tableBean;
	}

	public CfgService getService() {
		return service;
	}

	public void setService(CfgService service) {
		this.service = service;
	}

	public List<Object> getListBean() {
		return listBean;
	}

	public void setListBean(List<Object> listBean) {
		this.listBean = listBean;
	}

//	public Perf getPerf() {
//		return perf;
//	}
//
//	public void setPerf(Perf perf) {
//		this.perf = perf;
//	}

	public String getBeanId() {
		return beanId;
	}

	public void setBeanId(String beanId) {
		this.beanId = beanId;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public List<Permission> getSpecialPermission() {
		return specialPermission;
	}

	public void setSpecialPermission(List<Permission> specialPermission) {
		this.specialPermission = specialPermission;
	}

	public List<Permission> getPerfPermission() {
		return perfPermission;
	}

	public void setPerfPermission(List<Permission> perfPermission) {
		this.perfPermission = perfPermission;
	}

	public List<SectionDraw> getSds() {
		return sds;
	}

	public void setSds(List<SectionDraw> sds) {
		this.sds = sds;
	}

	public SecList[] getSls() {
		return sls;
	}

	public void setSls(SecList[] sls) {
		this.sls = sls;
	}

	public List<SectionListDraw> getSlds() {
		return slds;
	}

	public void setSlds(List<SectionListDraw> slds) {
		this.slds = slds;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public AttaList getAttaList() {
		return attaList;
	}

	public void setAttaList(AttaList attaList) {
		this.attaList = attaList;
	}

	public Map<String, Object> getMap() {
		return map;
	}

	public void setMap(Map<String, Object> map) {
		this.map = map;
	}

	public String getconfHeader() {
		return route;
	}

	public void setconfHeader(String confHeader) {
		this.route = confHeader;
	}

}
