package com.sg.common.empty.core;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.sg.common.empty.service.CfgService;
import com.sg.common.utils.Constant;

public class Params {
	HttpServletRequest request;
	CfgService service;

	public static Params init(HttpServletRequest request, CfgService service) {
		Params res = new Params();
		res.request = request;
		res.service = service;
		return res;
	}

	public List<String> build() {
		String route = (String) request.getAttribute("conf.route");
		List<String> ps = Constant.getStringList(route + ".params");
		List<String> sqlParams = new ArrayList<String>();
		for (String param : ps) {
			String beanValue = "";
			if (StringUtils.contains(param, "`")) {
				String[] split = StringUtils.split(param, "`");
				if (StringUtils.equals(split[0], "request")) {
					beanValue = request.getParameter(split[1]);
				} else if (StringUtils.equals(split[0], "requests")) {
					String[] temp = request.getParameterValues(split[1]);
					if (temp != null) {
						for (String t : temp) {
							beanValue = t + ",";
						}
					}
					if (StringUtils.isNotEmpty(beanValue)) {
						beanValue = StringUtils.substring(beanValue, 0,
								beanValue.length() - 1);
					}
				}
			} else {
				beanValue = (String) FilterVal.init(param, service).get2();
			}
			sqlParams.add(beanValue);
		}
		return sqlParams;
	}
}
