package com.sg.common.empty.core;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang3.StringUtils;

import com.sg.common.empty.entity.Myinput;
import com.sg.common.empty.entity.PerfProperty;
import com.sg.common.empty.entity.Property;
import com.sg.common.utils.Constant;

public class MyinputBuilder {
	PerfProperty pp;
	String[] beanValue;
	String from;
	boolean clearDescr;
	String suff;

	public MyinputBuilder(PerfProperty pp, String[] beanValue, String from,
			boolean clearDescr, String suff) {
		super();
		this.pp = pp;
		this.beanValue = beanValue;
		this.from = from;
		this.clearDescr = clearDescr;
		this.suff = suff;
	}

	public MyinputBuilder(PerfProperty pp, String[] beanValue,
			boolean clearDescr, String suff) {
		super();
		this.pp = pp;
		this.beanValue = beanValue;
		this.clearDescr = clearDescr;
		this.suff = suff;
	}

	public StringBuilder findInput() throws IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		StringBuilder res = new StringBuilder();
		String val0 = "";
		String val1 = "";
		if (beanValue != null) {
			val0 = beanValue[0];
			val1 = beanValue[1];
		}
		Property property = pp.getProperty();

		Myinput myinput = JspUtil3.propertyMyinput(pp);
		String esh = pp.getEsh();
		String field = property.getProperty();
		if (StringUtils.isNotEmpty(suff)) {
			field = suff + "." + property.getProperty();
		}
		if (StringUtils.isNotEmpty(property.getVotype())) {// 对于类搜索，类的id作为搜索条件
			field += ".id";
		}
		if ("0".equals(esh)) {
			res.append("<input type='hidden' name='").append(field)
					.append("' value='").append(val1).append("'>");
		} else if ("1".equals(esh)) {
			if (!clearDescr) {
				if(StringUtils.isNotEmpty(pp.getDescr()))
					res.append(pp.getDescr()).append(":");
				else
					res.append(property.getDescr()).append(":");
			}
			res.append(val1);
			// res.append("<input type='hidden' name='") //2016.8
			// -删除-若需要复合显示，即描述与hidden都显示，则在pn中复写即可
			// .append(field).append("' value='").append(val1)
			// .append("'>");
		} else if ("2".equals(esh) || ("3".equals(esh))) {
			if (myinput != null) {
				if (!clearDescr)
					if(StringUtils.isNotEmpty(pp.getDescr()))
						res.append(pp.getDescr()).append(":");
					else
					res.append(property.getDescr()).append(":");
				String type = myinput.getType();
				// String params = myinput.getParams();
				String url = Constant.get("URL")
						+ JspUtil3.propertyValue(pp, "valueurl");
				String dateOptionStr = "";
				String validateParams = "";// 用在只需要required的select、tree、pop
				if (!StringUtils.isNotEmpty(from)) {
					String max = JspUtil3.propertyValue(pp, "max");
					String min = JspUtil3.propertyValue(pp, "min");
					if (!StringUtils.isNotEmpty(max)
							&& StringUtils.isNotEmpty(property.getLength())) {
						max = property.getLength();
					}
					if (!StringUtils.isNotEmpty(min)) {
						min = "0";
					}
					String required = "";
					String requiredPv = JspUtil3.propertyValue(pp, "required");
					if ("1".equals(requiredPv)) {
						required = "required:true,";
						res.append("<font color=red>*</font>");
					}
					dateOptionStr = " data-options=\"" + required
							+ "validType:[";
					String validType = JspUtil3.propertyValue(pp, "validtype");
					if (StringUtils.isNotEmpty(validType)) {
						dateOptionStr += " '" + validType + "',";

					}
					if (StringUtils.isNotEmpty(max)) {
						dateOptionStr += "'length[" + min + "," + max
								+ "]']\" ";
					}
					dateOptionStr += "]\" ";

					if ("1".equals(requiredPv)) {
						validateParams = " required='true' ";
					}
				}
				if ("text".equals(type)) {
					res.append("<input class='easyui-validatebox'")
							.append(dateOptionStr)
							.append(" type='text' name='").append(field)
							.append("' value='").append(val1).append("'>");
				} else if ("textarea".equals(type)) {
					if ("toolbar".equals(from)) {
						res.append("<input class='easyui-validatebox'")
								.append(dateOptionStr)
								.append(" type='text' name='").append(field)
								.append("' value='").append(val1).append("'>");
					} else {
						res.append("<textarea class='easyui-validatebox'")
								.append(dateOptionStr)
								.append(" style='width: 699px; height: 267px;' name='")
								.append(field).append("'>").append(val1)
								.append("</textarea>");
					}
				} else {
					String idName = field.replace(".", "");
					if ("select".equals(type)) {
						res.append("<input ")
								.append(validateParams)
								.append(" id='")
								.append(idName)
								.append("' name='")
								.append(field)
								.append("' data-options='url: \"")
								.append(url)
								.append("\",valueField: \"id\",textField: \"text\",panelWidth: 350,editable: true");
						res.append("'>");
						setValueToInput(res, val1, field);
						// String js = propertyJs(pp, property);
						// if (StringUtils.isNotEmpty(js)) {
						// String com = "$('input[name=\"" + field + "\"]')";
						// res.append("<script language='JavaScript'>")
						// .append(com).append(".combobox({").append(js)
						// .append("});</script>");
						// }
						String com = "$('input[name=\"" + field + "\"]')";
						res.append("<script language='JavaScript'>")
								.append(com)
								.append(".combobox({")
								.append("filter: function(q, row){var opts = $(this).combobox('options');	return row[opts.textField].indexOf(q)>-1;	}")
								.append("});</script>");
					} else if ("sdate".equals(type)) {
						// 来自工具栏的搜索框，对日期搜索进行范围搜索
						res.append("<input type='text' id='").append(field)
								.append("' ").append(dateOptionStr)
								.append("' name='").append(field)
								.append("' size='25'>");
						res.append(
								"<script type='text/javascript' charset='utf-8'>var ")
								.append(idName)
								.append(" = new Kalendae.Input('")
								.append(field)
								.append("', {months:2,mode:'range',format:'YYYY-MM-DD'});</script>");
						setValueToInput(res, val1, field);
					} else if ("date".equals(type)) {
						res.append("<input class='easyui-datebox'")
								.append(dateOptionStr).append(" name='")
								.append(field).append("'>");
						setValueToInput(res, val1, field);
					} else if ("pop".equals(type)) {
						res.append("<input class='easyui-validatebox'")
								.append(validateParams)
								.append(" readonly='readonly' type='text' id='text_")
								.append(field).append("' name='text_")
								.append(field).append("' value='").append(val0)
								.append("'><input type='hidden' id='")
								.append(field).append("' name='").append(field)
								.append("' value='").append(val1).append("'>");
						// EDIT
						url = url + "&_controlName=" + field;
						String pop_params = "'" + field + "','" + url + "'";
						String pop_clear_params = "'" + field + "'";
						res.append(
								"<a href='javascript:void(0)' onclick=\"javascript:pop(")
								.append(pop_params)
								.append(")\" >选择&nbsp;</a><a href='javascript:void(0)' onclick=\"javascript:pop_clear(")
								.append(pop_clear_params).append(")\" >清空</a>");
						// res.append("<script language='JavaScript'>");
						// res.append("\r function pop(field,url){digStr='dialogHeight:900px;dialogWidth:1000px;center:yes';var ReturnValue = window.showModalDialog(url,'',digStr);if(ReturnValue!=null&&ReturnValue!=''&&ReturnValue!='null'){\r var ind = ReturnValue.indexOf(';');var text=ReturnValue.substring(0,ind);var value=ReturnValue.substring(ind+1,ReturnValue.length);var ft =$('input[name=\"text_'+field+'\"]');var fv =$('input[name=\"'+field+'\"]');ft.val(text);fv.val(value);}}");
						// res.append("function pop_clear(field){var ft =$('input[name=\"text_'+field+'\"]');var fv =$('input[name=\"'+field+'\"]');ft.val('');fv.val('');}");
						// res.append("</script>");
						res.append("<script language='JavaScript'>");
						res.append("\r function pop(field,url){digStr='dialogHeight:900px;dialogWidth:1000px;center:yes';window.open(url,'',digStr);}");
						res.append("function pop_clear(field){var ft =$('input[name=\"text_'+field+'\"]');var fv =$('input[name=\"'+field+'\"]');ft.val('');fv.val('');}");
						res.append("</script>");
					}
					// else if ("radio".equals(type)) {
					// res.append("<input type='radio' value='1' name='")
					// .append(field).append("'>是")
					// .append("<input type='radio' value='0' name='")
					// .append(field).append("'>否")
					// .append("<input type='radio' value='' name='")
					// .append(field).append("'>空");
					// String com = "$('input[name=\"" + field + "\"][value=\""
					// + val1 + "\"]')";
					// res.append("<script language='JavaScript'>").append(com)
					// .append(".attr('checked','checked');</script>");
					// }
					else if ("radio".equals(type)) {
						res.append("<input class='easyui-combobox'")
								.append(validateParams)
								.append(" id='")
								.append(idName)
								.append("' name='")
								.append(field)
								.append("' data-options='url: \"cfg/Cons/select?_domain=yorn\",valueField: \"id\",textField: \"text\",panelWidth: 350,editable: true");
						res.append("'>");
						setValueToInput(res, val1, field);
						// String js = propertyJs(pp, property);
						// if (StringUtils.isNotEmpty(js)) {
						// String com = "$('input[name=\"" + field + "\"]')";
						// res.append("<script language='JavaScript'>")
						// .append(com).append(".combobox({").append(js)
						// .append("});</script>");
						// }
					} else if ("radioEnable".equals(type)) {
						res.append("<input class='easyui-combobox'")
								.append(validateParams)
								.append(" id='")
								.append(idName)
								.append("' name='")
								.append(field)
								.append("' data-options='url: \"cfg/Cons/select?_domain=enable\",valueField: \"id\",textField: \"text\",panelWidth: 350,editable: true");
						res.append("'>");
						setValueToInput(res, val1, field);
					} else if ("tree".equals(type)) {
						url = EpUtil.addParam(url, "endTreeValue=" + val1);
						res.append("<input class=' easyui-combotree'")
								.append(validateParams).append(" name='")
								.append(field)
								.append("' data-options='url: \"").append(url)
								// .append("?endTreeValue=").append(val2)
								.append("\",editable:true,panelWidth: 350")
								.append("'>");
						setValueToInput(res, val1, field);
					} else if ("numberbox".equals(type)) {
						res.append(
								"<input class='easyui-numberbox' style='width:50px;'")
								.append(dateOptionStr).append(" name='")
								.append(field).append("' value='").append(val1)
								.append("'>");
					} else if ("double2".equals(type)) {
						res.append(
								"<input class='easyui-numberbox' precision='2' ")
								.append(dateOptionStr).append(" name='")
								.append(field).append("' value='").append(val1)
								.append("'>");
					} else if ("file".equals(type)) {
						res.append(
								"<input class='easyui-validatebox' type='file' ")
								.append(dateOptionStr)
								.append(" name='upload' >");
						res.append("<input type='hidden' name='").append(field)
								.append("' value='").append(val1).append("'>");
					} else if ("editor".equals(type)) {
						res.append("<script type='text/javascript' charset='utf-8' src='ueditor/ueditor.config.js'></script>"
								+ "<script type='text/javascript' charset='utf-8' src='ueditor/ueditor.all.min.js'> </script>"
								+ "<script type='text/javascript' charset='utf-8' src='ueditor/lang/zh-cn/zh-cn.js'></script>");
						res.append("<script id='editor' type='text/plain' style='width:1024px;height:500px;'></script>");
						String com = "$('input[name=\"" + field + "\"]')";
						res.append("<script type='text/javascript'>var ue = UE.getEditor('editor');ue.ready(function() {ue.setContent('"
								+ val1
								+ "','');});ue.addListener('contentChange',function(){"
								+ com + ".val(ue.getContent())});</script>");
						res.append("<input type='hidden' name='").append(field)
								.append("'  value='").append(val1).append("'>");
					} else if ("multiselect".equals(type)) {
						res.append("<input ")
								.append(validateParams)
								.append(" id='")
								.append(idName)
								.append("' name='")
								.append(field)
								.append("' data-options='url: \"")
								.append(url)
								.append("\",valueField: \"id\",textField: \"text\",multiple:true,panelWidth: 350,editable: true");
						res.append("'>");
						String com = "$('input[name=\"" + field + "\"]')";
						if (StringUtils.isNotEmpty(val1)) {
							String mv = "[";
							String[] vals = StringUtils.split(val1, " , ");
							for (String v : vals) {
								mv += "'" + v + "',";
							}
							mv = mv.substring(0, mv.length() - 1) + "]";
							String com_id = "$('#" + idName + "')";
							res.append("<script language='JavaScript'>")
									.append(com)
									.append(".combobox({")
									.append("filter: \r function(q, row) \n {var opts = $(this).combobox('options'); \n	return row[opts.textField].indexOf(q)>-1; \n	}")
									.append(",onLoadSuccess: function(param){")
									.append(com_id)
									.append(".combobox('setValues',")
									.append(mv).append(");}});</script>");
						}
						res.append("<script language='JavaScript'>")
								.append(com)
								.append(".combobox({")
								.append("filter: \r function(q, row) \n {var opts = $(this).combobox('options'); \n	return row[opts.textField].indexOf(q)>-1; \n	}")
								.append("});</script>");
						// res.append("<script language='JavaScript'>function "+idName+"(){$('#idst').combobox('setValues',").append(mv).append(");}</script>");
						// res.append("<input type='button' onclick='"+idName+"()'>");
						// //return row[opts.textField].indexOf(q)>-1;
					} else if (StringUtils.startsWith(type, "select2")) {
						res.append("<script src='select2-master/dist/js/select2.js'></script><link href='select2-master/dist/css/select2.css' rel='stylesheet' />");

						res.append("<select name='")
								.append(field)
								.append("' id='")
								.append(idName)
								.append("' class='js-data-example-ajax form-control')");
						if (StringUtils.equals(type, "select2_m")) {
							res.append(" multiple='multiple' ");
						}
						res.append("></select>");
						if (StringUtils.isNotEmpty(val1)) {
							res.append("<script src='js/select2SetVal.js'></script>");
							res.append("<script language='JavaScript'>select2SetVal('"
									+ val1
									+ "','"
									+ idName
									+ "','"
									+ url
									+ "');</script>");
						} else {
							res.append("<script src='js/select2Init.js'></script>");
							res.append("<script language='JavaScript'>selectInit('"
									+ idName + "','" + url + "');</script>");
						}
					}
				}
			}
		}
		return res;
	}

	void setValueToInput(StringBuilder res, String val2, String field) {
		String com = "$('input[name=\"" + field + "\"]')";
		res.append("<script language='JavaScript'>").append(com)
				.append(".val('").append(val2).append("');</script>");
	}

}
