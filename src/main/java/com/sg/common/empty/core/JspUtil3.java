package com.sg.common.empty.core;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.sg.common.empty.entity.Myinput;
import com.sg.common.empty.entity.PerfProperty;
import com.sg.common.empty.entity.Property;
import com.sg.common.empty.json.AttaList;
import com.sg.common.empty.json.Permission;
import com.sg.common.empty.json.SecList;
import com.sg.common.empty.json.SectionDraw;
import com.sg.common.utils.CollUtil;
import com.sg.common.utils.Constant;
import com.sg.common.utils.Generate;
import com.sg.common.utils.JsUtil;
import com.sg.common.utils.ReUtils;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigList;
import com.typesafe.config.ConfigValue;

public class JspUtil3 {

	public static String linkButton = "<a href='javascript:void(0)' class='easyui-linkbutton' data-options='plain:true' onclick='";
	public static final String ROWNOTSELECTED_TRIP = "请在表格中选中需要操作的一行！";
	public static final String ADD_EDIT_TABLE = "<table class='horTable' border='1px;'>";


	/**
	 * 保存时提交的js 提交时信息验证 用于显示返回信息
	 * 
	 * @param url
	 * @return
	 */
	public static StringBuilder findSaveSubmitJs() {
		StringBuilder res = new StringBuilder();
		res.append("$(function(){$('#df_form').form({onSubmit:function(){if($(this).form('validate')){ $.messager.progress({	title : 'Please waiting',msg : 'Loading data...'});$('#df_sub').attr('disabled','false'); } return $(this).form('validate');},success:function(ht){ $.messager.progress('close'); $('#df_action_msg').html('<font color=green>'+ht+'</font>'); $('#df_sub').removeAttr('disabled');}});});");
		return res;
	}

	/**
	 * 根据List内容 画出一个List的tableHTML
	 * 
	 * @param className
	 * @param psid
	 * @return
	 * @throws ClassNotFoundException
	 * @throws NoSuchFieldException.
	 * @throws IllegalAccessException
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchMethodException
	 * @throws InvocationTargetException
	 */
	public static InputStream drawXls(List<PerfProperty> listPl, List<Object> cl)
			throws ClassNotFoundException, NoSuchFieldException,
			IllegalAccessException, UnsupportedEncodingException,
			InvocationTargetException, NoSuchMethodException {
		StringBuilder tableHtmlBuilder = new StringBuilder();
		tableHtmlBuilder
				.append("<html><head><style type='text/css'>td {mso-number-format:\"\\@\";}h1 {font-family: Arial, sans-serif;font-size: 24px;margin: 20px;color: #369;padding-bottom: 4px;border-bottom: 1px solid #999;text-align:center}h3 {margin-left: 20px;color:#0099FF;}table{border-collapse: collapse;font-family: 'Lucida Sans Unicode','Lucida Grande',Sans-Serif;font-size: 12px;margin: 20px;text-align: left;width: 97%};td {padding: 8px;}</style></head><body>");
		tableHtmlBuilder.append("<table border='1' class='horTable'>");
		tableHtmlBuilder.append("<tr>");
		for (PerfProperty pp : listPl) {
			tableHtmlBuilder.append("<td>").append(pp.getProperty().getDescr())
					.append("</td>");
		}
		tableHtmlBuilder.append("</tr>");
		for (Object c : cl) {
			tableHtmlBuilder.append("<tr>");
			for (PerfProperty pp : listPl) {
				String beanValue = "";
				String field = pp.getProperty().getProperty();
				beanValue = ReUtils.getStr(c, field);
				tableHtmlBuilder.append("<td>").append(beanValue)
						.append("</td>");
			}
			tableHtmlBuilder.append("</tr>");
		}
		tableHtmlBuilder.append("</table></body></html>");
		// byte b[] = tableHtmlBuilder.toString().getBytes();
		// return new ByteArrayInputStream(b);
		return new ByteArrayInputStream(tableHtmlBuilder.toString().getBytes(
				"UTF8"));
	}

	// /**
	// * List数据的表格
	// *
	// * @param name
	// * @return
	// */
	// public static Object[] listTableHtml(String... name) {
	// Object[] res = new Object[2];
	// StringBuilder html = new StringBuilder();
	// String tableId = Generate.generateWord(3);
	// html.append("<table id='").append(tableId)
	// .append("'></table><input type=\"hidden\" id=\"ids\">");
	// res[0] = html;
	// res[1] = tableId;
	// return res;
	// }

	// /**
	// * List数据的表格
	// *
	// * @param name
	// * @return
	// */
	// public static Object[] listTableHtml(String from) {
	// Object[] res = new Object[2];
	// StringBuilder html = new StringBuilder();
	// StringBuilder tablHtml = new StringBuilder();
	// String tableId = Generate.generateWord(3);
	// html.append("<table id='").append(tableId)
	// .append("'></table><input type=\"hidden\" id=\"ids\">");
	// if (StrUtil.requestParamsNotEmpty(from))
	// tablHtml.append("<div style=\"height: 600px;\">")
	// .append(html)
	// .append("</div>")
	// .append("<div id='bt' style=\"height: 50px; width :1000px\"></div>");
	// else
	// tablHtml.append(html);
	// res[0] = tablHtml;
	// res[1] = tableId;
	// return res;
	// }

	// /**
	// * 列表页面 按钮：1、常规按钮 2、特殊按钮 3、打印、搜索、刷新
	// *
	// * @param tableName
	// * @param bean
	// * @param pid
	// * @param buttonPermission
	// * @param specialPermission
	// * @return
	// * @throws SecurityException
	// * @throws IllegalArgumentException
	// * @throws NoSuchFieldException
	// * @throws IllegalAccessException
	// */
	// public static StringBuilder listTableButtonHtml(String tableName,
	// Object bean, String pid, List<Permission> buttonPermission,
	// List<Permission> specialPermission,
	// List<Permission> perfPermission, String from)
	// throws SecurityException, IllegalArgumentException,
	// NoSuchFieldException, IllegalAccessException {
	// StringBuilder plain = new StringBuilder(
	// "<div style='margin-bottom:5px'>");
	// for (Permission p : buttonPermission) {
	// // String actionName = p.getActionname();
	// String uri2 = p.getUri2();
	// String jsName = tableName + "_" + uri2 + "()";
	// if (uri2.equals("detail")) {
	// plain.append(linkButton).append(jsName).append("'>")
	// .append(Constant.get("JSP_DETAIL")).append("</a>");
	// plain.append(addScript(openUrlByOneRowJs(tableName, jsName,
	// permissionUrl(p))));
	// } else if (uri2.equals("del")) {
	// plain.append(linkButton).append(jsName).append("'>")
	// .append(Constant.get("JSP_DEL")).append("</a>");
	// plain.append(addScript(findDelJs(tableName, jsName,
	// permissionUrl(p))));
	// } else if (uri2.equals("add")) {
	// plain.append(linkButton).append(jsName).append("'>")
	// .append(Constant.get("JSP_ADD")).append("</a>");
	// plain.append(addScript(findAddJs(jsName, permissionUrl(p), bean)));
	// String cpjsName = tableName + "_cp_" + uri2 + "()";
	// plain.append(linkButton).append(cpjsName).append("'>")
	// .append(Constant.get("JSP_COPY")).append("</a>");
	// plain.append(addScript(openUrlByOneRowJs(tableName, cpjsName,
	// permissionUrl(p))));
	// } else if (uri2.equals("edit")) {
	// plain.append(linkButton).append(jsName).append("'>")
	// .append(Constant.get("JSP_EDIT")).append("</a>");
	// plain.append(addScript(openUrlByOneRowJs(tableName, jsName,
	// permissionUrl(p))));
	// } else if (uri2.equals("batch")) {
	// plain.append(linkButton).append(jsName).append("'>")
	// .append(Constant.get("JSP_BATCHEDIT")).append("</a>");
	// plain.append(addScript(openUrlByOneRowJs(tableName, jsName,
	// permissionUrl(p))));
	// } else if (uri2.equals("exp")) {
	// plain.append(linkButton).append(jsName).append("'>")
	// .append("导出").append("</a>");
	// } else {
	// plain.append(linkButton).append(jsName).append("'>")
	// .append(p.getDescr()).append("</a>");
	// plain.append(addScript(openUrlByOneRowJs(tableName, jsName,
	// permissionUrl(p))));// TODO
	// // 设定button的访问后台方式--visitType---设定在对应的PERF显示方式中？
	// }
	// }
	// // for (Permission p : specialPermission) {
	// // String jsName = "jsm_" + p.getId() + "()";
	// // if (StringUtils.equals(p.uri(), "cfg/perf/layout")) {
	// // plain.append(linkButton).append(jsName)
	// // .append("'>" + p.getDescr() + "</a>");
	// // plain.append(addScript(c_editUnderPerf_js(jsName,
	// // permissionUrl(p), pid)));
	// // }
	// // }
	// //
	// plain.append("<script type='text/javascript' src='js/nl/NlData/consubmit.js'></script>");
	// plain.append(showSpecial(pid, specialPermission));
	// plain.append(buildPerfpermission(pid, perfPermission, tableName, ""));
	// plain.append(linkButton).append("window.print();'>打印</a>");
	// plain.append(linkButton).append(tableListJsMethod(tableName))
	// .append("' ><i class=\"icon-search\"></i>搜索</a>");
	//
	// plain.append(linkButton)
	// .append("javascript:window.document.location.reload()'><i class=\"icon-repeat\"></i>刷新</a>");
	// plain.append("</div>");
	// if (StrUtil.requestParamsNotEmpty(from)) {
	// StringBuilder fs = new StringBuilder();
	// fs.append(linkButton).append("df_cls()'>带值返回</a>");
	// plain.append(fs);
	// }
	// return plain;
	// }

	public static String tableListJsMethod(String tableName) {
		return tableName + "_list()";
	}

	/**
	 * 删除一个Bean方法
	 * 
	 * @param url
	 * 
	 * @param string
	 * @return
	 */

	/*
	 * public static StringBuilder findDelJs(String tableName,String jsName,
	 * String url) { StringBuilder res = new StringBuilder();
	 * res.append("function ") .append(jsName) .append("{var row = $('#")
	 * .append(tableName) .append(
	 * "').datagrid('getSelected');if(row){$.messager.confirm('确认框', '确认删除选中记录?', function(r){if (r){var id = row.id;$.post('"
	 * ) .append(url) .append(
	 * "&id='+id, function(data) {var msg=data['msg'];var delresult=data['delresult'];alert(msg);using(['dialog','messager'], function(){$.messager.show({title:'提示',msg:msg}); if(delresult=='success') {var index = $('#"
	 * ) .append(tableName) .append("').datagrid('getRowIndex', row);$('#")
	 * .append(tableName)
	 * .append("').datagrid('deleteRow', index);}});});}});}else{alert('")
	 * .append(ROWNOTSELECTED_TRIP).append("');}}"); return res; }
	 */

	// /**
	// * 删除一个Bean方法
	// *
	// * @param url
	// *
	// * @param string
	// * @return
	// */
	// public static StringBuilder findDelJs(String tableName, String jsName,
	// String url) {
	// StringBuilder res = new StringBuilder();
	// res.append("function ")
	// .append(jsName)
	// .append("{ var j=1; var rows = $('#")
	// .append(tableName)
	// .append("').datagrid('getSelections'); if(rows.length>0) {$.messager.confirm('确认框', '确认删除选中记录?', function(r){ if (r) { var ids=''; for(var i=0; i<rows.length; i++){ if(ids=='') ids=rows[i].id;else ids = ids+','+rows[i].id;}$.post('")
	// .append(url)
	// .append("&_ids='+ids, function(data) {var msg=data['msg'];var delresult=data['delresult'];alert(msg);using(['dialog','messager'], function(){$.messager.show({title:'提示',msg:msg}); if(delresult=='success') {var index = $('#")
	// .append(tableName)
	// .append("').datagrid('getRowIndex', row);$('#")
	// .append(tableName)
	// .append("').datagrid('deleteRow', index);}});});}});} else{alert('")
	// .append(ROWNOTSELECTED_TRIP).append("');}}");
	// return res;
	// }

	/**
	 * 签收卡片方法
	 * 
	 * @param url
	 * 
	 * @param string
	 * @return
	 */
	public static StringBuilder findSignJs(String tableName, String jsName,
			String url) {
		StringBuilder res = new StringBuilder();
		res.append(" function ")
				.append(jsName)
				.append("{ var j=1; var rows = $('#")
				.append(tableName)
				.append("').datagrid('getSelections'); if(rows.length>0) {var ids=''; for(var i=0; i<rows.length; i++){ if(ids=='') ids=rows[i].id;else ids = ids+','+rows[i].id;} "
						+ "confirmcode(ids,'" + url + "');")
				.append("} else{alert('").append(ROWNOTSELECTED_TRIP)
				.append("');}}");
		return res;
	}

	/**
	 * 综合日志中，用于交接班确认的“确认”功能键
	 * 
	 * @param tableName
	 * @param jsName
	 * @param url
	 * @param beanId
	 * @return
	 */
	public static StringBuilder findSucJs(String tableName, String jsName,
			String url, String beanId) {
		StringBuilder res = new StringBuilder();
		res.append("function ").append(jsName)
				.append("{confirmcode('" + beanId + "','" + url + "');};");
		return res;
	}

	/*
	 * public static StringBuilder openUrlByOneRowJs(String tableName,String
	 * jsName, String url) { StringBuilder res = new StringBuilder(); String
	 * grid = "$('#" + tableName + "')"; res.append("function ") .append(jsName)
	 * .append("{var row = ") .append(grid) .append(
	 * ".datagrid('getSelected');if(row){var ids=$('#ids').val();var id = row.id;window.open('"
	 * ) .append(url) .append("&id='+id+'&_ids='+ids,'_blank');}else{alert('")
	 * .append(ROWNOTSELECTED_TRIP).append("');}}"); return res; }
	 */
	// public static StringBuilder openUrlByOneRowJs(String tableName,
	// String jsName, String url) {
	// StringBuilder res = new StringBuilder();
	// String grid = "$('#" + tableName + "')";
	// res.append("function ")
	// .append(jsName)
	// .append("{var rows = ")
	// .append(grid)
	// .append(".datagrid('getSelections');if(rows.length>0){ var ids=$('#ids').val(); var id = ''; var editids=''; for(var i=0; i<rows.length; i++){ if(editids=='') {editids=rows[i].id;id=rows[i].id;}else editids = editids+','+rows[i].id;} window.open('")
	// .append(url)
	// .append("&id='+id+'&_ids='+ids+'&_editids='+editids,'_blank');}else{alert('")
	// .append(ROWNOTSELECTED_TRIP).append("');}}");
	// return res;
	// }

	// private static StringBuilder c_editUnderPerf_js(String jsName, String
	// url,
	// String pid) {
	// StringBuilder res = new StringBuilder();
	// res.append("function ").append(jsName).append("{window.open('")
	// .append(url).append("&id=" + pid + "','_blank');}");
	// return res;
	// }
//
//	public static String permissionUrl(Permission p) {
//		StringBuilder res = new StringBuilder(Constant.get("URL") + p.uri());
//		res.append(Constant.PAGE_PARAM);
//		return res.toString();
//	}

	public static String buttonUrl(String url) {
		StringBuilder res = new StringBuilder(Constant.get("URL") + url);
		res.append(Constant.PAGE_PARAM);
		return res.toString();
	}

	/**
	 * 新增一个Bean方法
	 * 
	 * 弹出一个新页面
	 * 
	 * @param qb
	 *            在Quote页面点击新增后，增加相关属性，以在下一个页面使用
	 * @param url
	 * @param jsName
	 * @return
	 * @throws IllegalAccessException
	 * @throws NoSuchFieldException
	 * @throws IllegalArgumentException
	 * @throws SecurityException
	 */
	// private static StringBuilder findAddJs(String jsName, String url,
	// Object bean) throws SecurityException, NoSuchFieldException,
	// IllegalArgumentException, IllegalAccessException {
	// StringBuilder res = new StringBuilder();
	// res.append("function ").append(jsName).append("{window.open('")
	// .append(url).append("','_blank');}");
	// return res;
	// }

	/**
	 * 列表页面 搜索框
	 * 
	 * @param ppl
	 * @param bean
	 * @param quotePropertyId
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	// public static StringBuilder listTableSearch(List<PerfProperty> ppl,
	// Object bean, String quotePropertyId) throws ClassNotFoundException,
	// IllegalArgumentException, IllegalAccessException,
	// SecurityException, NoSuchFieldException, InvocationTargetException,
	// NoSuchMethodException {
	// StringBuilder orgfilter = new StringBuilder();
	// for (PerfProperty pp : ppl) {
	// // final Property property = pp.getProperty();
	// String[] beanValue = filedVal(bean, pp);
	// // if (property.getId().equals(quotePropertyId)) {
	// // 如果这个属性就是引用页面的关联属性，则在搜索栏加入其hiddenValue
	// // pp.setEsh("0");
	// // 找到关联属性的值
	// // }
	// // findInput(pp, beanValue, "toolbar", false)
	// orgfilter.append(
	// new MyinputBuilder(pp, beanValue, "toolbar", false, "")
	// .findInput()).append("&nbsp;");
	// }
	// StringBuilder res = new StringBuilder("<div style='margin-bottom:5px'>");
	//
	// res.append(orgfilter);
	//
	// res.append("</div>");
	// return res;
	// }

	public static String[] filedVal(Object bean, PerfProperty pp)
			throws NoSuchFieldException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		String[] beanValue = { "", "" };// 描述+ID 或者 描述+描述
		Property property = pp.getProperty();// org.id
		String field = property.getProperty();// org.id
		// // ===================================dym
		// if (bean instanceof Map) {
		// if (StringUtils.isNotEmpty(property.getVotype())) {
		// Object bean2 = ReUtils.get(bean,field);
		// beanValue[1] = ReUtils.getStr(bean2, "id");
		// }else{
		// beanValue[1] = ReUtils.getStr(bean, field);
		// }
		// }
		// // ===================================dym
		// else {

		if (StringUtils.isNotEmpty(property.getVotype())) {// 指向一个tablebean时，取其ID值
			field += ".id";
		}

		String[] split = StringUtils.split(field, ".");
		if (split.length == 3) {
			int lastIndexOf = StringUtils.lastIndexOf(field, ".");
			String front = StringUtils.substring(field, 0, lastIndexOf);
			String end = StringUtils.substring(field, lastIndexOf + 1);
			Object innerBean = ReUtils.get(bean, front);
			if (innerBean != null) {
				beanValue[1] = ReUtils.getStr(innerBean, end);
			}
		} else {
		}
		// }
		String dftvalue = propertyDftvalue(pp, property);
		if (StringUtils.isNotEmpty(dftvalue)
				& !StringUtils.isNotEmpty(beanValue[0])
				& !StringUtils.isNotEmpty(beanValue[1])) {
			beanValue[0] = (String) FilterVal.init(dftvalue).get();// TODO:当指代信息需要以ID+描述的形式出现时，FilterValue需返回ID+描述两个值
			beanValue[1] = beanValue[0];
		}
		return beanValue;
	}

	public static String[] emptyFiledVal(Map<String, Object> map,
			PerfProperty pp) throws NoSuchFieldException,
			IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {
		String[] beanValue = { "", "" };// 描述+ID 或者 描述+描述
		Property property = pp.getProperty();// org.id
		String field = property.getProperty();// org.id
		// ===================================dym

		if (StringUtils.isNotEmpty(property.getVotype())) {
			Object bean2 = ReUtils.get(map, field);
			beanValue[1] = ReUtils.getStr(bean2, "id");
		} else {
			beanValue[1] = ReUtils.getStr(map, field);
		}

		if (StringUtils.isNotEmpty(property.getVotype())) {// 指向一个tablebean时，取其ID值
			field += ".id";
		}

		String[] split = StringUtils.split(field, ".");
		if (split.length == 3) {
			int lastIndexOf = StringUtils.lastIndexOf(field, ".");
			String front = StringUtils.substring(field, 0, lastIndexOf);
			String end = StringUtils.substring(field, lastIndexOf + 1);
			Object innerBean = ReUtils.get(map, front);
			if (innerBean != null) {
				beanValue[1] = ReUtils.getStr(innerBean, end);
			}
		} else {
		}
		// }
		String dftvalue = propertyDftvalue(pp, property);
		if (StringUtils.isNotEmpty(dftvalue)
				& !StringUtils.isNotEmpty(beanValue[0])
				& !StringUtils.isNotEmpty(beanValue[1])) {
			beanValue[0] = (String) FilterVal.init(dftvalue).get();// TODO:当指代信息需要以ID+描述的形式出现时，FilterValue需返回ID+描述两个值
			beanValue[1] = beanValue[0];
		}
		return beanValue;
	}

	public static String propertyDftvalue(PerfProperty pp,
			final Property property) {
		String dftvalue = property.getDftvalue();
		if (StringUtils.isNotEmpty(pp.getDftvalue())) {
			dftvalue = pp.getDftvalue();
		}
		return dftvalue;
	}

	// /**
	// * 针对不同的参数值，给出对应的结果集 用于filter数据过滤、增加页面中的默认数据 TODO 测试反射调用service查询值的能力
	// *
	// * @param val
	// * @param baseDao
	// * @return
	// * @throws ClassNotFoundException
	// */
	// public static Object filterValue(String val) {
	// Object res = val;
	// if (val.startsWith(":")) {
	// if (val.equals(":currentOrgAndUnderIds")) {
	// } else if (val.equals(":currentDate")) {
	// res = DateTimeUtils
	// .formatDate(new Timestamp(System.currentTimeMillis()),
	// "yyyy-MM-dd");
	// } else {
	// }
	// }
	// return res;
	// }

	/**
	 * 弹出框通用JS pop():弹出，并接受返回值，返回值本身不能含有"," pop_clear():清空值
	 * 
	 * field url 弹出框链接 返回值格式【aaa,bbb】 name为text_field赋值aaa，表现文本
	 * name为field赋值bbb，提交文本
	 * 
	 * @return
	 */
	// public static StringBuilder findPopJs() {
	// StringBuilder res = new StringBuilder();
	// res.append("function pop(field,url){digStr='dialogHeight:800px;dialogWidth:1000px;center:yes';var ReturnValue = window.showModalDialog(url,'',digStr);if(ReturnValue!=null&&ReturnValue!=''&&ReturnValue!='null'){var ind = ReturnValue.indexOf(',');var text=ReturnValue.substring(0,ind);var value=ReturnValue.substring(ind+1,ReturnValue.length);var ft =$('input[name=\"text_'+field+'\"]');var fv =$('input[name=\"'+field+'\"]');ft.val(text);fv.val(value);}}");
	// res.append("function pop_clear(field){var ft =$('input[name=\"text_'+field+'\"]');var fv =$('input[name=\"'+field+'\"]');ft.val('');fv.val('');}");
	// return res;
	// }

	// use MyinputBuilder first
	// @Deprecated
	// public static StringBuilder findInput(PerfProperty pp, String[]
	// beanValue,
	// String from, boolean clearDescr) throws SecurityException,
	// IllegalArgumentException, NoSuchFieldException,
	// IllegalAccessException, InvocationTargetException,
	// NoSuchMethodException {
	// StringBuilder res = new StringBuilder();
	// String val0 = "";
	// String val1 = "";
	// if (beanValue != null) {
	// val0 = beanValue[0];
	// val1 = beanValue[1];
	// }
	// Property property = pp.getProperty();
	//
	// Myinput myinput = propertyMyinput(pp);
	// String esh = pp.getEsh();
	// String field = property.getProperty();
	// if (StringUtils.isNotEmpty(property.getVotype())) {// 对于类搜索，类的id作为搜索条件
	// field += ".id";
	// }
	// if ("0".equals(esh)) {
	// res.append("<input type='hidden' name='").append(field)
	// .append("' value='").append(val1).append("'>");
	// } else if ("1".equals(esh)) {
	// String type = "";
	// if (myinput != null)
	// type = myinput.getType();
	// if ("img".equals(type)) {
	// res.append(
	// "<a href='http://10.10.38.15:8081/KBMS/FE/yulan.sg?code=")
	// .append(val1)
	// .append("' target='_blank'><img height='130px' width='100px' src='http://10.10.38.15:8081/KBMS/FE/yulan.sg?code=")
	// .append(val1).append("'></a>");
	// } else {
	// if (!clearDescr)
	// res.append(property.getDescr()).append(":");
	// res.append(val1).append("<input type='hidden' name='")
	// .append(field).append("' value='").append(val1)
	// .append("'>");
	// }
	// } else if ("2".equals(esh) || ("3".equals(esh))) {
	// if (myinput != null) {
	// if (!clearDescr)
	// res.append(property.getDescr()).append(":");
	// String type = myinput.getType();
	//
	// String url = propertyValue(pp, "valueurl");
	// String dateOptionStr = "";
	// String validateParams = "";// 用在只需要required的select、tree、pop
	// if (!StringUtils.isNotEmpty(from)) {
	// String max = propertyValue(pp, "max");
	// String min = propertyValue(pp, "min");
	// if (!StringUtils.isNotEmpty(max)
	// && StringUtils.isNotEmpty(property.getLength())) {
	// max = property.getLength();
	// }
	// if (!StringUtils.isNotEmpty(min)) {
	// min = "0";
	// }
	// String required = "";
	// String requiredPv = propertyValue(pp, "required");
	// if ("1".equals(requiredPv)) {
	// required = "required:true,";
	// res.append("<font color=red>*</font>");
	// }
	// dateOptionStr = " data-options=\"" + required
	// + "validType:[";
	// String validType = propertyValue(pp, "validtype");
	// if (StringUtils.isNotEmpty(validType)) {
	// dateOptionStr += " '" + validType + "',";
	//
	// }
	// if (StringUtils.isNotEmpty(max)) {
	// dateOptionStr += "'length[" + min + "," + max
	// + "]']\" ";
	// }
	// dateOptionStr += "]\" ";
	//
	// if ("1".equals(requiredPv)) {
	// validateParams = " required='true' ";
	// }
	// }
	// if ("text".equals(type)) {
	// res.append("<input class='easyui-validatebox'")
	// .append(dateOptionStr)
	// .append(" type='text' name='").append(field)
	// .append("' value='").append(val1).append("'>");
	// } else if ("textarea".equals(type)) {
	// if ("toolbar".equals(from)) {
	// res.append("<input class='easyui-validatebox'")
	// .append(dateOptionStr)
	// .append(" type='text' name='").append(field)
	// .append("' value='").append(val1).append("'>");
	// } else {
	// res.append("<textarea class='easyui-validatebox'")
	// .append(dateOptionStr)
	// .append(" style='width: 699px; height: 267px;' name='")
	// .append(field).append("'>").append(val1)
	// .append("</textarea>");
	// }
	// } else if ("select".equals(type)) {
	// res.append("<input ")
	// .append(validateParams)
	// .append(" id='")
	// .append(field.replace(".", ""))
	// .append("' name='")
	// .append(field)
	// .append("' data-options='url: \"")
	// .append(url)
	// .append("\",valueField: \"value\",textField: \"name\",panelWidth: 350,editable: true");
	// res.append("'>");
	// setValueToInput(res, val1, field);
	// // String js = propertyJs(pp, property);
	// // if (StringUtils.isNotEmpty(js)) {
	// // String com = "$('input[name=\"" + field + "\"]')";
	// // res.append("<script language='JavaScript'>")
	// // .append(com).append(".combobox({").append(js)
	// // .append("});</script>");
	// // }
	// String com = "$('input[name=\"" + field + "\"]')";
	// res.append("<script language='JavaScript'>")
	// .append(com)
	// .append(".combobox({")
	// .append("filter: function(q, row){var opts = $(this).combobox('options');	return row[opts.textField].indexOf(q)>-1;	}")
	// .append("});</script>");
	// } else if ("sdate".equals(type)) {
	// // 来自工具栏的搜索框，对日期搜索进行范围搜索
	// res.append("<input type='text' id='").append(field)
	// .append("' ").append(dateOptionStr)
	// .append("' name='").append(field)
	// .append("' size='25'>");
	// res.append(
	// "<script type='text/javascript' charset='utf-8'>var ")
	// .append(field.replace(".", ""))
	// .append(" = new Kalendae.Input('")
	// .append(field)
	// .append("', {months:2,mode:'range',format:'YYYY-MM-DD'});</script>");
	// setValueToInput(res, val1, field);
	// } else if ("date".equals(type)) {
	// res.append("<input class='easyui-datebox'")
	// .append(dateOptionStr).append(" name='")
	// .append(field).append("'>");
	// setValueToInput(res, val1, field);
	// } else if ("pop".equals(type)) {
	// res.append("<input class='easyui-validatebox'")
	// .append(validateParams)
	// .append(" readonly='readonly' type='text' name='text_")
	// .append(field).append("' value='").append(val0)
	// .append("'><input type='hidden' name='")
	// .append(field).append("' value='").append(val1)
	// .append("'>");
	// // EDIT
	// String pop_params = "'" + field + "','" + url + "'";
	// String pop_clear_params = "'" + field + "'";
	// res.append(
	// "<a href='javascript:void(0)' onclick=\"javascript:pop(")
	// .append(pop_params)
	// .append(")\" >选择&nbsp;</a><a href='javascript:void(0)' onclick=\"javascript:pop_clear(")
	// .append(pop_clear_params).append(")\" >清空</a>");
	// res.append("<script language='JavaScript'>");
	// res.append("\r function pop(field,url){digStr='dialogHeight:900px;dialogWidth:1000px;center:yes';var ReturnValue = window.showModalDialog(url,'',digStr);if(ReturnValue!=null&&ReturnValue!=''&&ReturnValue!='null'){\r var ind = ReturnValue.indexOf(';');var text=ReturnValue.substring(0,ind);var value=ReturnValue.substring(ind+1,ReturnValue.length);var ft =$('input[name=\"text_'+field+'\"]');var fv =$('input[name=\"'+field+'\"]');ft.val(text);fv.val(value);}}");
	// res.append("function pop_clear(field){var ft =$('input[name=\"text_'+field+'\"]');var fv =$('input[name=\"'+field+'\"]');ft.val('');fv.val('');}");
	// res.append("</script>");
	// }
	// // else if ("radio".equals(type)) {
	// // res.append("<input type='radio' value='1' name='")
	// // .append(field).append("'>是")
	// // .append("<input type='radio' value='0' name='")
	// // .append(field).append("'>否")
	// // .append("<input type='radio' value='' name='")
	// // .append(field).append("'>空");
	// // String com = "$('input[name=\"" + field + "\"][value=\""
	// // + val1 + "\"]')";
	// // res.append("<script language='JavaScript'>").append(com)
	// // .append(".attr('checked','checked');</script>");
	// // }
	// else if ("radio".equals(type)) {
	// res.append("<input class='easyui-combobox'")
	// .append(validateParams)
	// .append(" id='")
	// .append(field.replace(".", ""))
	// .append("' name='")
	// .append(field)
	// .append("' data-options='url: \"cfg/Cons/select?_domain=yorn\",valueField: \"value\",textField: \"name\",panelWidth: 350,editable: true");
	// res.append("'>");
	// setValueToInput(res, val1, field);
	// // String js = propertyJs(pp, property);
	// // if (StringUtils.isNotEmpty(js)) {
	// // String com = "$('input[name=\"" + field + "\"]')";
	// // res.append("<script language='JavaScript'>")
	// // .append(com).append(".combobox({").append(js)
	// // .append("});</script>");
	// // }
	// } else if ("radioEnable".equals(type)) {
	// res.append("<input class='easyui-combobox'")
	// .append(validateParams)
	// .append(" id='")
	// .append(field.replace(".", ""))
	// .append("' name='")
	// .append(field)
	// .append("' data-options='url: \"cfg/Cons/select?_domain=enable\",valueField: \"value\",textField: \"name\",panelWidth: 350,editable: true");
	// res.append("'>");
	// setValueToInput(res, val1, field);
	// } else if ("tree".equals(type)) {
	// url = CfgUtil.addParam(url, "endTreeValue=" + val1);
	// res.append("<input class=' easyui-combotree'")
	// .append(validateParams).append(" name='")
	// .append(field).append("' data-options='url: \"")
	// .append(url)
	// // .append("?endTreeValue=").append(val2)
	// .append("\",editable:true,panelWidth: 350")
	// .append("'>");
	// setValueToInput(res, val1, field);
	// } else if ("numberbox".equals(type)) {
	// res.append(
	// "<input class='easyui-numberbox' style='width:50px;'")
	// .append(dateOptionStr).append(" name='")
	// .append(field).append("' value='").append(val1)
	// .append("'>");
	// } else if ("double2".equals(type)) {
	// res.append("<input class='easyui-numberbox' precision='2' ")
	// .append(dateOptionStr).append(" name='")
	// .append(field).append("' value='").append(val1)
	// .append("'>");
	// } else if ("file".equals(type)) {
	// res.append("<input class='easyui-validatebox' type='file' ")
	// .append(dateOptionStr).append(" name='upload' >");
	// res.append("<input type='hidden' name='").append(field)
	// .append("' value='").append(val1).append("'>");
	// } else if ("editor".equals(type)) {
	// res.append("<script type='text/javascript' charset='utf-8' src='ueditor/ueditor.config.js'></script>"
	// +
	// "<script type='text/javascript' charset='utf-8' src='ueditor/ueditor.all.min.js'> </script>"
	// +
	// "<script type='text/javascript' charset='utf-8' src='ueditor/lang/zh-cn/zh-cn.js'></script>");
	// res.append("<script id='editor' type='text/plain' style='width:1024px;height:500px;'></script>");
	// String com = "$('input[name=\"" + field + "\"]')";
	// res.append("<script type='text/javascript'>var ue = UE.getEditor('editor');ue.ready(function() {ue.setContent('"
	// + val1
	// + "','');});ue.addListener('contentChange',function(){"
	// + com + ".val(ue.getContent())});</script>");
	// res.append("<input type='hidden' name='").append(field)
	// .append("'  value='").append(val1).append("'>");
	// }
	// }
	// }
	// return res;
	// }

	public static Myinput propertyMyinput(PerfProperty pp) {
		Myinput myinput = pp.getMyinput();
		if (myinput == null) {
			myinput = pp.getProperty().getMyinput();
		}
		return myinput;
	}

	// static void setValueToInput(StringBuilder res, String val2, String field)
	// {
	// String com = "$('input[name=\"" + field + "\"]')";
	// res.append("<script language='JavaScript'>").append(com)
	// .append(".val('").append(val2).append("');</script>");
	// }

	public static String propertyValue(PerfProperty pp, String propertyName)
			throws IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {
		Property property = pp.getProperty();
		String val = ReUtils.getStr(pp, propertyName);
		if (!StringUtils.isNotEmpty(val)) {
			val = ReUtils.getStr(property, propertyName);
		}
		return val;
	}

	/**
	 * 列表页面 工具栏
	 * 
	 * @param tableName
	 * @param listTableButtonHtml
	 * @param listTableSearch
	 * @return
	 */
	// public static StringBuilder listTableToolBar(String tableName,
	// StringBuilder listTableButtonHtml, StringBuilder listTableSearch) {
	// StringBuilder res = new StringBuilder("<div id='").append(
	// listTableToolBarName(tableName)).append("'>");
	//
	// // StringBuilder plain = plainToolbarHtml();
	// res.append(listTableButtonHtml).append(listTableSearch)
	// .append("</div>");
	// return res;
	// }

	public static String listTableToolBarName(String tableName) {
		return tableName + "_tb";
	}

	/**
	 * 列表页面 搜索js方法 组成easyui-datagrid的各个部分，如搜索条件param、显示字段field、分页设置等
	 * 
	 * @param tableName
	 * @param url
	 * @param searchPl
	 * @param listPl
	 * @param pre
	 * @return
	 * @throws SecurityException
	 * @throws ClassNotFoundException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws NoSuchFieldException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	// public static StringBuilder searJs(String tableName, String url,
	// List<PerfProperty> searchPl, List<PerfProperty> listPl, String pre,
	// String from) throws SecurityException, ClassNotFoundException,
	// IllegalArgumentException, IllegalAccessException,
	// InstantiationException, NoSuchFieldException,
	// InvocationTargetException, NoSuchMethodException {
	// StringBuilder res = new StringBuilder();
	// StringBuilder cols = new StringBuilder();
	// StringBuilder params = new StringBuilder();
	// StringBuilder idfeids = new StringBuilder();
	// cols.append("{field:'ck',checkbox:true},");
	// for (PerfProperty pp : listPl) {
	// Property property = pp.getProperty();
	// String field = property.getProperty();
	// String esh = pp.getEsh();
	// String fom = "";
	// String hid = "";
	// int indexOf = field.indexOf(".");
	// if (indexOf >= 0) {
	// String child = field.substring(indexOf + 1, field.length());
	// String f1 = field.substring(0, indexOf);
	// fom = ",formatter: function(value,rec){if(rec." + f1
	// + "==null){return '';}return rec." + f1 + "['" + child
	// + "']}";
	// }
	// if ("0".equals(esh)) {
	// hid = ",hidden:true";
	// }
	// String width = propertyValue(pp, "width");
	// StringBuilder wsb = new StringBuilder();
	// if (StringUtils.isNotEmpty(width)) {
	// wsb.append(",width:").append(width);
	// }
	// cols.append("{field:'").append(field).append("'").append(wsb)
	// .append(",sortable:true,title:'")
	// .append(property.getDescr()).append("'").append(fom)
	// .append(hid).append("},");
	// }
	// for (PerfProperty pp : searchPl) {
	// Property property = pp.getProperty();
	// String field = property.getProperty();
	// if (StringUtils.isNotEmpty(property.getVotype())) {// 对于类搜索，类的id作为搜索条件
	// field += ".id";
	// }
	// params.append("'").append(field).append("':").append("$('#")
	// .append(tableName).append("_tb input[name=\"")
	// .append(field).append("\"]').val(),");
	// }
	// // params.append("'isPage':'1',");
	// params.append("'_sql':$('#sql').val(),");
	// if (StringUtils.isNotEmpty(pre)) {
	// // String[] rs = StringUtils.split(pre, "$");
	// //
	// params.append("'").append(rs[0]).append("':").append(rs[1]).append(",");
	// params.append("'p_re':'").append(pre).append("',");
	// }
	// // params.append("'bean.id':$('#parmaValue').val(),");
	// if (StringUtils.isNotEmpty(cols.toString())) {
	// cols.deleteCharAt(cols.length() - 1);
	// }
	// if (StringUtils.isNotEmpty(params.toString())) {
	// params.deleteCharAt(params.length() - 1);
	// }
	// // 当作为弹出框时，增加翻页多选功能
	// if (StrUtil.requestParamsNotEmpty(from)) {
	// idfeids.append("idField:'id',");
	// }
	// res.append("function ")
	// .append(tableListJsMethod(tableName))
	// .append("{$('#")
	// .append(tableName)
	// .append("').datagrid({toolbar:'#")
	// .append(listTableToolBarName(tableName))
	// .append("',pageList:[100,50,20,10],onLoadSuccess:function(data){var rows=data['rows'];var len = rows.length;var ind = 0;var ids='';while(ind<len){var r=rows[ind];ids+=r['id']+',';ind++;}$('#ids').val(ids);},"
	// +
	// "showFooter: true,nowrap: false,pageSize:'10',singleSelect:false,fit:true,pagination:true,rownumbers:true,checkOnSelect:true,selectOnCheck:true,")
	// .append(idfeids).append("url:'").append(url)
	// .append("',queryParams:{").append(params)
	// .append("},columns:[[").append(cols).append("]]});}");
	// return res;
	// }

	/**
	 * 在页面初始化时调用LIST_JS_NAME方法
	 * 
	 * @param tableName
	 * 
	 * @return
	 */
	// public static StringBuilder laterSearInitJs(String tableName) {
	// StringBuilder res = new StringBuilder();
	// res.append("$(function(){setTimeout(\"")
	// .append(tableListJsMethod(tableName)).append(";\",500);});");
	// return res;
	// }

	/**
	 * 导出js 因为需要后台进行参数拼接，所以必须使用后台生成
	 * 
	 * @param tableName
	 * @param url
	 * @param searchPl
	 * @param pre
	 * @return
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws ClassNotFoundException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	// public static StringBuilder exportXlsJs(String tableName, String url,
	// List<PerfProperty> searchPl, String pre) throws SecurityException,
	// NoSuchFieldException, ClassNotFoundException,
	// IllegalArgumentException, IllegalAccessException {
	// StringBuilder res = new StringBuilder();
	// StringBuilder params = new StringBuilder();
	// for (PerfProperty pp : searchPl) {
	// Property property = pp.getProperty();
	// String field = property.getProperty();
	// params.append("'").append(field).append("':")
	// .append("$('input[name=\"").append(field)
	// .append("\"]').val(),");
	// }
	//
	// params.append("'_sql':$('#sql').val(),");
	// if (StrUtil.requestParamsNotEmpty(pre)) {
	// // String[] rs = StringUtils.split(pre, "$");
	// //
	// params.append("'").append(rs[0]).append("':").append(rs[1]).append(",");
	// params.append("'p_re':'").append(pre).append("',");
	// }
	// if (StringUtils.isNotEmpty(params.toString())) {
	// params.deleteCharAt(params.length() - 1);
	// }
	// res.append("function ")
	// .append(tableName)
	// .append("_exp(){var rows = $('#")
	// .append(tableName)
	// .append("').datagrid('getSelections'); if(rows.length>0){var ids=''; for(var i=0; i<rows.length; i++){ if(ids=='') ids=rows[i].id;else ids = ids+','+rows[i].id;}$.post('")
	// .append(url)
	// .append("',{'_ids':ids},function(json){window.open('" +
	// Constant.get("URL")
	// + "download?_exportUrl='+json);});")
	// .append("}else{$.post('")
	// .append(url)
	// .append("',{")
	// .append(params)
	// .append("},function(json){window.open('" + Constant.get("URL")
	// + "download?_exportUrl='+json);});}}");
	// // .append("},function(jsx){alert(jsx);});}");
	//
	// // res.append("function ").append(tableName)
	// // .append("_exp(){window.open('").append(url).append("');}");
	//
	// // res.append("function ").append(tableName)
	// // .append("_exp(){$.post('").append(url)
	// // .append("',{")
	// // .append(params).append("});}");
	// // 中文名需要在这里改造成post或者对中文进行编码
	// return res;
	// }

	/**
	 * 父页面关闭时调用的js方法 返回父页面选中的bean的id和DF_DESCR
	 * 
	 * @param from
	 * @param clazz2
	 * @param className
	 * @return
	 * @throws ClassNotFoundException
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	// public static StringBuilder findClsJs(String from, String tableName,
	// String value, String descr) throws ClassNotFoundException,
	// SecurityException, NoSuchFieldException, IllegalArgumentException,
	// IllegalAccessException {
	// StringBuilder res = new StringBuilder();
	// String value_name = "id";
	// String descr_name = "descr";
	// if (StrUtil.requestParamsNotEmpty(value))
	// value_name = value;
	// if (StrUtil.requestParamsNotEmpty(descr))
	// descr_name = descr;
	// if (StrUtil.requestParamsNotEmpty(from)) {
	// res.append(
	// "function df_cls() { var returnvalues='';var ids='';var descrs='';var rows = $('#")
	// .append(tableName)
	// .append("').datagrid('getSelections');if(rows.length>0){for(var i=0; i<rows.length; i++){ if(ids=='') {ids=rows[i].")
	// .append(value_name)
	// .append(";descrs=rows[i].")
	// .append(descr_name)
	// .append(";}else {ids = ids+','+rows[i].")
	// .append(value_name)
	// .append(";descrs = descrs+','+rows[i].")
	// .append(descr_name)
	// .append(";}} returnvalues=descrs+';'+ids;window.returnValue = returnvalues;window.close();}else{alert('")
	// .append(ROWNOTSELECTED_TRIP).append("');}}");
	// res.append("\r $(document).ready(function () { $('#")
	// .append(tableName)
	// .append("').datagrid({")
	// .append("onClickRow: function (rowindex,rowdata){")
	// .append("\r var id=rowdata.")
	// .append(value_name)
	// .append("; var descr=rowdata.")
	// .append(descr_name)
	// .append(";\r if(document.getElementById(id)==null) ")
	// .append("{ \r var ap=\"<div><input type='button' id='\"+id+ \"' value='\"+descr+\"'></div>\";")
	// .append("$('#bt').append(ap); ")
	// .append("}else{\r var el=document.getElementById(id);\r el.parentNode.remove();}}})});");
	// }
	// return res;
	// }

	// public static StringBuilder neditForm(String url, Object bean, Perf perf,
	// String beanId, String ids, List<Permission> specialPermission,
	// List<Permission> perfPermission, String enctype,
	// List<SectionDraw> sds, SecList[] sls)
	// throws ClassNotFoundException, SecurityException,
	// NoSuchFieldException, IllegalArgumentException,
	// IllegalAccessException, InvocationTargetException,
	// NoSuchMethodException {
	// StringBuilder res = new StringBuilder();
	//
	// fixTop(url, beanId, ids, res, buttons(null).toString(), perf.getId(),
	// specialPermission, perfPermission);
	// StringBuilder formTitle = formTitle(url, enctype);
	//
	// res.append(formTitle);
	// drawLayoutSecTable(bean, res, sds);
	// res.append("</form>");
	// drawLayoutSecList(bean, res, sls);
	//
	// return res;
	// }
//
//	public static void fixTop(String url, String beanId, String ids,
//			StringBuilder res, String but, String pid,
//			List<Permission> specialPermission, List<Permission> perfPermission) {
//		res.append("<div class=\"navbar navbar-fixed-top noprint\"> <div class=\"navbar-inner\"><div class=\"container-fluid\"><div class=\"nav-collapse collapse\">");
//		List<String> bl = new ArrayList<String>();
//		if (StringUtils.isNotEmpty(ids)) {
//			if (ids.endsWith(",")) {
//				ids = ids.substring(0, ids.length() - 1);
//			}
//			String[] split = ids.split(",");
//			int ind = 0;
//			int i = 0;
//			for (String s : split) {
//				if (beanId.equals(s)) {
//					ind = i;
//				}
//				i++;
//			}
//			int p = ind - 1;
//			int n = ind + 1;
//			if (p > -1) {
//				String r = url + "?_isp=1&id=" + split[p] + "&_ids=" + ids;
//				bl.add("if(e && event.ctrlKey && e.keyCode==37){window.location.href='"
//						+ r + "'}");
//				res.append("<ul class='nav'><li><a id='lastLink' title='快捷键：ctrl+向左' href='"
//						+ r
//						+ "'><i class=\"icon-backward\"></i>上一个</a></li></ul>");
//			}
//			if (n < i) {
//				String r = url + "?_isp=1&id=" + split[n] + "&_ids=" + ids;
//				bl.add("if(e && event.ctrlKey  && e.keyCode==39){window.location.href='"
//						+ r + "'}");
//				res.append("<ul class=\"nav pull-right\"><li><a id='nextLink' title='快捷键：ctrl+向右' href='"
//						+ r
//						+ "'>下一个<i class=\"icon-forward\"></i></a></li></ul>");
//			}
//		}
//		//
//
//		res.append("<ul class='nav'><li>")
//				.append(showSpecial(pid, specialPermission))
//				.append("</li></ul>");
//		res.append(buildPerfpermission(beanId, perfPermission, "", "fixTop"));
//		res.append("<ul class='nav'><li><a href='javascript:void(0)' onclick='window.print();'><i class=\"icon-print\"></i>打印</a></li></ul>");
//		res.append("<script type='text/javascript'>document.onkeyup=function(event){var e = event || window.event || arguments.callee.caller.arguments[0];");
//		for (String s : bl) {
//			res.append(s);
//		}
//		res.append("};</script>");
//		res.append(but);
//		res.append("</div></div></div></div>");
//	}

	public static void emptyFixTop(String route, String beanId,
			String ids, StringBuilder res, String but) {
		res.append("<div class=\"navbar navbar-fixed-top noprint\"> <div class=\"navbar-inner\"><div class=\"container-fluid\"><div class=\"nav-collapse collapse\">");
		List<String> bl = new ArrayList<String>();

		if (Constant.hasPath(route + ".perf.button")) {
			ConfigList buttonList = Constant.getList(route
					+ ".perf.button");
			for(int i = 0 ; i<buttonList.size();i++){
				Config btn = buttonList.get(i).atKey("btn");
				String buti = Constant.get(btn, "btn.uri");
				String bname = Constant.get(btn, "btn.name");
				String btype = Constant.get(btn, "btn.type");
				String bdescr = Constant.get(btn, "btn.descr");
				String bopenType = Constant.get(btn, "btn.openType");
				res.append(buildPerfButton("", "", buti,
						btype, bname,
						bdescr, bopenType, "fixTop"));
			}
		}
		// res.append(buildPerfpermission(beanId, perfPermission, "",
		// "fixTop"));
		res.append("<ul class='nav'><li><a href='javascript:void(0)' onclick='window.print();'><i class=\"icon-print\"></i>打印</a></li></ul>");
		res.append("<script type='text/javascript'>document.onkeyup=function(event){var e = event || window.event || arguments.callee.caller.arguments[0];");
		for (String s : bl) {
			res.append(s);
		}
		res.append("};</script>");
		res.append(but);
		res.append("</div></div></div></div>");
	}
//
//	public static StringBuilder showSpecial(String pid,
//			List<Permission> specialPermission) {
//		StringBuilder res = new StringBuilder();
//		for (Permission permission : specialPermission) {
//			if (StringUtils.equalsIgnoreCase(permission.uri(), "cfg/perf/edit")) {// TODO
//				// 将所有的特殊Permission提取，放在jsputil之外
//				res.append("<a target='_blank' href='cfg/perf/edit?_isp=1&id="
//						+ pid + "'>" + permission.getDescr() + "</a>");
//			}
//		}
//		return res;
//	}

	// public static StringBuilder showfixperfpermission(String beanId,
	// List<Permission> perfPermission, String tableName) {
	// StringBuilder res = new StringBuilder();
	// for (Permission permission : perfPermission) {
	// if (StringUtils.equals(permission.getOpen(), "1")) {
	// if (StringUtils.equals(permission.uri(), "1"))
	// res.append("<ul class='nav'><li><a href='javascript:void(0)' onclick='"
	// + permission.uri()
	// + "()'><i class=\"icon-print\"></i>"
	// + permission.getDescr() + "</a></li></ul>");
	// } else if (StringUtils.equals(permission.getOpen(), "2")) {
	// res.append("<script type='text/javascript' src='js/"
	// + permission.uri() + ".js'></script>");
	// res.append("<ul class='nav'><li><a href='javascript:void(0)' onclick='"
	// + permission.getUri2()
	// + "("
	// + tableName
	// + ")'></i>"
	// + permission.getDescr() + "</a></li></ul>");
	// } else {
	// res.append("<ul class='nav'><li><a target='_blank' href='"
	// + permission.uri() + "?_isp=1&id=" + beanId + "'>"
	// + permission.getDescr() + "</a></li></ul>");
	// }
	// }
	// return res;
	// }

	// public static StringBuilder showlistperfpermission(String beanId,
	// List<Permission> perfPermission, String tableName) {
	// StringBuilder res = new StringBuilder();
	// for (Permission permission : perfPermission) {
	// if (StringUtils.equals(permission.getOpen(), "1")) {
	// if (StringUtils.equals(permission.uri(), "1"))
	// res.append(linkButton).append(permission.getUri2())
	// .append("()'>").append(permission.getDescr())
	// .append("</a>");
	// } else if (StringUtils.equals(permission.getOpen(), "2")) {
	// res.append("<script type='text/javascript' src='js/"
	// + permission.uri() + "_js.js'></script>");
	// //
	// res.append("<script type='text/javascript' src='js/nl/NlData/consubmit.js'></script>");
	// res.append(
	// "<a href='javascript:void(0)' class='easyui-linkbutton' data-options='plain:true' onclick='")
	// .append(permission.getUri2())
	// .append("(\"" + tableName + "\")'>")
	// .append(permission.getDescr()).append("</a>");
	// } else {
	// if (StringUtils.equals(permission.getType(), "50")) {
	// String jsName = tableName + "_" + permission.getUri2()
	// + "()";
	//
	// res.append(" <script type='text/javascript' src='js/confirm.js'></script>");
	// res.append(
	// "<a href='javascript:void(0)' class='easyui-linkbutton' data-options='plain:true' onclick='")
	// .append(jsName).append("'>")
	// .append(permission.getDescr()).append("</a>");
	// res.append(addScript(findSignJs(tableName, jsName,
	// permissionUrl(permission))));
	//
	// } else {
	// res.append("<ul class='nav'><li><a target='_blank' href='"
	// + permission.uri() + "?_isp=1&id=" + beanId + "'>"
	// + permission.getDescr() + "</a></li></ul>");
	// }
	// }
	// }
	// return res;
	// }
//
//	public static StringBuilder buildPerfpermission(String beanId,
//			List<Permission> perfPermission, String tableName, String pageType) {
//		StringBuilder res = new StringBuilder();
//		for (Permission permission : perfPermission) {
//			String type = permission.getType();
//			String uri2 = permission.getUri2();
//			String jsName = tableName + "_" + uri2 + "()";
//			String open = permission.getOpen();
//			String click = jsName;
//			Perf perf = permission.getPerf();
//			String kind = perf.getKind();
//			// add js and get click
//			if ("10".equals(type)) {// 10:引入js/uri0/uri1/uri2_js.js（需要自定义），且访问其中的uri2(tableName)方法
//				res.append("<script type='text/javascript' src='js/"
//						+ permission.uri() + "_js.js'></script>");
//
//				click = permission.getUri2() + "(\"" + tableName + "\")";
//
//			} else if ("20".equals(type)) {// 20：弹出window.comfirm，确认后访问对应uri
//				res.append("<script>function " + jsName
//						+ "{window.confirm('确认打开？'){");
//				openUriByOpen(res, permission, open, beanId);
//				res.append("}}</script>");
//				// } else if ("30".equals(type)) {
//				//
//				// } else if ("40".equals(type)) {
//
//			} else if ("50".equals(type)) {// 50:弹出密码验证框后访问对应uri
//				res.append(" <script type='text/javascript' src='js/confirm.js'></script>");
//				if (StringUtils.endsWithIgnoreCase(kind, "list")) {
//					res.append(JsUtil.addScript(findSignJs(tableName, jsName,
//							permissionUrl(permission))));
//				} else if (StringUtils.endsWithIgnoreCase(kind, "nedit")
//						|| StringUtils.endsWithIgnoreCase(kind, "ndetail")) {
//					res.append(JsUtil.addScript(findSucJs(tableName, jsName,
//							permissionUrl(permission), beanId)));
//				}
//
//			} else {// 默认（空）：直接访问对应uri
//				res.append("<script>function " + jsName + "{");
//				openUriByOpen(res, permission, open, beanId);
//				res.append("}</script>");
//			}
//			// build button
//			if (StringUtils.equals("fixTop", pageType)) {
//				res.append("<ul class='nav'><li>");
//			}
//			res.append(linkButton).append(click).append("'>")
//					.append(permission.getDescr()).append("</a>");
//			if (StringUtils.equals("fixTop", pageType)) {
//				res.append("</li></ul>");
//			}
//
//		}
//		return res;
//	}

	public static StringBuilder buildPerfButton(String beanId,
			String tableName, String buttonUrl, String buttonType,
			String buttonName, String buttonDescr, String openType,
			String pageType) {
		StringBuilder res = new StringBuilder();
		String jsName = tableName + "_" + buttonName + "()";
		String click = jsName;
		// add js and get click
		// 10:引入js/uri0/uri1/uri2_js.js（需要自定义），且访问其中的uri2(tableName)方法
		if ("10".equals(buttonType)) {
			res.append("<script type='text/javascript' src='js/" + buttonUrl
					+ "_js.js'></script>");

			click = buttonName + "(\"" + tableName + "\")";

		} else if ("20".equals(buttonType)) {// 20：弹出window.comfirm，确认后访问对应uri
			res.append("<script>function " + jsName
					+ "{window.confirm('确认打开？'){");
			emptyOpenUriByOpen(res, buttonUrl, openType, beanId);
			res.append("}}</script>");
		}
		// else if ("50".equals(buttonType)) {// 50:弹出密码验证框后访问对应uri
		// res.append(" <script type='text/javascript' src='js/confirm.js'></script>");
		// if (StringUtils.endsWithIgnoreCase(kind, "list")) {
		// res.append(addScript(findSignJs(tableName, jsName,
		// permissionUrl(permission))));
		// } else if (StringUtils.endsWithIgnoreCase(kind, "nedit")
		// || StringUtils.endsWithIgnoreCase(kind, "ndetail")) {
		// res.append(addScript(findSucJs(tableName, jsName,
		// permissionUrl(permission), beanId)));
		// }
		//
		// }
		else {// 默认（空）：直接访问对应uri
			res.append("<script>function " + jsName + "{");
			emptyOpenUriByOpen(res, buttonUrl, openType, beanId);
			res.append("}</script>");
		}
		// build button
		if (StringUtils.equals("fixTop", pageType)) {
			res.append("<ul class='nav'><li>");
		}
		res.append(linkButton).append(click).append("'>").append(buttonDescr)
				.append("</a>");
		if (StringUtils.equals("fixTop", pageType)) {
			res.append("</li></ul>");
		}

		return res;
	}

//	private static void openUriByOpen(StringBuilder res, Permission permission,
//			String open, String beanId) {
//		if (StringUtils.equals(open, "1")) {
//			res.append("window.open('").append(permissionUrl(permission))
//					.append("&id=").append(beanId).append("');");
//		} else {
//			res.append("$.get('").append(permissionUrl(permission))
//					.append("&id=").append(beanId)
//					.append("',function(json){alert(json);});");
//		}
//	}

	private static void emptyOpenUriByOpen(StringBuilder res, String url,
			String open, String beanId) {
		if (StringUtils.equals(open, "1")) {
			res.append("window.open('").append(buttonUrl(url)).append("&id=")
					.append(beanId).append("');");
		} else {
			res.append("$.get('").append(buttonUrl(url)).append("&id=")
					.append(beanId).append("',function(json){alert(json);});");
		}
	}

	public static void drawLayoutSecTable(Object bean, StringBuilder res,
			List<SectionDraw> sds) throws NoSuchFieldException,
			IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {
		for (SectionDraw sd : sds) {
			buildTogTitle(sd.getTitle(), sd.getBk(), res);

			List<PerfProperty> pps = sd.getPps();
			if (CollUtil.isNotEmpty(pps)) {
				ntable(bean, res, pps);
			}

		}
	}

	public static void emptyDrawLayoutSecTable(Map<String, Object> map,
			StringBuilder res, List<SectionDraw> sds)
			throws NoSuchFieldException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		for (SectionDraw sd : sds) {
			buildTogTitle(sd.getTitle(), sd.getBk(), res);

			List<PerfProperty> pps = sd.getPps();
			if (CollUtil.isNotEmpty(pps)) {
				emptyNtable(map, res, pps);
			}

		}
	}

	public static void emptyDrawLayoutSecTable(Object bean, StringBuilder res,
			List<SectionDraw> sds, String confHeader)
			throws NoSuchFieldException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		if (Constant.hasPath(confHeader + ".handler.perf.addPn")) {
			ConfigList buttonList = Constant.getList(confHeader
					+ ".handler.perf.addPn");
			Iterator<ConfigValue> config = buttonList.iterator();
			while (config.hasNext()) {
				ConfigValue configValue = (ConfigValue) config.next();
				Map<String, Object> addPn = (Map<String, Object>) configValue
						.unwrapped();

				String title = (String) addPn.get("title");
				if (StringUtils.isEmpty(title))
					title = "新增";
				String bk = (String) addPn.get("bk");
				buildTogTitle(title, bk, res);

				List<Map<String, String>> props = (List<Map<String, String>>) addPn
						.get("props");
				for (Map<String, String> prop : props) {
					String pn = prop.get("pn");
					String descr = prop.get("descr");
					String esh = prop.get("esh");
				}
				String aString = "";
			}
		}
	}

	public static void drawLayoutSecList(Object bean, StringBuilder res,
			SecList[] sls, SecList[] nsls) throws NoSuchFieldException,
			IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {
		if (sls != null) {
			for (SecList sl : sls) {
				buildTogTitle(sl.getTitle(), res);
				String url = sl.getUrl();
				if (StringUtils.isNotEmpty(url)) {
					String divId = Generate.generateWord(3);
					String type = sl.getType();
					String toolBar = sl.getToolBar();
					StringBuilder params = new StringBuilder();
					params.append("'_cite':'1',");
					String param = sl.getParam();
					String paramValue = sl.getParamValue();
					if (StringUtils.equals(type, "p_re")) {
						params.append("'p_re':'").append(param).append("=")
								.append(ReUtils.getStr(bean, paramValue))
								.append("'");
					} else {
						params.append("'").append(param).append("':'")
								.append(ReUtils.getStr(bean, paramValue))
								.append("'");
					}
					if (StringUtils.isNotEmpty(toolBar)
							&& StringUtils.equalsIgnoreCase(toolBar, "0")) {
						params.append(",'toolBar':'0'");
					}
					res.append("<div id='")
							.append(divId)
							.append("' class='panel-noscroll' style='height:400px;'></div>");
					res.append("<script>").append("$.get('").append(url)
							.append("',{").append(params)
							.append("},function(ht){$('#").append(divId)
							.append("').html(ht)})").append("</script>");
				}

			}
		}
		if (nsls != null) {
			for (SecList sl : nsls) {
				buildTogTitle(sl.getTitle(), res);
				String url = sl.getUrl();
				if (StringUtils.isNotEmpty(url)) {
					String divId = Generate.generateWord(3);
					String type = sl.getType();
					StringBuilder params = new StringBuilder();
					params.append("'_cite':'1',");
					String param = sl.getParam();
					String paramValue = sl.getParamValue();
					if (StringUtils.equals(type, "p_re")) {
						params.append("'p_re':'").append(param).append("=")
								.append(ReUtils.getStr(bean, paramValue))
								.append("'");
					} else {
						params.append("'").append(param).append("':'")
								.append(ReUtils.getStr(bean, paramValue))
								.append("'");
					}
					res.append("<div id='").append(divId)
							.append("' class='panel-noscroll' style=''></div>");
					res.append("<script>").append("$.get('").append(url)
							.append("',{").append(params)
							.append("},function(ht){$('#").append(divId)
							.append("').html(ht)})").append("</script>");
				}

			}
		}
	}

	private static void ntable(Object bean, StringBuilder res,
			List<PerfProperty> pps) throws NoSuchFieldException,
			IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {
		res.append(ADD_EDIT_TABLE);
		for (PerfProperty pp : pps) {
			String[] beanValue = filedVal(bean, pp);
			// StringBuilder value = findInput(pp, beanValue, "", true);
			StringBuilder value = new MyinputBuilder(pp, beanValue, true, "")
					.findInput();
			if (!"0".equals(pp.getEsh())) {
				res.append("<tr>")
						.append("<td><a href='cfg/property/edit?_isp=1&id="
								+ pp.getProperty().getId() + "'>")
						.append(pp.getProperty().getDescr())
						.append("</a></td>").append("<td>").append(value)
						.append("</td>").append("</tr>");
			} else {
				res.append(value);
			}
		}
		res.append("</table>");
	}

	private static void emptyNtable(Map<String, Object> map, StringBuilder res,
			List<PerfProperty> pps) throws NoSuchFieldException,
			IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {
		res.append(ADD_EDIT_TABLE);
		for (PerfProperty pp : pps) {
			String[] beanValue = emptyFiledVal(map, pp);
			// StringBuilder value = findInput(pp, beanValue, "", true);
			StringBuilder value = new MyinputBuilder(pp, beanValue, true, "")
					.findInput();
			if (!"0".equals(pp.getEsh())) {
				res.append("<tr>").append(
						"<td><a href='cfg/property/edit?_isp=1&id="
								+ pp.getProperty().getId() + "'>");
				if (StringUtils.isNotEmpty(pp.getDescr()))
					res.append(pp.getDescr());
				else {
					res.append(pp.getProperty().getDescr());
				}
				res.append("</a></td>").append("<td>").append(value)
						.append("</td>").append("</tr>");
			} else {
				res.append(value);
			}
		}
		res.append("</table>");
	}

	public static StringBuilder buildTogTitle(String titleName,
			StringBuilder res) {
		StringBuilder title = res
				.append("<div style='background:#317fca;margin-bottom:10px;' onclick='$(this).next().toggle();$(this).next().next().toggle();'><span style='color:#ffffff;font-weight:bold;font-size:13pt;'>&nbsp;&nbsp;&nbsp;&nbsp;===")
				.append(titleName).append("===</span></div>");
		return title;
	}

	public static StringBuilder buildTogTitle(String titleName, String bk,
			StringBuilder res) {
		StringBuilder title = buildTogTitle(titleName, res);
		if (StringUtils.isNotEmpty(bk)) {
			title.append("<div class=\"demo-info\"><div>").append(bk)
					.append("</div></div>");
		}
		return title;
	}

	/**
	 * 我来组成《创建页面》的提交、刷新、发送按钮。
	 */
	public static StringBuilder buttons(StringBuilder sendBut) {
		StringBuilder res = new StringBuilder();
		res.append(
				"<script type='text/javascript'>document.onkeydown=function(event){var e = event || window.event || arguments.callee.caller.arguments[0];if(e && e.keyCode==13&& e.altKey){$(\"#df_form\").submit();}};</script>"
						+ "<ul class='nav'><li><a id='df_sub' title='快捷键：alt+enter' href='javascript:void(0)'  onclick='javascript:$(\"#df_form\").submit();'><i class=\"icon-ok\"></i>")
				.append(Constant.get("JSP_SUB")).append("</a></li>");
		res.append("<li><a id='df_action_msg'></a></li></ul>");
		if (sendBut != null)
			res.append(sendBut);
		return res;
	}

	/**
	 * 创建form的头
	 * 
	 * @param url
	 * @param enctype
	 * @return
	 */
	public static StringBuilder formTitle(String url, String enctype) {
		StringBuilder res = new StringBuilder();
		res.append("<form id='df_form' ");
		// res.append("<form id='df_form' class=\"form-horizontal form-actions\"");
		if (StringUtils.isNotEmpty(enctype)) {
			res.append(" enctype='multipart/form-data' ");
		}
		res.append("method='POST' action='").append(url).append("'>");
		return res;
	}

//	public static StringBuilder ndetail(String url, Object tableBean,
//			Object bean, Perf perf, String beanId, String ids,
//			List<Permission> specialPermission,
//			List<Permission> perfPermission, List<SectionDraw> sds,
//			SecList[] sls) throws ClassNotFoundException, SecurityException,
//			NoSuchFieldException, IllegalArgumentException,
//			IllegalAccessException, InvocationTargetException,
//			NoSuchMethodException {
//		StringBuilder res = new StringBuilder();
//		fixTop(url, beanId, ids, res, "", perf.getId(), specialPermission,
//				perfPermission);
//
//		for (SectionDraw sd : sds) {
//			List<PerfProperty> pps = sd.getPps();
//			for (PerfProperty pp : pps) {
//				pp.setEsh("1");
//			}
//		}
//		// drawLayoutSecTable(bean, res, sds);
//
//		SecList[] nsls = EpUtil.flowAttachment(tableBean);
//		drawLayoutSecList(bean, res, sls, nsls);
//
//		return res;
//	}
//
//	public static StringBuilder naddForm(String url, Object bean, Perf perf,
//			List<Permission> specialPermission,
//			List<Permission> perfPermission, String enctype,
//			List<SectionDraw> sds, SecList[] sls, AttaList attaList)
//			throws ClassNotFoundException, SecurityException,
//			NoSuchFieldException, IllegalArgumentException,
//			IllegalAccessException, InvocationTargetException,
//			NoSuchMethodException {
//		// 用于判断是否增加流程发送按钮
//		StringBuilder res = new StringBuilder();
//		fixTop(url, "", "", res, buttons(null).toString(), perf.getId(),
//				specialPermission, perfPermission);
//
//		StringBuilder formTitle = formTitle(url, enctype);
//		res.append(formTitle);
//
//		// drawLayoutSecTable(bean, res, sds);
//
//		drawLayoutAttaList(bean, res, attaList);
//
//		res.append("</form>");
//		drawLayoutSecList(bean, res, sls, null);
//		// if(ifSend)
//		// {
//		// res.append("<div id='dd' class='easyui-dialog' title='流程启动' style='width:450px;height:300px;left:100px;top:150px;padding:10px' toolbar='#dlg-toolbar' buttons='#dlg-buttons' resizable='true'>")
//		// .append("<input id='className' name='className' type='hidden' value='"+ReUtils.getStr(tableBean,
//		// "className")+"'> </div>");
//		// .append("<input id='className' name='className' type='hidden' value='com.sg.tcks.tk.entity.epcd'> </div>");
//		// }
//		return res;
//	}

	public static StringBuilder emptynaddForm(HttpServletRequest request,
			List<SectionDraw> sds) throws ClassNotFoundException,
			SecurityException, NoSuchFieldException, IllegalArgumentException,
			IllegalAccessException, InvocationTargetException,
			NoSuchMethodException {
		// 用于判断是否增加流程发送按钮
		StringBuilder res = new StringBuilder();
		String url = request.getRequestURI();
		// 配置文件路径
		String route = (String) request.getAttribute("conf.route");

		emptyFixTop(route, "", "", res, buttons(null).toString());
		String enctype = "";
		StringBuilder formTitle = formTitle(url, enctype);
		res.append(formTitle);
		Map<String, Object> map = new HashMap<String, Object>();
		emptyDrawLayoutSecTable(map, res, sds);

		res.append("</form>");
		return res;
	}

	public static void drawLayoutAttaList(Object bean, StringBuilder res,
			AttaList attaList) throws IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		if (attaList != null) {
			String title = attaList.getTitle();
			String bk = attaList.getBk();
			String beanID = ReUtils.getStr(bean, Constant.ID);
			String className = attaList.getClassName();
			String delete = attaList.getDelete();
			String upload = attaList.getUpload();
			String download = attaList.getDownload();
			String preview = attaList.getPreview();

			buildTogTitle(title, bk, res);
			String downloadUrl = Constant.get("downloadUrl");
			String uploadUrl = Constant.get("uploadUrl");
			String previewUrl = Constant.get("previewUrl");
			if (StringUtils.isNotEmpty(upload)) {
				res.append("<INPUT id='uploadFile' name='uploadFile' type='button' value='上传附件' onclick=\"showupload('"
						+ download
						+ "','"
						+ preview
						+ "','"
						+ delete
						+ "','"
						+ uploadUrl
						+ "','"
						+ downloadUrl
						+ "','"
						+ previewUrl
						+ "');\">");
				res.append(" <script  language='JavaScript' src='http://10.10.38.15:8081/TCKS/js/attachment/upload.js'></script> ");
			}
			res.append("<table class='horTable' border='1px;' id='attalist'>");
			res.append("<thead><tr><th>源文件名称</td>"
					+ "<th>压缩后文件名称</td><th>操作</td></tr></thead>"
					+ "<tbody id='tb'>" + "</tbody></table> ");

			if (StringUtils.isNotEmpty(beanID)
					&& StringUtils.isNotEmpty(className)) {
				res.append(" <script  language='JavaScript' src='http://10.10.38.15:8081/TCKS/js/attachment/attachList.js'></script> ");
				res.append(" <script  language='JavaScript'>");
				res.append("$(function() {attachList('" + download + "','"
						+ preview + "','" + delete + "','" + downloadUrl
						+ "','" + previewUrl + "','" + beanID + "','"
						+ className + "');});");
				res.append("</script> ");
			}
			if (StringUtils.isNotEmpty(delete)) {
				res.append(" <script  language='JavaScript' src='http://10.10.38.15:8081/TCKS/js/attachment/delete.js'></script> ");
			}

		}
	}
}
