package com.sg.common.empty.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.sg.common.utils.Constant;

public class Conditions {
	HttpServletRequest request;

	public static Conditions init(HttpServletRequest request) {
		Conditions res = new Conditions();
		res.request = request;
		return res;
	}
	

	public String buildSorts() {
		String sortSql = "";
		String sort = request.getParameter("sort");
		if (StringUtils.isNotEmpty(sort)) {
			String[] sorts = StringUtils.split(sort, ",");
			if (sorts.length > 0) {
				sortSql = " order by ";
			}
			int ind = 0;
			while (ind < sorts.length) {
				if (ind > 0) {
					sortSql += ",";
				}
				if (sorts[ind].startsWith("-")) {
					sortSql += sorts[ind].substring(1) + " desc";
				} else {
					sortSql += sorts[ind].substring(1) + " asc";
				}
				ind++;
			}
		}
		return sortSql;
	}

	public String buildLimit() {
		String limitSql = "";
		String page = request.getParameter("page");
		String limit = request.getParameter("limit");
		if (StringUtils.isNotEmpty(page) && StringUtils.isNotEmpty(limit)) {
			Integer pageInt = Integer.valueOf(page);
			Integer limitInt = Integer.valueOf(limit);
			limitSql += " limit " + (pageInt - 1) * limitInt + "," + limitInt;
		}
		return limitSql;
	}


	public ConditionRes buildCon() {
		ConditionRes res = new ConditionRes();
		List<String> pl = new ArrayList<String>();
		String conditionSql = "";

		String route = (String) request.getAttribute("conf.route");
		List<String> conditions = Constant.getStringList(route + ".conditions");
		for (int i = 0; i < conditions.size(); i++) {
			String condition = conditions.get(i);
			String[] split = StringUtils.split(condition, "`");
			if (split.length > 1 && StringUtils.isNotEmpty(split[0])) {
				String param = request.getParameter(split[0]);
				if (StringUtils.isNotEmpty(param)) {
					// param = URLDecoder.decode(param, "UTF-8");
					// 网页端传入此参数，则在condition中加入sql语句
					if (!StringUtils.startsWith(split[1], "@")) {
						// String conSql = StringUtils.replace(split[1], "?",
						// param);
						pl.add(param);
						conditionSql += " and " + split[1];
					} else {
						String[] ss = StringUtils.split(split[1], ",");
						if (StringUtils.equals("@dateRange", ss[0])) {
							conditionSql += " and " + ss[1] + " >= ? and "
									+ ss[1] + " <= ? ";
							pl.addAll(Arrays.asList(param.split(" - ")));
						} else if (StringUtils.equals("@like", ss[0])) {
							String sqlVariable = split[0];
							if (ss.length > 1) {
								sqlVariable = ss[1];
							}
							conditionSql += " and " + sqlVariable + " like ? ";
							pl.add("%" + param + "%");
						}
					}
				}else{
					//TODO 业务内容应从此处分开
					if(StringUtils.equals(split[1], "sessionAttributeRestId")){
						HttpSession session = request.getSession();
						String myRestaurantId = (String)session.getAttribute(Constant.SESSION_MYRESTAURANT);
						if(StringUtils.isNotBlank(myRestaurantId)){
							String[] restIds = myRestaurantId.split(",");
							conditionSql += " and " + split[0] + " in (";
							for(int l=0; l<restIds.length; l++){
								pl.add(restIds[l]);
								if(l==0){
									conditionSql +=  "?";
								}else{
									conditionSql += "," + "?";
								}
							}
							conditionSql += ")";
						}
					}
				}
			}
		}
		res.setParams(pl.toArray());
		res.setSql(conditionSql);
		return res;
	}

}
