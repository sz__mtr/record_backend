package com.sg.common.empty.core;

import java.util.List;

public class AsyncTree {
	String id;
	String label;
	String code;
//	String state;
	String checked;
	List<AsyncTree> children;
	
	public AsyncTree() {
		super();
	}
	public AsyncTree(String id, String text, String state) {
		this.id = id;
		this.label = label;
//		this.state = state;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	public List<AsyncTree> getChildren() {
		return children;
	}
	public void setChildren(List<AsyncTree> children) {
		this.children = children;
	}
}
