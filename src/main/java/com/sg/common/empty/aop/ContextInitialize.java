package com.sg.common.empty.aop;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.sg.common.empty.service.TableBeanService;
import com.sg.common.properties.PropertyFileUtil;
import com.sg.common.task.TaskManager;
import com.sg.common.utils.Constant;

/**
 * 全局初始化。
 * 
 * @author qianxiaowei
 * 
 */
public class ContextInitialize implements ServletContextListener {

	private Logger logger = LoggerFactory.getLogger(getClass());

	//
	// @Autowired
	// CfgService dao;

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {

		ServletContext servletContext = arg0.getServletContext();
		ApplicationContext ctx = WebApplicationContextUtils
				.getWebApplicationContext(servletContext);
		// CfgService service = (CfgService) ctx.getBean("cfgService");
		TableBeanService tbservice = (TableBeanService) ctx
				.getBean("tableBeanService");

		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		String layoutJsonPath = loader.getResource("/properties").getFile();
		Constant.PROPERTIES_PATH=layoutJsonPath;
		
		PropertyFileUtil.flushCONF(layoutJsonPath);
		

		tbservice.flush("consmap");
		// classname_propertyName:Property放入常量
		tbservice.flush("beanPropmap");
//		行政组织树
		tbservice.flush("orgTree");

		try {
			PropertyFileUtil.init();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TaskManager.fillTm(ctx);
		TaskManager.exec();

	}


}
