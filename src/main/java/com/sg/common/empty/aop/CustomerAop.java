package com.sg.common.empty.aop;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.ModelAndView;

import com.sg.common.empty.service.CfgService;

public abstract class CustomerAop {
	
	private CfgService service;
	
//	public abstract String pre(HttpServletRequest request,@RequestBody Map<String, Object> requestBody,
//			HttpServletResponse response, Object handler);
	
	public abstract String pre(HttpServletRequest request,
			HttpServletResponse response, Object handler);
	
	public abstract void after(HttpServletRequest request,
			HttpServletResponse response, Object handler);

	public abstract void post(HttpServletRequest request, HttpServletResponse response,
			Object handler, ModelAndView modelAndView);

	public CfgService getService() {
		return service;
	}

	public void setService(CfgService service) {
		this.service = service;
	}
}
