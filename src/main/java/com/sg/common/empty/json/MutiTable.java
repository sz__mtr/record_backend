package com.sg.common.empty.json;

public class MutiTable {
	private String title;
	private Prop[] props;
	String ref;
	/**
	 * sm列表的类
	 */
	private String refClass;
	String joinColumn;
	String refColumn="id";//关联表的关联属性，默认是关联id
	String _sqlFilter;

	public Prop[] getProps() {
		return props;
	}

	public void setProps(Prop[] props) {
		this.props = props;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getJoinColumn() {
		return joinColumn;
	}

	public void setJoinColumn(String joinColumn) {
		this.joinColumn = joinColumn;
	}

	public String getRefColumn() {
		return refColumn;
	}

	public void setRefColumn(String refColumn) {
		this.refColumn = refColumn;
	}

	public String get_sqlFilter() {
		return _sqlFilter;
	}

	public void set_sqlFilter(String _sqlFilter) {
		this._sqlFilter = _sqlFilter;
	}

	public String getRefClass() {
		return refClass;
	}

	public void setRefClass(String refClass) {
		this.refClass = refClass;
	}
}
