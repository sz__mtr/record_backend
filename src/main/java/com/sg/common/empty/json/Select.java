package com.sg.common.empty.json;


public class Select {
	Prop[] search;
	String fordescr;
	String sort;
	String order;
	
	public Prop[] getSearch() {
		return search;
	}
	public void setSearch(Prop[] search) {
		this.search = search;
	}
	public String getFordescr() {
		return fordescr;
	}
	public void setFordescr(String fordescr) {
		this.fordescr = fordescr;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
}
