package com.sg.common.empty.json;

public class Nsecs {
	private String enctype;
	SecTable[] sts;
	private MutiTable[] sms;
	SecList[] sls;
	AttaList attalist;

	public String getEnctype() {
		return enctype;
	}

	public void setEnctype(String enctype) {
		this.enctype = enctype;
	}

	public SecTable[] getSts() {
		return sts;
	}

	public void setSts(SecTable[] sts) {
		this.sts = sts;
	}

	public SecList[] getSls() {
		return sls;
	}

	public void setSls(SecList[] sls) {
		this.sls = sls;
	}

	public MutiTable[] getSms() {
		return sms;
	}

	public void setSms(MutiTable[] sms) {
		this.sms = sms;
	}

	public AttaList getAttalist() {
		return attalist;
	}

	public void setAttalist(AttaList attalist) {
		this.attalist = attalist;
	}
}
