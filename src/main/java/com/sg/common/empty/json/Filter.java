package com.sg.common.empty.json;

import org.springframework.stereotype.Component;

@Component
public class Filter {
	String descr;
	String type;
	String func;
	//处理类的名称
	String dealClass;
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFunc() {
		return func;
	}
	public void setFunc(String func) {
		this.func = func;
	}
	public String getDealClass() {
		return dealClass;
	}
	public void setDealClass(String dealClass) {
		this.dealClass = dealClass;
	}
	
}
