package com.sg.common.empty.json;

import com.sg.common.empty.core.DealPermission;

public class Prehandler {
	Permission permission;
	Filter validate;
	Business business;

	
	
	
	public Permission getPermission() {
		return permission;
	}

	public void setPermission(Permission permission) {
		this.permission = permission;
	}

	public Filter getValidate() {
		return validate;
	}

	public void setValidate(Filter validate) {
		this.validate = validate;
	}

	public Business getBusiness() {
		return business;
	}

	public void setBusiness(Business business) {
		this.business = business;
	}

}
