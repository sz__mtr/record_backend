package com.sg.common.empty.json;


public class TableList {
	Prop[] search;
	private Prop[] list;
	String sort;
	String order;
	private String idField;
	private String pageSize="10";
	private String autoSear="1";
	public Prop[] getSearch() {
		return search;
	}
	public void setSearch(Prop[] search) {
		this.search = search;
	}
	public Prop[] getList() {
		return list;
	}
	public void setList(Prop[] list) {
		this.list = list;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public String getIdField() {
		return idField;
	}
	public void setIdField(String idField) {
		this.idField = idField;
	}
	public String getPageSize() {
		return pageSize;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
	public String getAutoSear() {
		return autoSear;
	}
	public void setAutoSear(String autoSear) {
		this.autoSear = autoSear;
	}
}
