package com.sg.common.empty.json;

public class SecTable {
	private String title;
	private String bk;
	private Prop[] props;

	public Prop[] getProps() {
		return props;
	}

	public void setProps(Prop[] props) {
		this.props = props;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBk() {
		return bk;
	}

	public void setBk(String bk) {
		this.bk = bk;
	}

}
