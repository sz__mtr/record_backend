package com.sg.common.empty.json;

import org.springframework.stereotype.Component;

@Component
public class Permission {
	String descr;
	String url;
	// 用于判断该功能需要的权限类型
	// 0 标准功能，需分配权限
	// 1 无需权限
	// 2 需登录权限
	String type;

	// 关联的角色
	String[] roles;
	// 关联的组织
	String[] orgs;
	// 关联的人员
	String[] persons;
	
	// 关联的人员
	String js;
		
	// 处理类的名称
	String dealClass;

	
	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String[] getRoles() {
		return roles;
	}

	public void setRoles(String[] roles) {
		this.roles = roles;
	}

	public String[] getOrgs() {
		return orgs;
	}

	public void setOrgs(String[] orgs) {
		this.orgs = orgs;
	}

	public String[] getPersons() {
		return persons;
	}

	public void setPersons(String[] persons) {
		this.persons = persons;
	}

	public String getDealClass() {
		return dealClass;
	}

	public void setDealClass(String dealClass) {
		this.dealClass = dealClass;
	}

	public String getJs() {
		return js;
	}

	public void setJs(String js) {
		this.js = js;
	}

}
