package com.sg.common.empty.json;


public class TreeList {
	String treeUrl;
	String treeTitle;
	String treeJs;
	private TableList tl;
	public String getTreeUrl() {
		return treeUrl;
	}
	public void setTreeUrl(String treeUrl) {
		this.treeUrl = treeUrl;
	}
	public String getTreeTitle() {
		return treeTitle;
	}
	public void setTreeTitle(String treeTitle) {
		this.treeTitle = treeTitle;
	}
	public String getTreeJs() {
		return treeJs;
	}
	public void setTreeJs(String treeJs) {
		this.treeJs = treeJs;
	}
	public TableList getTl() {
		return tl;
	}
	public void setTl(TableList tl) {
		this.tl = tl;
	}
}
