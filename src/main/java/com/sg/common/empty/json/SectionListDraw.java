package com.sg.common.empty.json;

import java.util.List;

import com.sg.common.empty.entity.PerfProperty;

public class SectionListDraw {
	private String title;
	private String refClass;
	String ref;
	String joinColumn;
	String refColumn="id";//关联表的关联属性，默认是关联id
	String sqlFilter;
	private List<PerfProperty> pps;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<PerfProperty> getPps() {
		return pps;
	}
	public void setPps(List<PerfProperty> pps) {
		this.pps = pps;
	}
	public String getRef() {
		return ref;
	}
	public void setRef(String ref) {
		this.ref = ref;
	}
	public String getJoinColumn() {
		return joinColumn;
	}
	public void setJoinColumn(String joinColumn) {
		this.joinColumn = joinColumn;
	}
	public String getRefColumn() {
		return refColumn;
	}
	public void setRefColumn(String refColumn) {
		this.refColumn = refColumn;
	}
	public String getSqlFilter() {
		return sqlFilter;
	}
	public void setSqlFilter(String sqlFilter) {
		this.sqlFilter = sqlFilter;
	}
	public String getRefClass() {
		return refClass;
	}
	public void setRefClass(String refClass) {
		this.refClass = refClass;
	}
}
