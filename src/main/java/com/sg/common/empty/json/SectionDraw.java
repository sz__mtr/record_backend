package com.sg.common.empty.json;

import java.util.List;

import com.sg.common.empty.entity.PerfProperty;

public class SectionDraw {
	private String title;
	private String bk;
	private List<PerfProperty> pps;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<PerfProperty> getPps() {
		return pps;
	}
	public void setPps(List<PerfProperty> pps) {
		this.pps = pps;
	}
	public String getBk() {
		return bk;
	}
	public void setBk(String bk) {
		this.bk = bk;
	}
}
