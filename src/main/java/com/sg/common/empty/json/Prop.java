package com.sg.common.empty.json;

public class Prop {
	String pn;
	private String esh="2";
	private String taskKey;
	private String js;
	public String getPn() {
		return pn;
	}

	public void setPn(String pn) {
		this.pn = pn;
	}

	public String getEsh() {
		return esh;
	}

	public void setEsh(String editshowhide) {
		this.esh = editshowhide;
	}

	public String getTaskKey() {
		return taskKey;
	}

	public void setTaskKey(String taskKey) {
		this.taskKey = taskKey;
	}

	public String getJs() {
		return js;
	}

	public void setJs(String js) {
		this.js = js;
	}
	
}
