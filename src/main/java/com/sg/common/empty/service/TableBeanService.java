package com.sg.common.empty.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sg.common.dao.BaseDao;
import com.sg.common.empty.core.AsyncTree;
import com.sg.common.empty.entity.Cons;
import com.sg.common.empty.entity.Property;
import com.sg.common.empty.entity.TableBean;
import com.sg.common.login.entity.Org;
import com.sg.common.utils.Constant;


/**
 * 请假实体管理接口
 *
 * @author HenryYan
 * @param <T>
 * @param <T>
 */
@Component
public class TableBeanService extends BaseDao{
	
	@Autowired
	CfgService service;
	public TableBean byClassName(String className){
		return (TableBean) getSession().createQuery("from TableBean where className=?")
		.setParameter(0, className).list().get(0);
	}
	
	public void flush(String q) {
		if(StringUtils.equals(q, "consmap")){
			DetachedCriteria dc1 = DetachedCriteria.forClass(Cons.class);
			dc1.add(Restrictions.eq("enable", "1"));
			List<Cons> clList = find(dc1);
			Map<String,String> consMap=new HashMap<String,String>(); 
			for(Cons c:clList){
				consMap.put(c.getDomain().getDomain()+"_"+c.getValue(), c.getName());
			}
			Constant.CONSMAP=consMap;
		}else if(StringUtils.equals(q, "beanPropmap")){
			DetachedCriteria dc = DetachedCriteria.forClass(Property.class);
			List<Property> pl = find(dc);
			Map<String,Property> pm=new HashMap<String,Property>();
			Map<String,Property> emptyPM=new HashMap<String,Property>();
			for(Property p:pl){
				emptyPM.put(p.getBean().getTableName()+"."+p.getProperty(), p);
				pm.put(p.getBean().getClassName()+"_"+p.getProperty(), p);
			}
			Constant.BEANPROPMAP=pm;
			Constant.EMPTYPROPS=emptyPM;
		}else if (StringUtils.equals(q, "orgTree")){
			Org org = new Org();
//			org.setOrge("8165");		//委外团队
			org.setOrge("1000,1400");
			List<AsyncTree> list = null;
			try {
				list = service.findTreeValue(org, null,new ArrayList<String>(){{add("all");}}, new ArrayList<String>(){{add("descr");}});
			} catch (NoSuchFieldException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Map<String, Object> result = new HashMap<String, Object>();
			result.put("items",list);
			Constant.ORGTREE = result;
		}else if (StringUtils.equals(q, "trResum")){
			String memberSql = "select cfg_member.id, cfg_member.code,cfg_member.descr,cfg_post.descr as postDescr,cfg_member.pocaDescr from cfg_member left join cfg_post on cfg_member.post=cfg_post.id where cfg_member.delfg='0' and cfg_member.pocacode<>'' and cfg_member.code like '20%'";
			String memberRes = "memberId,memberCode,name,postDescr,pocaDescr";
			List<Map<String, Object>> memberData = service.queryBySqlToMap(memberSql, memberRes.split(","));
			String actualSql = "select id,date,students,post,hour,hour2,enable from tr_actual where enable<>'-1'";
			String actualRes = "id,date,students,post,hour,hour2,enable";
			List<Map<String, Object>> actualData = service.queryBySqlToMap(actualSql, actualRes.split(","));
			String personalSql = "select tr_personal.epnum,tr_personal.name,tr_personal.hour,tr_clst.tyds,tr_clst.level from tr_personal left join tr_clst on tr_personal.clst=tr_clst.id where tr_personal.enable='1' and tr_personal.hour<>''";
			String personalRes = "epnum,name,hour,tyds,level";
			List<Map<String, Object>> personalData = service.queryBySqlToMap(personalSql, personalRes.split(","));
			String clstSql = "select id,tyds,level,hour from tr_clst where enable='1'";
			String clstRes = "id,tyds,level,hour";
			List<Map<String, Object>> clstData = service.queryBySqlToMap(clstSql, clstRes.split(","));
			Constant.MEMBERDATA = memberData;
			Constant.ACTUALDATA = actualData;
			Constant.PERSONALDATA = personalData;
			Constant.CLSTDATA = clstData;
		}
		
	}
}
