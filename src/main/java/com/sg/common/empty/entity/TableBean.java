package com.sg.common.empty.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 *
 * @author qxw
 */
@Entity
@Table(name = "cfg_bean")
public class TableBean extends BaseTO  {
    private static final long serialVersionUID = 1L;
	public String bean="";
	public String getBean(){return this.bean;}
	public void setBean(String one){this.bean=one;}
	public String descrProperty="";
	@Column(name="descrproperty")
	public String getDescrProperty(){return this.descrProperty;}
	public void setDescrProperty(String one){this.descrProperty=one;}
	public String descr="";
	public String getDescr(){return this.descr;}
	public void setDescr(String one){this.descr=one;}
	public String tableName="";
	@Column(name="tablename")
	public String getTableName(){return this.tableName;}
	public void setTableName(String one){this.tableName=one;}
	public String isFlow="";

	@Column(name="isflow")
	public String getIsFlow(){return this.isFlow;}
	public void setIsFlow(String one){this.isFlow=one;}
	
	
	public String className="";
	public String pkg="";
	public String getPkg(){return this.pkg;}
	public void setPkg(String one){this.pkg=one;}
	public String usrct="";
	public String getUsrct(){return this.usrct;}
	public void setUsrct(String one){this.usrct=one;}
	
	@Column(name="classname")
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	//使用getset方法，导致数据映射不正常，以及取coeo时不识别
	private List<Property> propertys = new ArrayList<Property>();
}
