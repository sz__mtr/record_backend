package com.sg.common.empty.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "cfg_myinput")
public class Myinput extends BaseTO{
	private static final long serialVersionUID = 1L;
	public String descr="";
	public String getDescr(){return this.descr;}
	public void setDescr(String one){this.descr=one;}
//	public String params="";
	public String type="";
	public String getType(){return this.type;}
	public void setType(String one){this.type=one;}
	public String usrct="";
	public String getUsrct(){return this.usrct;}
	public void setUsrct(String one){this.usrct=one;}
//	public String getParams() {
//		return params;
//	}
//	public void setParams(String params) {
//		this.params = params;
//	}
}