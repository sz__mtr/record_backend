package com.sg.common.empty.entity;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sg.common.login.entity.Member;
@Entity
@Table(name = "cfg_cons")
public class Cons extends BaseTO{
	
	public String name="";
	public String getName(){return this.name;}
	public void setName(String one){this.name=one;}
	public String value="";
	public String getValue(){return this.value;}
	public void setValue(String one){this.value=one;}
	public String descr="";
	public String getDescr(){return this.descr;}
	public void setDescr(String one){this.descr=one;}
	public String ord="";
	public String getOrd(){return this.ord;}
	public void setOrd(String one){this.ord=one;}
	public Domain domain;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "domain")
	public Domain getDomain(){return this.domain;}
	public void setDomain(Domain one){this.domain=one;}
	public String enable="";
	public String getEnable(){return this.enable;}
	public void setEnable(String one){this.enable=one;}
	public String createDate="";
	public String getCreateDate(){return this.createDate;}
	public void setCreateDate(String one){this.createDate=one;}
	public Member createMember;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "createMember")
	public Member getCreateMember(){return this.createMember;}
	public void setCreateMember(Member one){this.createMember=one;}
	public String updateDate="";
	public String getUpdateDate(){return this.updateDate;}
	public void setUpdateDate(String one){this.updateDate=one;}
	public Member updateMember;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "updateMember")
	public Member getUpdateMember(){return this.updateMember;}
	public void setUpdateMember(Member one){this.updateMember=one;}
	public String ws0="";
	public String getWs0(){return this.ws0;}
	public void setWs0(String one){this.ws0=one;}
	public String ws1="";
	public String getWs1(){return this.ws1;}
	public void setWs1(String one){this.ws1=one;}
	public String ws2="";
	public String getWs2(){return this.ws2;}
	public void setWs2(String one){this.ws2=one;}
	public String ws3="";
	public String getWs3(){return this.ws3;}
	public void setWs3(String one){this.ws3=one;}
	public String ws4="";
	public String getWs4(){return this.ws4;}
	public void setWs4(String one){this.ws4=one;}
	public String ws5="";
	public String getWs5(){return this.ws5;}
	public void setWs5(String one){this.ws5=one;}
	public String ws6="";
	public String getWs6(){return this.ws6;}
	public void setWs6(String one){this.ws6=one;}
	public String ws7="";
	public String getWs7(){return this.ws7;}
	public void setWs7(String one){this.ws7=one;}
	public String ws8="";
	public String getWs8(){return this.ws8;}
	public void setWs8(String one){this.ws8=one;}
	public String ws9="";
	public String getWs9(){return this.ws9;}
	public void setWs9(String one){this.ws9=one;}
	public String usrct="";
	public String getUsrct(){return this.usrct;}
	public void setUsrct(String one){this.usrct=one;}
}