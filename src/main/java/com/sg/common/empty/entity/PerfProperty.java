package com.sg.common.empty.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//@Entity
//@Table(name = "cfg_perf_property")
public class PerfProperty extends BaseTO{
//	public Perf perf;
//	@ManyToOne(fetch=FetchType.EAGER)
//	public Perf getPerf(){return this.perf;}
//	public void setPerf(Perf one){this.perf=one;}
	public Property property;
	@ManyToOne(fetch=FetchType.EAGER)
	public Property getProperty(){return this.property;}
	public void setProperty(Property one){this.property=one;}
	public String descr="";
	public String getDescr() {return this.descr;}
	public void setDescr(String one) {this.descr = one;}
	public String esh="";
	public String getEsh(){return this.esh;}
	public void setEsh(String one){this.esh=one;}
	public String kind="";
	public String getKind(){return this.kind;}
	public void setKind(String one){this.kind=one;}
	public Myinput myinput;
	@ManyToOne(fetch=FetchType.EAGER)
	public Myinput getMyinput(){return this.myinput;}
	public void setMyinput(Myinput one){this.myinput=one;}
	public String required="";
	public String getRequired(){return this.required;}
	public void setRequired(String one){this.required=one;}
	public String valueurl="";
	public String getValueurl(){return this.valueurl;}
	public void setValueurl(String one){this.valueurl=one;}
	public String validtype="";
	public String getValidtype(){return this.validtype;}
	public void setValidtype(String one){this.validtype=one;}
	public String dftvalue="";
	public String getDftvalue(){return this.dftvalue;}
	public void setDftvalue(String one){this.dftvalue=one;}
	public String width="";
	public String getWidth(){return this.width;}
	public void setWidth(String one){this.width=one;}
	public String min="";
	public String getMin(){return this.min;}
	public void setMin(String one){this.min=one;}
	public String max="";
	public String getMax(){return this.max;}
	public void setMax(String one){this.max=one;}
	public String showvalue="";
	public String getShowvalue(){return this.showvalue;}
	public void setShowvalue(String one){this.showvalue=one;}
	public String usrct="";
	public String getUsrct(){return this.usrct;}
	public void setUsrct(String one){this.usrct=one;}
}