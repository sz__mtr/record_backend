package com.sg.common.empty.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 统一定义id的entity基类.
 * <p/>
 * 基类统一定义id的属性名称、数据类型、列名映射及生成策略.
 * 子类可重载getId()函数重定义id的列名映射和生成策略.
 *
 * @author calvin
 */
//JPA 基类的标识
@MappedSuperclass
public abstract class BaseTO implements Serializable {

	private static final long serialVersionUID = 1L;
	

//	@Autowired
//	HttpServletRequest request;
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	public BaseTO() {
		super();
//		logger.info("init baseto here;");
//		Map parameterMap = request.getParameterMap();
//		logger.info(ArrayUtils.toString(parameterMap));
		 
	}
	

    protected String id;

    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    //@GeneratedValue(strategy = GenerationType.SEQUENCE)
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(length=32)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
