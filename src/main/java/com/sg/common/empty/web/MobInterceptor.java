package com.sg.common.empty.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.sg.common.empty.aop.CustomerAop;
import com.sg.common.empty.core.DealFilter;
import com.sg.common.empty.core.DealPermission;
import com.sg.common.empty.core.EpUtil;
import com.sg.common.empty.service.CfgService;
import com.sg.common.login.entity.Member;
import com.sg.common.login.entity.Org;
import com.sg.common.login.entity.Role;
import com.sg.common.login.service.OrgMemberService;
import com.sg.common.utils.Constant;
import com.sg.common.utils.StrUtil;

public class MobInterceptor implements HandlerInterceptor {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private String[] allowUrls;

	@Autowired
	CfgService service;

	@Autowired
	OrgMemberService omService;

	/**
	 * preHandle方法是进行处理器拦截用的，顾名思义，该方法将在Controller处理之前进行调用，
	 * SpringMVC中的Interceptor拦截器是链式的，可以同时存在
	 * 多个Interceptor，然后SpringMVC会根据声明的前后顺序一个接一个的执行
	 * ，而且所有的Interceptor中的preHandle方法都会在
	 * Controller方法调用之前调用。SpringMVC的这种Interceptor链式结构也是可以进行中断的
	 * ，这种中断方式是令preHandle的返 回值为false，当preHandle的返回值为false的时候整个请求就结束了。
	 */
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		
		String requestUrl = request.getRequestURI().replace(
				request.getContextPath(), "");
		// url检测
		if (urlFilter(requestUrl)) {
			return true;
		}
		System.out.println(requestUrl + " " + request.getQueryString());
		logger.info(requestUrl + " " + request.getQueryString());

		int lastIndex = StringUtils.lastIndexOf(requestUrl, "/");
		String header = StringUtils.substring(requestUrl, 1, lastIndex)
				.replace("/", ".");
		String route = header + "."
				+ StringUtils.substring(requestUrl, lastIndex + 1);
		request.setAttribute("conf.header", header);
		request.setAttribute("conf.route", route);

		// 处理权限
		Integer type = Constant.getInt(StrUtil.dot(route, "type"));

		if (type != null && type!=1) {
			// 需登录权限
//			String memberCode = EpUtil.findMyCode(request);
			String memberCode = (String) request.getSession().getAttribute(
					Constant.SESSION_MEMBER_CODE);
			if (StringUtils.isEmpty(memberCode)) {
//				request.setAttribute("errMsg", "该功能Permission需要权限,但用户为空,返回");
//				request.getRequestDispatcher("/login").forward(request,
//						response);
				response.setStatus(401);
//				response.setContentType("application/json; charset=utf-8");
				return false;
			} else {
				DetachedCriteria dc = DetachedCriteria.forClass(Member.class);
				dc.add(Restrictions.eq("code", memberCode)).add(
						Restrictions.eq("delfg", "0"));
				Member member = service.findBean(dc);

				if (member == null) {
					logger.info("我的工号中返回的工号,在本地数据库中无法找到对应人员，返回");
					request.setAttribute("errMsg",
							"我的工号中返回的工号,在本地数据库中无法找到对应人员，返回");
					request.getRequestDispatcher("/logout").forward(request,
							response);
					return false;
				}
//				SessionUtil.sessionMsg(omService,request, memberCode, member);
			}
			// } else
			if (type == 0) {// 标准功能，需分配权限
				DealPermission dealPermission = DealPermission.permissionClazz(
						request, service, omService);
				if (dealPermission != null) {
					if (!dealPermission.getCanIn()) {
						logger.info("在我的所处的角色对应的权限里面 ，没有{}权限。", requestUrl);
						request.setAttribute("errMsg", "在我的所处的角色对应的权限里面 ，没有"
								+ requestUrl + "权限。");
						request.getRequestDispatcher("403").forward(request,
								response);
						return false;
					}
				}
			}
		}
//		if (Constant.hasPath(route + ".filter")) {
//			String filterDealClass = Constant.getT(route + ".filter.dealClass",
//					String.class);
//			DealFilter dealFilter = null;
//			if (StringUtils.isEmpty(filterDealClass)) {
//				dealFilter = new DealFilter();
//			} else {
//				Class<?> clazz;
//				try {
//					clazz = Class.forName(filterDealClass);
//					dealFilter = (DealFilter) clazz.newInstance();
//				} catch (Exception e) {
//					logger.info(e.getMessage());
//				}
//			}
//			if (dealFilter != null) {
//				dealFilter.setRequest(request);
//				dealFilter.setResponse(response);
//
//				String filtertype = Constant.getAnyRef(route + ".filter.type");
//				dealFilter.setType(filtertype);
//				String filterjs = Constant.getAnyRef(route + ".filter.js");
//				dealFilter.setFunc(filterjs);
//				String filterdescr = Constant.getAnyRef(route + ".filter.descr");
//				dealFilter.setDescr(filterdescr);
//
//				if (!dealFilter.getDataIn())
//					return false;
//			}
//		}
		return true;

	}

	private boolean urlFilter(String requestUrl) {

		if (null != getAllowUrls() && getAllowUrls().length >= 1) {
			for (String url : getAllowUrls()) {
				if (requestUrl.contains(url)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 这个方法只会在当前这个Interceptor的preHandle方法返回值为true的时候才会执行。postHandle是进行处理器拦截用的，
	 * 它的执行时间是在处理器进行处理之
	 * 后，也就是在Controller的方法调用之后执行，但是它会在DispatcherServlet进行视图的渲染之前执行
	 * ，也就是说在这个方法中你可以对ModelAndView进行操
	 * 作。这个方法的链式结构跟正常访问的方向是相反的，也就是说先声明的Interceptor拦截器该方法反而会后调用
	 * ，这跟Struts2里面的拦截器的执行过程有点像，
	 * 只是Struts2里面的intercept方法中要手动的调用ActionInvocation的invoke方法
	 * ，Struts2中调用ActionInvocation的invoke方法就是调用下一个Interceptor
	 * 或者是调用action，然后要在Interceptor之前调用的内容都写在调用invoke之前
	 * ，要在Interceptor之后调用的内容都写在调用invoke方法之后。
	 */
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		String requestUrl = request.getRequestURI().replace(
				request.getContextPath(), "");
		String afterClazz = Constant.get(requestUrl);
		if (StringUtils.isNotEmpty(afterClazz)) {
			Class<?> clazz = Class.forName(afterClazz);
			CustomerAop ca = (CustomerAop) clazz.newInstance();
			ca.setService(service);
			ca.post(request, response, handler, modelAndView);
		}
	}

	/**
	 * 该方法也是需要当前对应的Interceptor的preHandle方法的返回值为true时才会执行。该方法将在整个请求完成之后，
	 * 也就是DispatcherServlet渲染了视图执行，
	 * 这个方法的主要作用是用于清理资源的，当然这个方法也只能在当前这个Interceptor的preHandle方法的返回值为true时才会执行。
	 */
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		if (ex == null) {
			String requestUrl = request.getRequestURI().replace(
					request.getContextPath(), "");
			String afterClazz = Constant.get(requestUrl);
			if (StringUtils.isNotEmpty(afterClazz)) {
				Class<?> clazz = Class.forName(afterClazz);
				CustomerAop ca = (CustomerAop) clazz.newInstance();
				ca.setService(service);
				ca.after(request, response, handler);
			}
		}
	}

	public String[] getAllowUrls() {
		return allowUrls;
	}

	public void setAllowUrls(String[] allowUrls) {
		this.allowUrls = allowUrls;
	}

}
