package com.sg.common.empty.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sg.common.empty.core.FilterVal;
import com.sg.common.empty.service.CfgService;
import com.sg.common.utils.Constant;

@Repository
@Controller
@RequestMapping(value = "/api")
public class AddController {

//	private Logger logger = LoggerFactory.getLogger(getClass());
//
//	@Autowired
//	CfgService service;
//
//	@RequestMapping(value = "/{tb1}/{tb2}/add")
//	@ResponseBody
//	public Object add(HttpServletRequest request) throws Exception {
//		String route = (String) request.getAttribute("conf.route");
//
//		String oransql = Constant.get(route + ".sql");
//		List<String> sqlParams = buildParams(request, route);
//		service.excuteBySql(oransql, sqlParams.toArray());
//
//		Map<String, Object> map = new HashMap<String, Object>();
//		Map<String, Object> item = new HashMap<String, Object>();
//		item.put("id", sqlParams.get(0));
//		map.put("item", item);
//		map.put("code", 1);
//		return map;
//	}
//
//	@RequestMapping(value = "/{tb1}/{tb2}/update")
//	@ResponseBody
//	public Object up(HttpServletRequest request) throws Exception {
//		String route = (String) request.getAttribute("conf.route");
//
//		String oransql = Constant.get(route + ".sql");
//		List<String> sqlParams = buildParams(request, route);
//		service.excuteBySql(oransql, sqlParams.toArray());
//
//		Map<String, Object> map = new HashMap<String, Object>();
//		map.put("code", 1);
//		return map;
//	}
//
//	@RequestMapping(value = "/{tb1}/{tb2}/del")
//	@ResponseBody
//	public Object del(HttpServletRequest request) throws Exception {
//		String route = (String) request.getAttribute("conf.route");
//
//		String oransql = Constant.get(route + ".sql");
//		String ids = request.getParameter("ids");
//		Map<String, Object> map = new HashMap<String, Object>();
//		if (StringUtils.isNotEmpty(ids)) {
//			String[] split = StringUtils.split(ids, ",");
//			for (String id : split) {
//				service.excuteBySql(oransql, new String[] { id });
//			}
//			map.put("code", 1);
//		}
//		return map;
//	}
//
//	public List<String> buildParams(HttpServletRequest request, String route) {
//		List<String> ps = Constant.getStringList(route + ".params");
//		List<String> sqlParams = new ArrayList<String>();
//		for (String param : ps) {
//			String beanValue = "";
//			if (StringUtils.contains(param, "`")) {
//				String[] split = StringUtils.split(param, "`");
//				if (StringUtils.equals(split[0], "request")) {
//					beanValue = request.getParameter(split[1]);
//				}
//			} else {
//				beanValue = (String) FilterVal.init(param, service).get2();
//			}
//			sqlParams.add(beanValue);
//		}
//		return sqlParams;
//	}

}