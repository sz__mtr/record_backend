package com.sg.common.empty.web;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.sg.common.utils.Constant;
import com.sg.common.utils.StrUtil;

public class WebUtil {
	/**
	 * e,r,t => e,r,t
	 * '' => descr
	 * @param forDescr
	 * @return
	 */
	public static List<String> findDescrPropertyByFd(String forDescr) {
		List<String> descrs = new ArrayList<String>();

		if (StringUtils.isNotEmpty(forDescr)) {// 有描述字段，则将得到字段列表
			descrs = StrUtil.split(forDescr);
		} else {
			descrs.add(Constant.DESCR);
		}
		return descrs;
	}
	
}
