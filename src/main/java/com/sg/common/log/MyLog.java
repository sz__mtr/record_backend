package com.sg.common.log;

import org.apache.log4j.Logger;

@Deprecated
public class MyLog {

	private static Logger log = Logger.getLogger(MyLog.class);
	public static <T> void info(Class<T> obj, String s0){
		Logger.getLogger(obj).info(s0);
	}
	public static void info(String s0){
		log.info(s0);
	}
	public static void err(String s0){
		log.error(s0);
	}
}
