package com.sg.common.authenticate;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.sg.common.http.HttpHelper;
import com.sg.common.utils.MD5;

public class Authenticate {
	public static JSONObject user(String postPsd, String code, String url,
			String key) {
		/**
		 * 以下paramsMap生成方式、获取http认证结果的固定写法
		 */
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put("name", code);
		paramsMap.put("psd", postPsd);
		long currentTimeMillis = System.currentTimeMillis();
		String tstr = String.valueOf(currentTimeMillis);
		paramsMap.put("_t", String.valueOf(currentTimeMillis));
		Long x1 = Long.valueOf(StringUtils.substring(tstr, 0, 6));
		Long x2 = Long.valueOf(StringUtils.substring(tstr, -3, -1));
		paramsMap
				.put("_m",
						MD5.encode(String.valueOf(x1 + x2
								+ Long.valueOf(key))));
		/**
		 * res： { "errcode": 0, "errmsg": "ok" }
		 * 
		 * errcode:0-登陆成功，1-IP认证失败，2-加密认证失败,3-用户认证失败 errmsg：msg
		 */
		JSONObject res = HttpHelper.httpPost(url, paramsMap);
		return res;
	}
}
