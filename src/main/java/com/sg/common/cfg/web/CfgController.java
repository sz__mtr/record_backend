package com.sg.common.cfg.web;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sg.activiti.service.MyActivitiService;
import com.sg.common.empty.core.AsyncTree;
import com.sg.common.empty.service.CfgService;
import com.sg.common.login.entity.Org;
import com.sg.common.utils.Constant;


@Repository
@Controller
@RequestMapping(value = "/api")
public class CfgController {
	private Logger logger = Logger.getLogger(getClass());
	// final String SEPORATOR = System.getProperty("line.separator");
	@Autowired
	MyActivitiService myActivitiService;

	@Autowired
	CfgService service;
	
	@RequestMapping(value = "/cfg/org/tree")
	@ResponseBody
	public Object tree(HttpServletRequest request) throws SQLException,
			NoSuchFieldException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {

//		Org org = new Org();
//		org.setOrge("1000");
//		List<AsyncTree> list = service.findTreeValue(org, null,new ArrayList<String>(){{add("all");}}, new ArrayList<String>(){{add("descr");}});
//		Map<String, Object> result = new HashMap<String, Object>();
//		result.put("items",list);
		return Constant.ORGTREE;
				
	}

}
