package com.sg.common.format;

import java.text.NumberFormat;

public class MyNumberFormat {

	private static NumberFormat n3 = NumberFormat.getInstance();
	static {
		getN3().setMaximumFractionDigits(3);
		getN3().setGroupingUsed(false);
	}
	private static NumberFormat n2 = NumberFormat.getInstance();
	static {
		getN2().setMaximumFractionDigits(2);
		getN2().setGroupingUsed(false);
	}
	public static NumberFormat getN3() {
		return n3;
	}
	public static NumberFormat getN2() {
		return n2;
	}


	public static NumberFormat getNumberFormat(int max) {
		NumberFormat res = NumberFormat.getInstance();
		res.setMaximumFractionDigits(max);
		res.setGroupingUsed(false);
		return res;
	}

}
