package com.sg.common.login.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sg.common.empty.entity.BaseTO;

/**
 * @JoinColumn 在jpa与sessionfactory的默认实现是不同的，jpa中默认为字段名，sessionfactory中默认为字段名_id
 * @author ggg
 *
 */
@Entity
@Table(name = "cfg_org_member")
public class OrgMember extends BaseTO{
	private String orgCode="";
	private String memberCode="";
	public String delfg="";
	public String getDelfg(){return this.delfg;}
	public void setDelfg(String one){this.delfg=one;}
	public String usrct="";
	public String getUsrct(){return this.usrct;}
	public void setUsrct(String one){this.usrct=one;}
	public String oran_id="";
	public String getOran_id(){return this.oran_id;}
	public void setOran_id(String one){this.oran_id=one;}
	public Org org;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="org")
	public Org getOrg(){return this.org;}
	public void setOrg(Org one){this.org=one;}
	public Member member;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="member")
	public Member getMember(){return this.member;}
	public void setMember(Member one){this.member=one;}
	public String enable="1";
	public String getEnable(){return this.enable;}
	public void setEnable(String one){this.enable=one;}
	public String createdate="";
	public String getCreatedate(){return this.createdate;}
	public void setCreatedate(String one){this.createdate=one;}
	public Member createmember;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="createmember")
	public Member getCreatemember(){return this.createmember;}
	public void setCreatemember(Member one){this.createmember=one;}
	public String updatedate="";
	public String getUpdatedate(){return this.updatedate;}
	public void setUpdatedate(String one){this.updatedate=one;}
	public Member updatemember;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="updatemember")
	public Member getUpdatemember(){return this.updatemember;}
	public void setUpdatemember(Member one){this.updatemember=one;}
	public String ws0="";
	public String getWs0(){return this.ws0;}
	public void setWs0(String one){this.ws0=one;}
	public String ws1="";
	public String getWs1(){return this.ws1;}
	public void setWs1(String one){this.ws1=one;}
	public String ws2="";
	public String getWs2(){return this.ws2;}
	public void setWs2(String one){this.ws2=one;}
	public String ws3="";
	public String getWs3(){return this.ws3;}
	public void setWs3(String one){this.ws3=one;}
	public String ws4="";
	public String getWs4(){return this.ws4;}
	public void setWs4(String one){this.ws4=one;}
	public String ws5="";
	public String getWs5(){return this.ws5;}
	public void setWs5(String one){this.ws5=one;}
	public String ws6="";
	public String getWs6(){return this.ws6;}
	public void setWs6(String one){this.ws6=one;}
	public String ws7="";
	public String getWs7(){return this.ws7;}
	public void setWs7(String one){this.ws7=one;}
	public String ws8="";
	public String getWs8(){return this.ws8;}
	public void setWs8(String one){this.ws8=one;}
	public String ws9="";
	public String getWs9(){return this.ws9;}
	public void setWs9(String one){this.ws9=one;}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public String getMemberCode() {
		return memberCode;
	}
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
}