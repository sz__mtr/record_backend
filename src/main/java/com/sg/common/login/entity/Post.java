package com.sg.common.login.entity;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sg.common.empty.entity.BaseTO;
@Entity
@Table(name = "cfg_post")
/** 岗位  */
public class Post extends BaseTO{
	/** 编码  */
	public String code="";
	public String getCode(){return this.code;}
	public void setCode(String one){this.code=one;}
	/** 描述  */
	public String descr="";
	public String getDescr(){return this.descr;}
	public void setDescr(String one){this.descr=one;}
	/** 组织  */
	public Org org;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "org")
	public Org getOrg(){return this.org;}
	public void setOrg(Org one){this.org=one;}
	/** 组织代码  */
	public String orgCode="";
	public String getOrgCode(){return this.orgCode;}
	public void setOrgCode(String one){this.orgCode=one;}
	/** 摘要  */
	public String sgabstract="";
	public String getSgabstract(){return this.sgabstract;}
	public void setSgabstract(String one){this.sgabstract=one;}
	/** 有效?  */
	public String enable="1";
	public String getEnable(){return this.enable;}
	public void setEnable(String one){this.enable=one;}
	/** 创建日期  */
	public String createDate="";
	public String getCreateDate(){return this.createDate;}
	public void setCreateDate(String one){this.createDate=one;}
	/** 创建人  */
	public Member createMember;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "createMember")
	public Member getCreateMember(){return this.createMember;}
	public void setCreateMember(Member one){this.createMember=one;}
	/** 更新日期  */
	public String updateDate="";
	public String getUpdateDate(){return this.updateDate;}
	public void setUpdateDate(String one){this.updateDate=one;}
	/** 更新人  */
	public Member updateMember;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "updateMember")
	public Member getUpdateMember(){return this.updateMember;}
	public void setUpdateMember(Member one){this.updateMember=one;}
	/** 扩展0  */
	public String ws0="";
	public String getWs0(){return this.ws0;}
	public void setWs0(String one){this.ws0=one;}
	/** 扩展1  */
	public String ws1="";
	public String getWs1(){return this.ws1;}
	public void setWs1(String one){this.ws1=one;}
	/** 扩展2  */
	public String ws2="";
	public String getWs2(){return this.ws2;}
	public void setWs2(String one){this.ws2=one;}
	/** 扩展3  */
	public String ws3="";
	public String getWs3(){return this.ws3;}
	public void setWs3(String one){this.ws3=one;}
	/** 扩展4  */
	public String ws4="";
	public String getWs4(){return this.ws4;}
	public void setWs4(String one){this.ws4=one;}
	/** 扩展5  */
	public String ws5="";
	public String getWs5(){return this.ws5;}
	public void setWs5(String one){this.ws5=one;}
	/** 扩展6  */
	public String ws6="";
	public String getWs6(){return this.ws6;}
	public void setWs6(String one){this.ws6=one;}
	/** 扩展7  */
	public String ws7="";
	public String getWs7(){return this.ws7;}
	public void setWs7(String one){this.ws7=one;}
	/** 扩展8  */
	public String ws8="";
	public String getWs8(){return this.ws8;}
	public void setWs8(String one){this.ws8=one;}
	/** 扩展9  */
	public String ws9="";
	public String getWs9(){return this.ws9;}
	public void setWs9(String one){this.ws9=one;}
	/** 用户端  */
	public String usrct="";
	public String getUsrct(){return this.usrct;}
	public void setUsrct(String one){this.usrct=one;}
}