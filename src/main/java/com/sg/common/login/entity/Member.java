package com.sg.common.login.entity;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sg.common.empty.entity.BaseTO;
@Entity
@Table(name = "cfg_member")
/** 人员  */
public class Member extends BaseTO{
	/** 工号  */
	public String code="";
	public String getCode(){return this.code;}
	public void setCode(String one){this.code=one;}
	/** 登录账户  */
	public String loginId="";
	public String getLoginId(){return this.loginId;}
	public void setLoginId(String one){this.loginId=one;}
	/** 密码  */
	public String psd="";
	public String getPsd(){return this.psd;}
	public void setPsd(String one){this.psd=one;}
	/** 名称  */
	public String descr="";
	public String getDescr(){return this.descr;}
	public void setDescr(String one){this.descr=one;}
	/** 摘要  */
	public String sgabstract="";
	public String getSgabstract(){return this.sgabstract;}
	public void setSgabstract(String one){this.sgabstract=one;}
	/** 性别  */
	public String sex="";
	public String getSex(){return this.sex;}
	public void setSex(String one){this.sex=one;}
	/** 邮箱地址  */
	public String syuid="";
	public String getSyuid(){return this.syuid;}
	public void setSyuid(String one){this.syuid = one;}
	/** 组织  */
	public Org org;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "org")
	public Org getOrg(){return this.org;}
	public void setOrg(Org one){this.org=one;}
	/** 岗位  */
	public Post post;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "post")
	public Post getPost(){return this.post;}
	public void setPost(Post one){this.post=one;}
	/** 岗位2  */
	public Post post2;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "post2")
	public Post getPost2(){return this.post2;}
	public void setPost2(Post one){this.post2=one;}
	/** 是否确认  */
	public String confirmed="";
	public String getConfirmed(){return this.confirmed;}
	public void setConfirmed(String one){this.confirmed=one;}
	/** 是否有照片  */
	public String photeState="";
	public String getPhoteState(){return this.photeState;}
	public void setPhoteState(String one){this.photeState=one;}
	/** 是否已发送制卡  */
	public String sended="";
	public String getSended(){return this.sended;}
	public void setSended(String one){this.sended=one;}
	/** 删除标志  */
	public String delfg="";
	public String getDelfg(){return this.delfg;}
	public void setDelfg(String one){this.delfg=one;}
	/** 用户端  */
	public String usrct="";
	public String getUsrct(){return this.usrct;}
	public void setUsrct(String one){this.usrct=one;}
	/** 主数据ID  */
	public String oranId="";
	public String getOranId(){return this.oranId;}
	public void setOranId(String one){this.oranId=one;}
	/** 食堂信息 */
	public String restaurantId="";
	public String getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(String restaurantId) {
		this.restaurantId = restaurantId;
	}
	
}