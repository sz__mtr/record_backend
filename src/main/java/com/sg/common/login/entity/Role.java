package com.sg.common.login.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.sg.common.empty.entity.BaseTO;

@Entity
@Table(name = "cfg_role")
public class Role extends BaseTO{
	public int ord;
	public int getOrd(){return this.ord;}
	public void setOrd(int one){this.ord=one;}
	public String role="";
	public String getRole(){return this.role;}
	public void setRole(String one){this.role=one;}
	public String descr="";
	public String getDescr(){return this.descr;}
	public void setDescr(String one){this.descr=one;}
	public String usrct="";
	public String getUsrct(){return this.usrct;}
	public void setUsrct(String one){this.usrct=one;}
}