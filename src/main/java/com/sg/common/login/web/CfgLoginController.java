package com.sg.common.login.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sg.common.empty.core.EpUtil;
import com.sg.common.empty.json.Permission;
import com.sg.common.empty.service.CfgService;
import com.sg.common.empty.web.SessionUtil;
import com.sg.common.http.HttpHelper;
import com.sg.common.login.entity.Member;
import com.sg.common.login.entity.Org;
import com.sg.common.login.entity.Role;
import com.sg.common.login.service.OrgMemberService;
import com.sg.common.utils.CollUtil;
import com.sg.common.utils.Constant;

@Repository
@Controller
public class CfgLoginController {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	CfgService service;
	@Autowired
	OrgMemberService omService;

	@RequestMapping(value = "/api/login/login", method = RequestMethod.POST)
	@ResponseBody
	public Object loginApi(HttpServletRequest request) {
		Map<String, Object> res = new HashMap<String, Object>();
		String token = createToken();
		res.put("token", token);
		String yornsso = Constant.get("yornsso");
		String myCode="";
		int httpCode=2000;
		String httpMsg="";
		boolean sso = yornsso.equals("yes");
		myCode = sso?EpUtil.findMyCode(request):request.getParameter("username");
		boolean notEmptyCode = StringUtils.isNotEmpty(myCode);
		httpCode=notEmptyCode?httpCode:sso?4021:4022;
		httpMsg=notEmptyCode?httpMsg:sso?"统一登录返回用户ID为空":"本地登录用户ID为空";
		
		if(StringUtils.isNotEmpty(myCode)){
			DetachedCriteria dc = DetachedCriteria.forClass(Member.class);
			dc.add(Restrictions.eq("code", myCode)).add(
					Restrictions.eq("delfg", "0"));
			Member member = service.findBean(dc);
			if(member!=null){
				String roleRes = SessionUtil.sessionMsg(omService,request, myCode, member);
				if(StringUtils.equals(roleRes, "error")){
					httpCode = 4023;
					httpMsg = "未找到该用户角色" +myCode +",请重新登录"; 
				}
			}else{
				httpCode=4023;
				httpMsg="在本地数据库未找到该用户："+myCode;
			}
		}
		res.put("code", httpCode);
		res.put("msg", httpMsg);
		return res;
	}

	private String createToken() {
		// TODO Auto-generated method stub
		return "admin";// 这里token只用于前端判断是否需要登陆
	}

	@RequestMapping(value = "/api/user/info", method = RequestMethod.GET)
	@ResponseBody
	public Object logininfoApi(HttpServletRequest request) {

		Map<String, Object> res = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		res.put("name", Constant.getSessionMember(request).getDescr());
		res.put("memCode", Constant.getSessionMember(request).getCode());
		Org myWorkShop = (Org)session.getAttribute(Constant.SESSION_MYWORKSHOPORG);
		if(myWorkShop!=null){
			res.put("myWorkShop", myWorkShop.getId());
		}
		
		Org myStationOrg = (Org)session.getAttribute(Constant.SESSION_MYSTATIONORG);
		if(myStationOrg!=null){
			res.put("myStationOrg", myStationOrg);
		}
		
		// List<Role> myRoles = (List<Role>)
		// session.getAttribute(Constant.SESSION_MYROLES);
		// if(CollUtil.isNotEmpty(myRoles)){
		// List<String> rs=new ArrayList<String>();
		// for(Role r:myRoles){
		// rs.add(r.getRole());
		// }
		// res.put("roles", rs);
		// }
//		res.put("myStationOrg", Constant.getSessionMember(request).getCode());
		List<String> myRoles = (List<String>) session
				.getAttribute(Constant.SESSION_MYROLES_CODE);
		res.put("roles", myRoles.toArray());
		res.put("code", 2000);
		return res;
	}

	@RequestMapping(value = "/api/login/logout", method = RequestMethod.POST)
	@ResponseBody
	public Object loginlogout(HttpServletRequest request) {
		String yornsso = Constant.get("yornsso");
		Map<String, Object> res = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		session.invalidate();
		if (yornsso.equals("yes")) {
			Map<String, String> mip = new HashMap<String, String>();
			mip.put("clientIp", request.getRemoteAddr());
			HttpHelper.httpPost(Constant.get("logout"), mip);
			res.put("autologout", Constant.get("autologout"));
		}
		return res;
	}

	/**
	 * 
	 取得当前的账号信息（无论单点与否） 判断账号是否为空 否 跳转到home页面 是 若单点情况跳转到单点服务器登陆页面 若非，则跳转到本地登陆页面
	 * (登陆成功后跳转到home页面)
	 * 
	 * @param request
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	@ResponseBody
	public Object loginPage(HttpServletRequest request)
			throws ServletException, IOException {
		String myCode = EpUtil.findMyCode(request);
		if (StringUtils.isNotEmpty(myCode)) {
			// request.getRequestDispatcher("/cfg/common/home").forward(request,
			// response);

			return new ModelAndView("redirect:/cfg/common/home");
		}
		String yornsso = Constant.get("yornsso");
		if (yornsso.equals("yes")) {
			ModelAndView modelAndView = new ModelAndView("redirect:"
					+ Constant.get("autologin"));
			Map<String, Object> model = modelAndView.getModel();
			model.put("sName", Constant.get("PROJECT_NAME"));
			String url = request.getRequestURL().toString();
			// 获取请求的参数字符串
			String queryString = request.getQueryString();
			if (StringUtils.isNotEmpty(queryString))
				url = url + "?" + queryString;
			model.put("returnUrl", java.net.URLEncoder.encode(url, "utf-8"));

			return modelAndView;
		} else {
			ModelAndView res = new ModelAndView("/jsp/login");
			return res;
		}

	}

	/**
	 * 只用于非单点登录情况下的登陆post请求
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(HttpServletRequest request) {

		String memberCode = request.getParameter("_memberCode");
		DetachedCriteria dc = DetachedCriteria.forClass(Member.class);
		dc.add(Restrictions.eq("code", memberCode)).add(
				Restrictions.eq("delfg", "0"));
		Member member = service.findBean(dc);
		HttpSession session = request.getSession();
		session.setAttribute(Constant.SESSION_MEMBER, member);
		session.setAttribute(Constant.SESSION_MEMBER_CODE, memberCode);
		List<Org> myOrgs = omService.findMyOrgs(member);
		session.setAttribute(Constant.SESSION_MYORGS, myOrgs);
		session.setAttribute(Constant.SESSION_MYORGS_INDIRECT,
				omService.findMyOrgsIndirect(member));
		List<Org> myManageOrgs = omService.bsyManageOrgs(myOrgs);
		session.setAttribute(Constant.SESSION_MANAGE_ORGS_BSY, myManageOrgs);
		List<Role> myRoles = omService.findMyRoles(request, member);
		session.setAttribute(Constant.SESSION_MYROLES, myRoles);
		String url = request.getParameter("_url");
		String sysp = "";
		if (url.contains("sys=")) {
			sysp = url.substring(StringUtils.indexOf(url, "sys="));
		}
		if (StringUtils.isNotEmpty(url) && url.contains("home")
				|| url.contains("login") || url.contains("logout")) {
			url = "";
		}
		if (StringUtils.isNotEmpty(url)) {
			return "redirect:" + url;
		} else {
			if (StringUtils.isNotEmpty(sysp)) {
				return "redirect:/cfg/common/home?" + sysp;
			} else {
				return "redirect:/cfg/common/home";
			}
		}
	}

	@RequestMapping(value = "/cfg/common/home")
	@ResponseBody
	public Object home(HttpServletRequest request) {
		ModelAndView res = new ModelAndView("/jsp/homex");

		// res.addObject("navHtml", createMenu2(request));
		return res;
	}

	//
	// private String createMenu2(HttpServletRequest request) {
	// Member member = (Member) request.getSession().getAttribute(
	// Constant.SESSION_MEMBER);
	// List<Role> myRoles = (List<Role>) request.getSession().getAttribute(
	// Constant.SESSION_MYROLES);
	// String sys = (String) request.getParameter("sys");
	// StringBuilder nav = new StringBuilder();
	// nav.append("<div class='easyui-panel' style='padding:5px;'>");
	// StringBuilder nav2 = new StringBuilder();
	// List<Permission> rpl = omService.findMyPermissions(myRoles, "1");
	// rpl.addAll(omService.findMyAddPermissionsWithFullD(myRoles));
	//
	// List<String> uri0List = new ArrayList<String>();
	// DetachedCriteria dc = DetachedCriteria.forClass(Permission.class);
	// dc.add(Restrictions.eq("type", "3"))
	// .add(Restrictions.eq("enable", "1"))
	// .add(Restrictions.eq("enable", "1"));
	// if (StringUtils.isNotEmpty(sys)) {
	// uri0List.add("cfg");
	// uri0List.add("acti");
	// String[] syss = sys.split(",");
	// for (String s : syss) {
	// uri0List.add(s);
	// }
	// dc.add(Restrictions.in("uri0", uri0List));
	// }
	//
	// List<Permission> p3 = service.find(dc);
	// for (Permission p : p3) {
	// // nav.append(
	// // "<a  class='easyui-menubutton' data-options=\"menu:'#"
	// // + p.getId() + "'\" href='javascript:void(0)' >")
	// // .append(p.getDescr()).append("</a>");
	// List<Permission> p2 = new ArrayList<Permission>();
	// for (Permission rp : rpl) {
	// if (rp.getUri0().equals(p.getUri0())) {
	// p2.add(rp);
	// }
	// }
	// if (CollUtil.isNotEmpty(p2)) {
	// // nav2.append("<div id='" + p.getId() + "'>");
	// for (Permission p1 : p2) {
	// String descr = p1.getDescr();
	// if (StringUtils.isNotEmpty(p1.getFullDescr())) {
	// descr = p1.getFullDescr();
	// }
	// nav2.append("<div onclick='javascript:addTab(\"")
	// .append(descr).append("\",\"").append(p1.uri())
	// .append("\")'>").append(descr).append("</div>");
	// }
	// nav2.append("</div>");
	// }
	// }
	// // 人员，退出
	// nav.append("<a  class='easyui-menubutton' data-options=\"menu:'#"
	// + member.getCode() + "'\" href='javascript:void(0)'>"
	// + member.getDescr() + "</a>");
	// nav2.append("<div id='" + member.getCode() + "'>");
	// nav2.append(
	// " <div onclick=\"javascript:window.open('"
	// + Constant.get("psdChangeUrl") + "')\">")
	// .append("修改密码").append("</div>");
	// nav2.append(
	// "<div onclick=\"javascript:window.location.href='"
	// + Constant.get("URL") + "logout'\"").append("\")'>")
	// .append("退出").append("</div>");
	// nav2.append("</div>");
	// // 关于
	// if (StringUtils.isNotEmpty(sys)) {
	// String[] syss = sys.split(",");
	// nav.append("<a  class='easyui-menubutton' data-options=\"menu:'#about'\" href='javascript:void(0)'>关于</a>");
	// nav2.append("<div id='about' class=\"menu-content\" style=\"background:#f0f0f0;padding:10px;text-align:left\">");
	// nav2.append(" <img src=\""
	// + Constant.get("ABOUT_PICTURE_URL_" + syss[0])
	// + "\" style=\"width:315px;height:201px\">");
	// nav2.append("<p style=\"font-size:14px;color:#444;\">"
	// + Constant.get("ABOUT_DESCR_URL_" + syss[0]) + "</p></div>");
	// } else {
	// nav.append("<a  class='easyui-menubutton' data-options=\"menu:'#about'\" href='javascript:void(0)'>关于</a>");
	// nav2.append("<div id='about' class=\"menu-content\" style=\"background:#f0f0f0;padding:10px;text-align:left\">");
	// nav2.append(" <img src=\"" + Constant.get("ABOUT_PICTURE_URL")
	// + "\" style=\"width:315px;height:201px\">");
	// nav2.append("<p style=\"font-size:14px;color:#444;\">"
	// + Constant.get("ABOUT_DESCR_URL") + "</p></div>");
	// }
	// // 外部链接
	// Map<String, String> links = new HashMap<String, String>();
	// if (StringUtils.isNotEmpty(sys)) {
	// String[] syss = sys.split(",");
	// links = Constant.getStart("MENU_LINK_URL_" + syss[0]);
	// } else {
	// links = Constant.getStart("MENU_LINK_URL");
	// }
	// if (!links.isEmpty()) {
	// nav.append("<a  class='easyui-menubutton' data-options=\"menu:'#link'\" href='javascript:void(0)'>相关链接</a>");
	// nav2.append("<div id='link'>");
	// Set<String> linkKeys = links.keySet();
	// for (String key : linkKeys) {
	//
	// nav2.append(
	// " <div onclick=\"javascript:window.open('"
	// + Constant.get(key) + "')\">")
	// .append(Constant.get("DESCR_" + key)).append("</div>");
	//
	// }
	// nav2.append("</div>");
	// }
	// nav.append("</div>");
	// return nav.append(nav2).toString();
	// }
	//
	// private String createMenu(Member member) {
	// StringBuilder nav = new StringBuilder();
	// List<Permission> rpl = omService.findMyPermissions(member, "1");
	// DetachedCriteria dc = DetachedCriteria.forClass(Permission.class);
	// dc.add(Restrictions.eq("type", "3"))
	// .add(Restrictions.eq("enable", "1"));
	// List<Permission> p3 = service.find(dc);
	// nav.append("<li><a id='zy' href='javascript:void(0)' onclick='javascript:addTab(\"\",\"主页\",\"\");'>主页</a></li>");
	// for (Permission p : p3) {
	// nav.append(
	// "<li class='dropdown'><a href='javascript:void(0)' data-toggle=\"dropdown\" class=\"dropdown-toggle\">")
	// .append(p.getDescr()).append("<b class=\"caret\"></b></a>");
	// List<Permission> p2 = new ArrayList<Permission>();
	// for (Permission rp : rpl) {
	// if (rp.getUri0().equals(p.getUri0())) {
	// p2.add(rp);
	// }
	// }
	// if (CollUtil.isNotEmpty(p2)) {
	// nav.append("<ul class='dropdown-menu' id='menu1'>");
	// for (Permission p1 : p2) {
	// nav.append(
	// "<li><a href='javascript:void(0)' onclick='javascript:addTab(\"")
	// .append(p.getDescr())
	// .append("\",\"")
	// .append(p1.getDescr())
	// .append("\",\"")
	// .append(p1.uri())
	// .append("\")'><i class=\"icon-chevron-right\"></i>")
	// .append(p1.getDescr()).append("</a> </li>");
	// }
	// nav.append("</ul>");
	// }
	// nav.append("</li>");
	// }
	// return nav.toString();
	// // StringBuilder nav = new StringBuilder();
	// // //我拥有的功能类型为1的权限
	// // List<Permission> rpl = myPermissions();
	// //
	// // //我拥有的所以功能的权限
	// // List<Permission> arpl = myallPermissions();
	// //
	// //
	// // DetachedCriteria dc = DetachedCriteria.forClass(Menu.class);
	// // dc.add(Restrictions.eq("level", "1")).add(
	// // Restrictions.eq("enable", "1"));
	// // dc.addOrder(Order.asc("ord"));
	// // List<Menu> ml1 = baseDao.find(dc);
	// //
	// //
	// nav.append("<li><a id='zy' href='javascript:void(0)' onclick='javascript:addTab(\"\",\"主页\",\"\");'>主页</a></li>");
	// // for (Menu m : ml1) {
	// // if (iscontainMenu(m, arpl)) {
	// // nav.append(
	// //
	// "<li class='dropdown'><a href='javascript:void(0)' data-toggle=\"dropdown\" class=\"dropdown-toggle\">")
	// // .append(m.getPermission().getDescr())
	// // .append("<b class=\"caret\"></b></a>");
	// // dc = DetachedCriteria.forClass(Menu.class);
	// // dc.add(Restrictions.eq("level", "2"))
	// // .add(Restrictions.eq("enable", "1"))
	// // .add(Restrictions.eq("parentPermission",
	// // m.getPermission()));
	// // dc.addOrder(Order.asc("ord"));
	// // List<Menu> ml2 = baseDao.find(dc);
	// // List<Permission> pl2 = new ArrayList<Permission>();
	// // for (Menu m2 : ml2) {
	// // Permission p2 = isMenuChild(m2, rpl);
	// // if (p2 != null) {
	// // pl2.add(p2);
	// // }
	// // }
	// // if (Util.notEmptyList(pl2)) {
	// // nav.append("<ul class='dropdown-menu' id='menu1'>");
	// // for (Permission p1 : pl2) {
	// // String urlstr = p1.getNamespace() + "_"
	// // + p1.getActionName() + Constant.URL_SUFFIX
	// // + "?isPage=1";
	// // nav.append(
	// // "<li><a href='javascript:void(0)' onclick='javascript:addTab(\"")
	// // .append(m.getPermission().getDescr())
	// // .append("\",\"")
	// // .append(p1.getDescr())
	// // .append("\",\"")
	// // .append(urlstr)
	// // .append("\")'><i class=\"icon-chevron-right\"></i>")
	// // .append(p1.getDescr()).append("</a> </li>");
	// // }
	// // nav.append("</ul>");
	// // }
	// // }
	// // }
	// // nav.append("</li>");
	// // setNavHtml(nav.toString());
	// }
	//
	// private Permission findP3(Permission permission) {
	// if ("3".equals(permission.type)) {
	// return permission;
	// }
	// // else {
	// // Permission parent = permission.getParent();
	// // if (parent != null) {
	// // Permission pp = service.get(Permission.class, parent.getId());
	// // return findP3(pp);
	// // }
	// // }
	// return null;
	// }

	// private Permission isMenuChild(Menu m2, List<Permission> rpl) {
	// for (Permission p : rpl) {
	// if ((p.getParent() != null
	// &&
	// p.getParent().getId().equals(m2.getPermission().getId()))||(p.getId().equals(m2.getPermission().getId())))
	// return p;
	// }
	// return null;
	// }
	//
	// private boolean iscontainMenu(Menu pm, List<Permission> pl3) {
	// for (Permission p3 : pl3) {
	// if (pm.getPermission().getId().equals(p3.getId()))
	// return true;
	// }
	// return false;
	// }

	// private List<Permission> myPermissions2() {
	// List<Role> myrl = new ArrayList<Role>();
	// DetachedCriteria dc = DetachedCriteria.forClass(OrgRoleMember.class);
	// dc.add(Restrictions.or(Restrictions.eq("member",
	// user),Restrictions.eq("org", user.getOrg())));//或者我的部门在org下面
	// List<OrgRoleMember> orpl = baseDao.find(dc);
	// List<Permission> rpl=new ArrayList<Permission>();
	// if(Util.notEmptyList(orpl)){
	// for(OrgRoleMember orp : orpl){
	// OrgRole orgRole = orp.getOrgRole();
	// myrl.add(orgRole.getRole());
	// }
	//
	// dc = DetachedCriteria.forClass(RolePermission.class);
	// dc.createAlias("permission", "p");
	// dc.add(Restrictions.in("role", myrl)).add(Restrictions.eq("p.enable",
	// "1"))
	// .add(Restrictions.eq("p.type", "1"));
	// ProjectionList projectionList = Projections.projectionList();
	// projectionList.add(Projections.property("permission"));
	// dc.setProjection(Projections.distinct(projectionList));//防止多个角色下的功能重复。
	// rpl = baseDao.find(dc);
	// }
	// return rpl;
	// }
	// private void createLeftPagebak() {
	// StringBuilder body = new StringBuilder();
	// body.append("<div  region='west' split='true' title='导航栏' style='width:280px;padding1:1px;overflow:hidden;'>")
	// .append("<div class='easyui-accordion' fit='true' border='false'>");
	// List<Role> myrl = new ArrayList<Role>();
	// DetachedCriteria dc = DetachedCriteria.forClass(OrgRoleMember.class);
	// dc.add(Restrictions.eq("member", user));
	// List<OrgRoleMember> orpl = baseDao.find(dc);
	//
	// if(Util.notEmptyList(orpl)){
	// for(OrgRoleMember orp : orpl){
	// OrgRole orgRole = orp.getOrgRole();
	// myrl.add(orgRole.getRole());
	// }
	//
	// dc = DetachedCriteria.forClass(RolePermission.class);
	// dc.createAlias("permission", "p");
	// dc.add(Restrictions.in("role", myrl)).add(Restrictions.eq("p.enable",
	// "1"))
	// .add(Restrictions.eq("p.type", "1"));
	// List<RolePermission> rpl = baseDao.find(dc);
	// body.append("<div title='").append("链接").append("' style='padding:10px;'>");
	// for(RolePermission rp : rpl){
	// Permission permission = rp.getPermission();
	// String urlstr =
	// permission.getNamespace()+"/"+permission.getActionName()+Constant.URL_SUFFIX+"?isPage=1";
	// body.append("<a class='easyui-linkbutton' iconCls='icon-daily' plain='true' href='javascript:void(0)' onclick='javascript:window.parent.addTab(\"")
	// .append(permission.getDescr()).append("\",\"")
	// .append(urlstr).append("\")'><span>").append(permission.getDescr()).append("</span></a><p/>");
	// }
	// body.append("</div>");
	//
	// }
	// body.append("</div></div>");
	// setBodyHtml(body.toString());
	// }
	@RequestMapping(value = "/logout")
	@ResponseBody
	public Object logout(HttpServletRequest request) throws IOException {
		String yornsso = Constant.get("yornsso");
		request.getSession().invalidate();
		if (yornsso.equals("yes")) {
			Map<String, String> mip = new HashMap<String, String>();
			mip.put("clientIp", request.getRemoteAddr());
			HttpHelper.httpPost(Constant.get("logout"), mip);
			return new ModelAndView("redirect:" + Constant.get("autologout"));
		}
		ModelAndView res = new ModelAndView("/jsp/login");
		return res;
	}

}
