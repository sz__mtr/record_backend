package com.sg.common.http;

import java.util.Map;

public class ServerUtil {

	public static void checkRefuse(String gsmcMsg){
		
	}


	public static String prepareParam(Map<String, Object> paramMap) {
		StringBuffer sb = new StringBuffer();
		if (paramMap.isEmpty()) {
			return "";
		} else {
			for (String key : paramMap.keySet()) {
				String value = String.valueOf(paramMap.get(key));
				if (sb.length() < 1) {
					sb.append(key).append("=").append(value);
				} else {
					sb.append("&").append(key).append("=").append(value);
				}
			}
			return sb.toString();
		}
	}
}
