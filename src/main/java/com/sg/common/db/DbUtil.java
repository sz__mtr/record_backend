package com.sg.common.db;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * 数据库连接生成类
 * 
 * @author qianxiaowei
 *
 */
public class DbUtil {
	
	public static Connection flydymCon() throws SQLException{
		return createCon("flydym");
	}
	
	public static Connection csCon() throws SQLException{
		return createCon("cs");
	}
	public static Connection uuvCon() throws SQLException{
		return createCon("uuv");
	}
	
	public static Connection info1Con() throws SQLException{
		return createCon("info1");
	}
	
	public static Connection mxCon() throws SQLException{
		return createCon("mx");
	}
	
	public static Connection mdmCon() throws SQLException{
		return createCon("mdm");
	}
	
	public static Connection tcksCon() throws SQLException{
		return createCon("tcks");
	}
	
	public static Connection wwsmsCon() throws SQLException{
		return createCon("wwsms");
	}
	
	public static Connection dingCon() throws SQLException{
		return createCon("ding");
	}
	
	public static Connection cusCon(String conStr) throws SQLException{
		return createCon(conStr);
	}
	
	public static Connection createCon(String propertyFile) throws SQLException{
		return DruidPoolFac.getInstance(propertyFile).getConnection();
	}
}
