package com.sg.common.db;

import java.util.HashMap;
import java.util.Map;

/**
 * Druid数据库连接池的单例工厂创建类
 * 
 * @author qianxiaowei
 *
 */
public class DruidPoolFac  {
	private static Map<String,DruidPool> databasePoolMap = new HashMap<String, DruidPool>();
    public static synchronized DruidPool getInstance(String propertyFile) {
    	DruidPool databasePool;
        if (!databasePoolMap.containsKey(propertyFile)||databasePoolMap.get(propertyFile)==null) {
        	databasePool = new DruidPool(propertyFile);
            databasePoolMap.put(propertyFile, databasePool);
        }else{
        	databasePool = databasePoolMap.get(propertyFile);
        }
        return databasePool;
    }
}
