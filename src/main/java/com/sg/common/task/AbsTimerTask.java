package com.sg.common.task;

import java.util.TimerTask;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

/**
 * 定时任务接口，自定义任务需要继承该抽象类
 * @author qianxiaowei
 *
 */
public abstract class AbsTimerTask {

//	Logger log = Logger.getLogger(AbsTimerTask.class);
	public ApplicationContext ctx;
	
	public abstract TimerTask createTimerTask();

	public void setCtx(ApplicationContext ctx) {
		this.ctx = ctx;
	}
	
}
