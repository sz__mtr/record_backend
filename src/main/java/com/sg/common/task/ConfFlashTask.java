package com.sg.common.task;

import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.sg.common.dao.BaseDao;
import com.sg.common.properties.PropertyFileUtil;
import com.sg.common.utils.Constant;

public class ConfFlashTask extends AbsTimerTask{
	private Logger log = Logger.getLogger(ConfFlashTask.class);
	@Override
	public TimerTask createTimerTask() {
		
		return new TimerTask() {
			
			@Override
			public void run() {

				log.info("flash x.conf");
				PropertyFileUtil.flushCONF(Constant.PROPERTIES_PATH);
			}
		};
	}

}
