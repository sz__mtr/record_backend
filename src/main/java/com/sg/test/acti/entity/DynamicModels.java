package com.sg.test.acti.entity;
import java.util.HashMap;
import java.util.Map;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;

public class DynamicModels {

        /**
         * @param args
         */
        public static void main(String[] args) {
                // TODO Auto-generated method stub

                //读取Customer配置文件
                Configuration config = new Configuration().configure().setProperty(Environment.FORMAT_SQL, "true").addResource("com/sg/test/acti/entity/TCustomer.hbm.xml");
                
                //通过配置文件建表
                SchemaUpdate schemaUpdate = new SchemaUpdate(config);
                schemaUpdate.execute(true, true);
                
                //利用动态模型操作数据库
                SessionFactory factory = config.buildSessionFactory();
                Session s = factory.openSession();
                Transaction tx =  s.beginTransaction();
                // Create a customer
                Map david = new HashMap();
                david.put("name", "David");
                // Create an organization
                Map foobar = new HashMap();
                foobar.put("name", "Foobar Inc.");
                // Link both
                david.put("organization", foobar);
                // Save both
                s.save("TCustomer", david);
                //s.save("Organization", foobar);
                tx.commit();
                s.close();
        }

}

