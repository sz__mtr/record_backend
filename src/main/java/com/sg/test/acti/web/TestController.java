package com.sg.test.acti.web;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sg.activiti.service.MyActivitiService;
import com.sg.common.login.service.OrgMemberService;
import com.sg.common.utils.UUIDGenerator;
import com.sg.test.acti.service.TestService;

@Repository
@Controller
public class TestController {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	HttpServletRequest request;

	@Autowired
	TestService service;
	@Autowired
	OrgMemberService orgMemberService;
	@Autowired
	MyActivitiService myActivitiService;
	
//	@RequestMapping(value = "/test", produces = "application/json; charset=utf-8")
	@RequestMapping(value = "/test")
	@ResponseBody
	public Object add() throws Exception {
//		ApplyBill one = new ApplyBill();
//		one.setId("8a8aa6af5895881b015895884c600000");
//		int execute = service.execute("update ApplyItem set state='1' where applyBill=? and enable='1'",new Object[]{one});
//		System.out.println(execute);
		for(int j=0;j<10000;j++)
		{
			String x = new UUIDGenerator().generate().toString();
			System.out.println(x);
		}
		//SendMailForScard.sendMail(orgMemberService, myActivitiService);
//		String aString=DesUtils.encrypt("123456","sgyyxxhk");
//		System.out.println("123456");
//		System.out.println(aString);
//		DesUtils.decrypt(aString, "sgyyxxhk");
		/*Object bean = service.get(TkConstant.SCARDMAKE_EN, "8a8aa6af56afc8840156afdd977c0000");
		DetachedCriteria dc = DetachedCriteria.forEntityName(TkConstant.CARD_EN);
		dc.add(Restrictions.eq("scardMake", bean));
		List<Object> ol = service.find(dc);*/
//		return ol.size();
//		CsUtil.cs(orgMemberService, myActivitiService);
//		Map<String, Object> bean = new HashMap<String, Object>();
//		bean.put("no", DateTimeUtils.formatDate(new Date(System.currentTimeMillis()), "yyMMddHH"));
//		bean.put("amount", "1");
//		service.save("com.sg.dym.tk.entity.ScardMake", bean);
//		MdmUtil.synchroMember(service);
//		MdmUtil.synchroOrg(service);
//		MdmUtil.synchroOrgMember(service);
//		return UUID.randomUUID().toString();
//		return new UUIDGenerator().generate().toString();
//		String orgSelSql = "select orgCode,orge,descr,parentId,delfg,orglv from view_tcks_cfg_org order by orge,orgCode";
//		String orgInsertSql = "INSERT INTO cfg_org (id,code,orge,descr,parent,delfg,orglv) VALUES(?,?,?,?,?,?,?)";
//		String orgUpdateSql = "update cfg_org set orge=?,descr=?,parent=?,delfg=?,orglv=? where code=?";
//		String comparePropertyName = "code";
//		String memberSelSql = "select epnum,epnum,ename,confirmed,delfg from view_tcks_cfg_member order by epnum";
//		String memberInsertSql = "INSERT INTO cfg_member (id,code,loginId,descr,confirmed,delfg) VALUES(?,?,?,?,?,?)";
//		String memberUpdateSql = "update cfg_member set loginId=?,descr=?,confirmed=?,delfg=? where code=?";
//		String cusConStr = "tcks";
////		SyncUtil.sync(service,cusConStr,orgSelSql,Org.class,orgInsertSql,orgUpdateSql,comparePropertyName);
////		SyncUtil.sync(service,cusConStr,memberSelSql,Member.class,memberInsertSql,memberUpdateSql,comparePropertyName);
//		MdmUtil.synchroOrgMember(service);
		
//		HashMap hashMap = new HashMap();
//		hashMap.put("kk", "看见");
//		return hashMap;
//		return "f分";
		return "";
	}

    /**
     * 任务列表
     *
     *
     * @param leave
     * @throws NoSuchMethodException 
     * @throws InvocationTargetException 
     * @throws IllegalAccessException 
     * @throws ClassNotFoundException 
     */
    @RequestMapping(value = "/acti/mytask")
    @ResponseBody
    public Object taskList(HttpSession session, HttpServletRequest request) throws ClassNotFoundException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {

//    	return service.findTodoTasks(TEST_USER_ID);
//    	Map<String,Object> map = new HashMap<String,Object>();
//    	map.put("name", "n1");
//    	service.save("TCustomer",map);
//    	Map<String,Object> map = new HashMap<String,Object>();
//    	map.put("name", "org2");
//    	map.put("code", "code2");
//    	service.save("Test_Org",map);
//    	Map<String,Object> map2 = new HashMap<String,Object>();
//    	map2.put("name", "n1");
//    	map2.put("address", "ad1");
//    	map2.put("org", map);
//    	service.save("Test_TCustomer",map2);
//    	DetachedCriteria dc = DetachedCriteria.forEntityName("Test_TCustomer");
//    	dc.add(Restrictions.eq("name", "n1"));
//    	List<Object> find = service.find(dc);
//    	for(Object obj:find){
//    		
//    		Object object = ReUtils.get(obj, "name");
//    		System.out.print(object);
//    	}
    	
    	
//    	DetachedCriteria dc = DetachedCriteria.forClass(TableBean.class);
//    	List<TableBean> tbl = service.find(dc);
//    	for(TableBean tb:tbl){
//    		dc = DetachedCriteria.forClass(Property.class);
//    		List<Property> pl = service.find(dc);
//    		StringBuilder pros = new StringBuilder("[");
//    		for(Property p:pl){
//    			pros.append("{").append("\"name\"").append(p.getProperty());
//    		}
//    		pros.append("]");
//    	}
//    	
    	service.testtra();
    	return null;
    }
    
    
}
