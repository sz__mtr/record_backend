package com.sg.test.acti.service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.activiti.engine.HistoryService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sg.activiti.ext.Todo;
import com.sg.common.dao.BaseDao;
import com.sg.common.empty.entity.TableBean;
import com.sg.common.empty.service.TableBeanService;
import com.sg.common.login.entity.Member;
import com.sg.common.login.entity.Org;
import com.sg.common.login.entity.OrgMember;
import com.sg.common.utils.DateTimeUtils;
import com.sg.common.utils.ReUtils;

@Component
public class TestService extends BaseDao {

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	protected TaskService taskService;

	@Autowired
	protected HistoryService historyService;

	@Autowired
	protected RepositoryService repositoryService;

	@Autowired
	private IdentityService identityService;

	@Autowired
	private TableBeanService tbService;
	
	@Transactional
	public Object testtra(){
		Member mem = new Member();
		mem.setLoginId("test");
		
		save(mem);
		System.out.println(1/0);
		return null;
	}
	
	@Transactional
	public ProcessInstance startWorkflow(String className,String businessKey,
			Map<String, Object> variables) throws IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		//save(bean);
		//String businessKey = ReUtils.getStr(bean, "id");

		ProcessInstance processInstance = null;
		try {
//			identityService.setAuthenticatedUserId(ReUtils.getStr(bean,
//					"member.id"));

			processInstance = runtimeService.startProcessInstanceByKey(className, businessKey, variables);
			String processInstanceId = processInstance.getId();
			// entity.setProcessInstanceId(processInstanceId);
		} finally {
			identityService.setAuthenticatedUserId(null);
		}
		return processInstance;
	}

	/**
	 * 查询待办任务
	 * 
	 * @param userId
	 *            用户ID
	 * @return
	 * @throws ClassNotFoundException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 */
	@Transactional(readOnly = true)
	public List<Todo> findTodoTasks(String userId) throws ClassNotFoundException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		List<Todo> results = new ArrayList<Todo>();

		// find member by id
		Member mem = get(Member.class,userId)
				;
		
		// find tasks by member's code
		TaskQuery taskQuery1 = taskService.createTaskQuery()
				.taskCandidateOrAssigned(mem.getId());
		// find member's groups
		DetachedCriteria dc = DetachedCriteria.forClass(OrgMember.class);
		dc.add(Restrictions.eq("member", mem));
		List<OrgMember> list = find(dc);
        List<String> gl = new ArrayList<String>();
        for(OrgMember om:list){
        	Org org = om.getOrg();
			gl.add(org.getCode());
        }
		// find tasks by groups
        TaskQuery taskQuery2 = taskService.createTaskQuery()
				.taskCandidateGroupIn(gl);
		// distinct tasks to mytasks
        List<Task> tasks = taskQuery1.list();
		tasks.addAll(taskQuery2.list());
        
		// 根据当前人的ID查询
//		TaskQuery taskQuery = taskService.createTaskQuery()
//				.taskCandidateOrAssigned("leaderuser");// taskCandidateGroup("002");

		// 根据流程的业务ID查询实体并关联
		for (Task task : tasks) {
			String processInstanceId = task.getProcessInstanceId();
			// ru_execution
			ProcessInstance processInstance = runtimeService
					.createProcessInstanceQuery()
					.processInstanceId(processInstanceId).active()
					.singleResult();
			ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionId(processInstance.getProcessDefinitionId()).singleResult();
			String key = processDefinition.getKey();
			String businessKey = processInstance.getBusinessKey();
			if (businessKey != null) {
				TableBean tb = tbService.byClassName(key);
				Object x = get(Class.forName(key),businessKey);
				Todo td = new Todo();
				td.setDescr(ReUtils.getStr(x, "id"));
				td.setCdate(DateTimeUtils.formatDate(task.getCreateTime(), "yy-MM-dd HH:mm"));
				td.setLink(tb.getPkg()+"/"+tb.getBean()+"/edit?_isp=1&id="+businessKey);
				results.add(td);
			}
			// Leave leave = get(processInstance.getName()new
			// Long(businessKey));
			// leave.setTask(task);
			// leave.setProcessInstance(processInstance);
			// leave.setProcessDefinition(getProcessDefinition(processInstance.getProcessDefinitionId()));
			// results.add(leave);
		}

		return results;
	}
}
