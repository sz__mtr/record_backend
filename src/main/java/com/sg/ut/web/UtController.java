package com.sg.ut.web;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.sg.common.empty.core.ConditionRes;
import com.sg.common.empty.core.Conditions;
import com.sg.common.empty.core.Params2;
import com.sg.common.empty.service.CfgService;
import com.sg.common.utils.Constant;
import com.sg.common.utils.DateTimeUtils;
import com.sg.ut.service.UtService;

@Repository
@Controller
@RequestMapping(value = "/mob")
public class UtController {

	private Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	UtService service;
	
	@RequestMapping(value = "/ut/self/list")
	@ResponseBody
	public Object find(HttpServletRequest request) throws Exception {
		String route = (String) request.getAttribute("conf.route");
		// TODO 如果有js，使用js确定使用哪个sql
		Conditions condis = Conditions.init(request);
		ConditionRes cr = condis.buildCon();
		String sorts = condis.buildSorts();
		String limit = condis.buildLimit();

		// 替换
		String orancsql = Constant.get(route + ".csql");
		String oransql = Constant.get(route + ".sql");
		orancsql = orancsql.replace("@conditions", cr.getSql()).replace(
				"@orderby", sorts);
		oransql = oransql.replace("@conditions", cr.getSql())
				.replace("@orderby", sorts).replace("@limit", limit);

		List<Object> countList = service.queryBySql(orancsql, cr.getParams());
		String res = Constant.get(route + ".res");
		List<Map<String, Object>> dataList = service.queryBySqlToMap(oransql,
				cr.getParams(), res.split(","));

		int count = ((BigInteger) countList.get(0)).intValue();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("items", dataList);
		map.put("total", count);
		map.put("code", 2000);
		return map;
	}

	@RequestMapping(value = "/ut/self/xzdblist")
	@ResponseBody
	public Object xzdbfind(HttpServletRequest request) throws Exception {
		String route = (String) request.getAttribute("conf.route");
		// TODO 如果有js，使用js确定使用哪个sql
		Conditions condis = Conditions.init(request);
		ConditionRes cr = condis.buildCon();
		String sorts = condis.buildSorts();
		String limit = condis.buildLimit();

		String scRes = "sid,no,title,content,dept,state,sys";
		List<Map<String, Object>> taskList = service.queryBySqlToMap(
				"SELECT id,no,affair,claim,dept,state,'ICE' as sys FROM sc_pro where enable=1 order by no desc",
				new String[] {}, scRes.split(","));

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("items", taskList);
		map.put("total", taskList.size());
		map.put("code", 2000);
		return map;
	}
	
	@RequestMapping(value = "/ut/self/update")
	@ResponseBody
	public Object update(HttpServletRequest request,
			@RequestBody Map<String, Object> requestBody) throws Exception {
		Map<String, Object> task = (Map<String, Object>) requestBody
				.get("task");
		String type = (String) requestBody.get("type");
		String approver = (String) requestBody.get("approver");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map=service.updateTask(task, type,approver);
		} catch (Exception e) {
			logger.error(e.getMessage());
			map.put("code", 5030);
			map.put("msg", "更新发生内部错误，请联系管理员");
		}
		return map;
	}
	
	@RequestMapping(value = "/ut/self/detail")
	@ResponseBody
	public Object detail(HttpServletRequest request,
			@RequestBody Map<String, Object> requestBody) throws Exception {
//		String taskId = requestBody.get("taskId").toString();
		String eventId = requestBody.get("eventId").toString();
		String sys = requestBody.get("sys").toString();
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			String scRes = "编号,任务类型,任务来源,时间,类别,事项,督办要求,落实部门,关注日期,要求完成日期,实际完成日期,落实情况";
			List<Map<String, Object>> taskList = service.queryBySqlToMap(
					"SELECT no,local,scFrom,scTime,kind,affair,claim,dept,markTime,limitTime,okTime,plog FROM sc_pro where id=?",
					new String[] { eventId }, scRes.split(","));
			
//			String reRes = "dept,finishTime,rlog";
//			List<Map<String, Object>> taskreList = service.queryBySqlToMap(
//			 		"SELECT dept,finishTime,rlog FROM sc_rectificate where sid=?",
//					new String[] { eventId }, reRes.split(","));
			Map<String, Object> taskMap=taskList.get(0);
			Iterator it = taskMap.keySet().iterator();
			List<Object> task=new ArrayList<>();
			while (it.hasNext()) {
				String key = it.next().toString();
				String value="";
				Object o=taskMap.get(key);
				if(o!=null)
				{
					value=o.toString();
				}
				if(StringUtils.equals(key, "任务类型"))
				{
					value=findLocalStr(value);
				}
				Map<String, Object> km = new HashMap<String, Object>();
				km.put("label", key);
				km.put("value", value);
				task.add(km);
			}
			map.put("task", task);
//			map.put("items", taskreList);
			map.put("code", 2000);
		} catch (Exception e) {
			logger.error(e.getMessage());
			map.put("code", 5030);
			map.put("msg", "更新发生内部错误，请联系管理员");
		}
		return map;
	}
	
	public static String LOCAL1="sc";
	public static String LOCAL2="zjb";
	public static String LOCAL3="ydlh";
	public static String LOCAL4="zth";
	public static String LOCAL5="zngl";
	public static String LOCAL6="ddzl";
	public static String LOCAL7="gzjs";
	public static String LOCAL8="zxb";
	public static String LOCAL9="bmgb";
	public static String LOCAL10="awh";
	public static String LOCAL11="zlh";
	public static String LOCAL12="zthy";
	
	public static String findLocalStr(String local){
		if(LOCAL1.equals(local)){
			return "生产交班会";
		}else if(LOCAL2.equals(local)){
			return "总经理办公会";
		}else if(LOCAL3.equals(local)){
			return "分公司月度例会";
		}else if(LOCAL4.equals(local)){
			return "职工季度座谈会";
		}else if(LOCAL5.equals(local)){
			return "职能管理会";
		}else if(LOCAL6.equals(local)){
			return "调度质量问题";
		}else if(LOCAL7.equals(local)){
			return "故障技术分析通报";
		}else if(LOCAL8.equals(local)){
			return "知行信息表";
		}else if(LOCAL9.equals(local)){
			return "百名干部进班组";
		}else if(LOCAL10.equals(local)){
			return "分公司安委会";
		}else if(LOCAL11.equals(local)){
			return "分公司质量会";
		}else if(LOCAL12.equals(local)){
			return "专题会议";
		}
		return "";
	}
	
}
