package com.sg.ut.service;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.sg.common.dao.BaseDao;
import com.sg.common.db.DbUtil;
import com.sg.common.utils.Constant;
import com.sg.common.utils.DateTimeUtils;

@Component
public class UtService extends BaseDao {

	String now = DateTimeUtils.formatDate(
			new Timestamp(System.currentTimeMillis()), "yyyy-MM-dd HH:mm");

	@Transactional
	public Map<String, Object> updateTask(Map<String, Object> task,
			String type, String approver) throws Exception {
		Map<String, Object> res = new HashMap<String, Object>();
		if (StringUtils.equals((String) task.get("sys"), "ICE")) {
			String taskId = task.get("id").toString();
			String sId = task.get("sid").toString();
			String rId = task.get("rid").toString();
			String dept = task.get("dept").toString();
			long nums = Long.parseLong(task.get("nums").toString());

			String scRes = "limitTime,plog,rlog,affair,no,zrdept";
			List<Map<String, Object>> dataList = queryBySqlToMap(
					"SELECT limitTime,plog,rlog,sc_pro.affair,sc_pro.no,sc_pro.dept FROM sc_delay inner join sc_rectificate on sc_delay.rid=sc_rectificate.id INNER JOIN sc_pro ON sc_rectificate.sid = sc_pro.id WHERE sc_delay.`status`=0 and sc_delay.id=?",
					new String[] { taskId }, scRes.split(","));
			Timestamp oldlimitTime = null;
			String plog = "";
			String rlog = "";
			String affair = "";
			String no = "";
			String zrdept = "";
			if (dataList.size() > 0) {
				Map<String, Object> dataMap = dataList.get(0);
				oldlimitTime = (Timestamp) dataMap.get("limitTime");
				plog = (String) dataMap.get("plog");
				rlog = (String) dataMap.get("rlog");
				affair = (String) dataMap.get("affair");
				no = (String) dataMap.get("no");
				zrdept = (String) dataMap.get("zrdept");
				// 1通过 2不通过
				String isApproval = "2";
				String today = DateTimeUtils
						.formatDate(new Timestamp(System.currentTimeMillis()),
								"yyyy-MM-dd");
				String rnewLog = "";
				Timestamp newlimitTime = null;
				if (StringUtils.equals(type, "fav")) {
					isApproval = "1";
					newlimitTime = new Timestamp(oldlimitTime.getTime() + nums
							* 24 * 60 * 60 * 1000);

					plog = plog + dept + " " + today + " 延期" + nums + "天 "
							+ ";<br>";
					rnewLog = dept + " " + today + " 延期" + nums + "天 "
							+ "(通过);<br>";
					String updateProSql = "update sc_pro set limitTime=?,plog=? where id=?";
					excuteBySql(updateProSql,
							new Object[] { newlimitTime, plog,
									sId });
					rlog = rlog + rnewLog;
				} else {
					rnewLog = dept + " 申请延期" + nums + "天 " + "（驳回）;<br>";
					rlog = rlog + rnewLog;
				}
				String updateDelaySql = "update sc_delay set status=1,isapproval=?,approver=?,utime=? where id=?";
				excuteBySql(updateDelaySql, new Object[] { isApproval,
						approver, now,taskId });

				String updateRSql = "update sc_rectificate set rlog=? where id=?";
				excuteBySql(updateRSql, new Object[] { rlog, rId });

				String users=Constant.get("xzdb_dd");
				if (StringUtils.equals(type, "fav")) {
					String content = "{\"title\": \"行政督办延期提醒\",\"text\": \"您好!  \n    "
							+ dept
							+ "对于《"
							+ affair
							+ "》申请了"
							+ nums
							+ "天的延期,请领导知悉。  \n	编号为："
							+ no
							+ ";  \n	责任部门:"
							+ zrdept
							+ "  \n	要求延时部门:"
							+ dept
							+ "  \n	原先要求完成时间为:"
							+ DateTimeUtils.formatDate(oldlimitTime,"yyyy-MM-dd")
							+ ",现改为"
							+ DateTimeUtils.formatDate(newlimitTime,"yyyy-MM-dd")
							+ "  \n	请进入行政督办钉钉微应用中查看详情。  \n [手机客户端点我进入](http://47.92.54.167:8080/xzdb/#/home/taskDetail?eventId="
							+ sId + "&sys=ICE)\"}";
					String sql = "INSERT INTO tosend(date,time,content,state,type,msgtype,userid,partyid,status,inserttime) VALUES('','','"
							+ content
							+ "',1,'待办通知','markdown','"
							+ users + "','','notsent','" + now + "')";
					excuteBySql(sql, null);
				}
				res.put("code", "2000");
			} else {
				res.put("code", "4000");
				res.put("msg", "该任务不存在，或已被处理，请下拉刷新后重试！");
			}

		}
		return res;
	}

}
