package com.sg.activiti.listener;


import java.util.List;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.sg.common.empty.service.CfgService;
import com.sg.common.login.entity.Member;
import com.sg.common.login.service.OrgMemberService;

//@Component("dingsend_comp")
@Transactional
public class DingSend implements TaskListener {

    private static final long serialVersionUID = 1L;

	@Autowired
	private RuntimeService runtimeService;
	
	@Autowired
	protected RepositoryService repositoryService;
	
	@Autowired
	protected CfgService service;

	@Autowired
	OrgMemberService orgMemberService;

	
	public Expression subject;
    public Expression togroup;
    public Expression tomember;
    public Expression body;
    
    
    public void notify(DelegateTask delegateTask) {
    	System.out.println("-info---come into CardAdd!");
//    	ServletContext servletContext = arg0.getServletContext();
//		ApplicationContext ctx = WebApplicationContextUtils
//				.getWebApplicationContext(servletContext);
    	 String sub= subject.getExpressionText();
    	 String tom=tomember.getExpressionText();
    	 String tog=togroup.getExpressionText();
    	 String bo=body.getExpressionText();
    	 if(StringUtils.isNotEmpty(tog))
    	 {
    		List<Member> mList= orgMemberService.findMemInOrg(tog);
    	 }
    	System.out.println("-info---come into CardAdd!"+subject);
	
    }
	public Expression getSubject() {
		return subject;
	}
	public void setSubject(Expression subject) {
		this.subject = subject;
	}
	public Expression getTogroup() {
		return togroup;
	}
	public void setTogroup(Expression togroup) {
		this.togroup = togroup;
	}
	public Expression getTomember() {
		return tomember;
	}
	public void setTomember(Expression tomember) {
		this.tomember = tomember;
	}
	public Expression getBody() {
		return body;
	}
	public void setBody(Expression body) {
		this.body = body;
	}

	
	
}
