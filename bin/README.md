#gdelm_manage_java
饿了么后台管理的数据接口程序，只需增加配置，提供数据的增删改查等数据接口

功能：
1、通用功能：
通过文件配置（properties\iface\{模块名}\）来提供一般功能，每一个文件对应一个模块的接口，如food.conf中，api.fs.food表示响应api/fs/food/xx的uri请求，其中最后的xx可自定义
解析处理配置文件的java程序：com.sg.common.empty.web.ApiController
①列表接口：
1.1定义查询SQL
1.2定义查询SQL中需要的参数，如前台传递的参数A则使用request`A、后台自定义的参数如currentMember等
1.3定义返回接口的包装形式

②更新接口：
2.1定义更新SQL
2.2定义SQL中的参数

③新增接口：
3.1定义新增SQL
3.2定义SQL中的参数

④选择接口：
1.1定义查询SQL
1.2定义查询SQL中需要的参数
1.3定义返回接口的包装形式

自定义接口：


2、权限配置
在配置文件中的每一个xx块（如1中所说）中加入如下属性：
type：该xx块的权限类型（0 标准功能，需分配权限	// 1 无需权限	// 2 需登录权限），以下属性在type为0时生效
roles：允许进入的角色
orgs：允许进入的组织
persons：允许进入的人员
js：执行js返回是否允许进入
以上权限标记为或关系，即符合任一即可，程序：com.sg.common.empty.core.DealPermission

七牛图片上传的后台：
通过com.sg.qiniu获取token，然后在前台上传图片

Druid数据库配置：
通过hibernate、jdbc配置

Spring MVC:
web使用spring进行控制




未完成功能：
1、对于前台的多选控件，增加后台应对的处理方式
2、通过requestBody来获取前台结构化数据
